//
//  FilePreviewController.swift
//  Runner
//
//  Created by FancyLou on 2023/5/23.
//

import UIKit
import QuickLook

class FilePreviewController: QLPreviewController {
    var currentFileURLS:[NSURL] = []
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.delegate = self
        self.dataSource = self
        if let url = self.currentFileURLS[0].path {
            if url.lowercased().hasSuffix(".png") || url.lowercased().hasSuffix(".jpg") || url.lowercased().hasSuffix(".jpeg") {
                self.loadImageDownloadBtn()
            }
        }
    }
    
    
    private func loadImageDownloadBtn() {
        let downImageBtn = UIImageView(frame: CGRect(x: UIScreen.main.bounds.width - 48 - 12 , y: UIScreen.main.bounds.height - 48 - 10, width: 48, height: 48))
        downImageBtn.image = UIImage(named: "icon_download")
        self.view.addSubview(downImageBtn)
        downImageBtn.isHidden = false
        downImageBtn.addTapGesture { tap in
            self.saveImageToAlbum()
        }
    }
    
    private func saveImageToAlbum() {
        print("保存图片到相册！")
        if let url = self.currentFileURLS[0].path {
            print("path: \(url)")
            UIImageWriteToSavedPhotosAlbum(UIImage(contentsOfFile: url)!, self, #selector(self.saveImage(image:didFinishSavingWithError:contextInfo:)), nil)
        }
    }
    
    @objc func saveImage(image: UIImage, didFinishSavingWithError error: NSError?, contextInfo:UnsafeRawPointer) {
        if let e = error {
            print(e.localizedDescription)
            //self.showError(title: "保存图片失败！")
        } else {
            //self.showSuccess(title: "保存图片到相册成功！")
            print("保存图片到相册成功！")
        }
    }
}



extension FilePreviewController: QLPreviewControllerDelegate,QLPreviewControllerDataSource {
    func numberOfPreviewItems(in controller: QLPreviewController) -> Int {
        return self.currentFileURLS.count
    }
    
    func previewController(_ controller: QLPreviewController, previewItemAt index: Int) -> QLPreviewItem {
        return self.currentFileURLS[index]
    }
    
    
}
