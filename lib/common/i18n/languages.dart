
import 'dart:ui';

import 'package:get/get.dart';

import 'lang/en_US.dart';
import 'lang/zh_HK.dart';
import 'lang/zh_Hans.dart';
import 'lang/es_ES.dart';

class Languages extends Translations {

  static Locale? get locale => Get.deviceLocale;
  static Locale fallbackLocale = const Locale('zh', 'CN');

  @override
  Map<String, Map<String, String>> get keys => {
        'zh_Hans': zh_Hans,
        'zh_Hans_CN': zh_Hans,
        'zh_CN': zh_Hans,
        'zh_HK': zh_HK,
        'en_US': en_US,
        'es_ES': es_ES
      };
}
