

// import 'dart:io';

// import 'package:flutter_baidu_mapapi_base/flutter_baidu_mapapi_base.dart';
// import 'package:flutter_baidu_mapapi_utils/flutter_baidu_mapapi_utils.dart';
// import 'package:flutter_bmflocation/flutter_bmflocation.dart';
// import 'package:get/get.dart';
// import 'package:permission_handler/permission_handler.dart';

// import '../values/index.dart';
// import 'loading.dart';
// import 'log_util.dart';


// typedef BaiduLocationHelperCallback = void Function(BaiduLocation result);

// class BaiduLocationHelper {
//   // 定位结果返回 多次定位执行多次
//   final BaiduLocationHelperCallback callback;
//   // 是否单次定位 单次定位 返回结果的时候直接停止
//   final bool isSingleLocation;
//   BaiduLocationHelper({required this.callback, this.isSingleLocation = false});

//   // 百度地图定位插件
//   LocationFlutterPlugin myLocPlugin = LocationFlutterPlugin();
  

//   /// 结束定位
//   Future<void> stopLocation() async {
//     await myLocPlugin.stopLocation();
//   }

//   /// 启动定位 
//   /// 
//   /// 需要先做一些初始化和权限验证
//   Future<void> initLocationAndStartRequest() async {
//     OLogger.d('开始初始化定位sdk，设置privacy');
//     BMFMapSDK.setAgreePrivacy(true);
//     // 设置是否隐私政策
//     myLocPlugin.setAgreePrivacy(true);
//     OLogger.d("设置隐私");
//     if (Platform.isIOS) {
//       /// 设置ios端ak, android端ak可以直接在清单文件中配置
//       var authAK = await myLocPlugin.authAK(O2.baiduMapIosAppKey);
//       OLogger.d("ios 设置 $authAK");
//     }
//     _requestLocationPermission();
//   }

//   /// 定位权限判断
//   Future<bool> _locationPermission() async {
//     var status = await Permission.location.status;
//     if (status == PermissionStatus.granted) {
//       return true;
//     } else {
//       status = await Permission.location.request();
//       if (status == PermissionStatus.granted) {
//         return true;
//       }
//     }
//     return false;
//   }
//   /// 定位权限 不通过就提示错误信息
//   void _requestLocationPermission() async {
//     // 申请权限
//     bool hasLocationPermission = await _locationPermission();
//     if (hasLocationPermission) {
//       // 权限申请通过
//       OLogger.d('定位权限已开启，开始定位');
//       _startRequestLocation();
//     } else {
//       OLogger.e('没有权限，无法定位');
//       Loading.showError('attendance_location_permission_not_allow'.tr);
//     }
//   }

//   /// 真正开始定位
//   void _startRequestLocation() async {
//     // 设置参数
//     Map iosMap = _initIOSOptions().getMap();
//     Map androidMap = _initAndroidOptions().getMap();
//     var prepare = await myLocPlugin.prepareLoc(androidMap, iosMap);
//     OLogger.d('设置定位参数：$prepare');
//     myLocPlugin.seriesLocationCallback(callback: (loc) {
//       // 返回定位信息
//       OLogger.d(
//           "定位信息, latitude: ${loc.latitude}, longitude: ${loc.longitude} , address:${loc.address}, locationDetail:${loc.locationDetail}");
//       callback(loc);
//       if (isSingleLocation) {
//         stopLocation();
//       }
//     });
//     var isStart = await myLocPlugin.startLocation();
//     OLogger.i('开始定位：$isStart');
//   }
//   ///
//   /// Android端定位参数
//   ///
//   BaiduLocationAndroidOption _initAndroidOptions() {
//     BaiduLocationAndroidOption options = BaiduLocationAndroidOption(
//         coorType: 'bd09ll',
//         // 定位模式，可选的模式有高精度、仅设备、仅网络。默认为高精度模式
//         locationMode: BMFLocationMode.hightAccuracy,
//         // 是否需要返回地址信息
//         isNeedAddress: true,
//         // 是否需要返回海拔高度信息
//         isNeedAltitude: true,
//         // 是否需要返回周边poi信息
//         isNeedLocationPoiList: true,
//         // 是否需要返回新版本rgc信息
//         isNeedNewVersionRgc: true,
//         // 是否需要返回位置描述信息
//         isNeedLocationDescribe: true,
//         // 是否使用gps
//         openGps: true,
//         // 可选，设置场景定位参数，包括签到场景、运动场景、出行场景
//         locationPurpose: BMFLocationPurpose.signIn,
//         //百度坐标系 可选，默认gcj02，设置返回的定位结果坐标系
//         coordType: BMFLocationCoordType.bd09ll,
//         // 设置发起定位请求的间隔，int类型，单位ms
//         // 如果设置为0，则代表单次定位，即仅定位一次，默认为0
//         scanspan: 5000);
//     return options;
//   }

//   ///
//   /// Ios端定位参数
//   ///
//   BaiduLocationIOSOption _initIOSOptions() {
//     BaiduLocationIOSOption options = BaiduLocationIOSOption(
//       // 坐标系
//       coordType: BMFLocationCoordType.bd09ll,
//       BMKLocationCoordinateType: 'BMKLocationCoordinateTypeBMK09LL',
//       // 位置获取超时时间
//       locationTimeout: 10,
//       // 获取地址信息超时时间
//       reGeocodeTimeout: 10,
//       // 应用位置类型 默认为automotiveNavigation
//       activityType: BMFActivityType.automotiveNavigation,
//       // 设置预期精度参数 默认为best
//       desiredAccuracy: BMFDesiredAccuracy.best,
//       // 是否需要最新版本rgc数据
//       isNeedNewVersionRgc: true,
//       // 指定定位是否会被系统自动暂停
//       pausesLocationUpdatesAutomatically: false,
//       // 指定是否允许后台定位,
//       // 允许的话是可以进行后台定位的，但需要项目
//       // 配置允许后台定位，否则会报错，具体参考开发文档
//       allowsBackgroundLocationUpdates: false,
//       // 设定定位的最小更新距离
//       distanceFilter: 10,
//     );
//     return options;
//   }

// }