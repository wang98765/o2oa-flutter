

extension IntO2Ext on int {

  String friendlyFileLength() {
    if (this < 1024) {
        return '$this B';
    } else {
        int kb = this ~/ 1024;
        if (kb < 1024) {
            return '$kb KB';
        } else {
            int mb = kb ~/ 1024;
            if (mb < 1024) {
                return '$mb MB';
            } else {
                int gb = mb ~/ 1024;
                return '$gb GB';
            }
        }
    }
  }
}