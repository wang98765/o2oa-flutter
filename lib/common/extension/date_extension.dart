

import 'package:get/get.dart';

extension O2DateTimeUtils on DateTime {


  /// 当前月份第一天
  DateTime monthFirstDate() {
    return DateTime(year, month, 1);
  }

  /// 当前月份最后一天
  DateTime monthLastDate() {
    return DateTime(year, month + 1, 0);
  }
  ///
  /// 秒数归零
  DateTime zeroedSeconds() {
    return DateTime(year, month, day, hour, minute, 0);
  }
   ///
   /// 分数归零 包括秒数
  DateTime zeroedMinutes({isIncludeSecond = true}) {
    return DateTime(year, month, day, hour, 0, isIncludeSecond ? 0 : second);
  }
   ///
   /// 小时数归零 包括秒数
  DateTime zeroedHours({isIncludeMinute = true, isIncludeSecond = true}) {
    return DateTime(year, month, day, 0, isIncludeMinute ? 0:minute, isIncludeSecond ? 0 : second);
  }

  ///
  /// 增加年数 负数就是减去年数
  /// 
  DateTime addYears(int years) {
    final days = 365 * years;
    return add(Duration(days: days));
  }
  ///
  /// 增加月数 负数就是减去月数
  /// 
  DateTime addMonths(int months) {
    return DateTime(year, month + months, day, hour, minute, second);
  }
  ///
  /// 增加日数 负数就是减去日数
  /// 
  DateTime addDays(int days) {
    return add(Duration(days: days));
  }
  ///
  /// 增加小时数 负数就是减去小时数
  /// 
  DateTime addHours(int hours) {
    return add(Duration(hours: hours));
  }
  ///
  /// 增加分数 负数就是减去分数
  /// 
  DateTime addMinutes(int minutes) {
    return add(Duration(minutes: minutes));
  }

  String ymd({bool needHyphen = true}) {
    var year = this.year;
    var month = this.month > 9 ? '${this.month}': '0${this.month}';
    var date = day > 9 ? '$day': '0$day';
    if (needHyphen) {
      return "$year-$month-$date";
    }
    return "$year$month$date";
  }

  String ymdhms() {
    var year = this.year;
    var month = this.month > 9 ? '${this.month}': '0${this.month}';
    var date = day > 9 ? '$day': '0$day';
    var hour = this.hour > 9 ? '${this.hour}': '0${this.hour}';
    var minute = this.minute > 9 ? '${this.minute}': '0${this.minute}';
    var second = this.second > 9 ? '${this.second}': '0${this.second}';
    return "$year-$month-$date $hour:$minute:$second";
  }

  String hm() {
    var hour = this.hour > 9 ? '${this.hour}': '0${this.hour}';
    var minute = this.minute > 9 ? '${this.minute}': '0${this.minute}';
    return "$hour:$minute";
  }

  String hms() {
    var hour = this.hour > 9 ? '${this.hour}': '0${this.hour}';
    var minute = this.minute > 9 ? '${this.minute}': '0${this.minute}';
    var second = this.second > 9 ? '${this.second}': '0${this.second}';
    return "$hour:$minute:$second";
  }

  String friendlyTime() {
    var now = DateTime.now();
    var minute = now.difference(this).inMinutes;
    if (minute < 60) {
      return "$minute ${"im_time_minute_before".tr}";
    }
    var hour = now.difference(this).inHours;
    if (hour < 24) {
      return "$hour ${"im_time_hour_before".tr}";
    }
    var days = now.difference(this).inDays;
    if (days == 1) {
      var yesterday = now.add(const Duration(days: -1));
      if (yesterday.day == day) {
        return "im_time_yesterday".tr;
      }
      return "im_time_before_yesterday".tr;
    } else if (days == 2) {
      var beforeYesterday = now.add(const Duration(days: -2));
      if (beforeYesterday.day == day) {
        return "im_time_before_yesterday".tr;
      }
      return "im_time_three_days_ago".tr;
    } else if (days < 31) {
      return "$days ${"im_time_days_before".tr}";
    } else if (days >= 31 && days <= 2 * 31) {
      return "im_time_one_month_before".tr;
    }  else if (days > 2 * 31 && days <= 3 * 31) {
      return "im_time_two_month_before".tr;
    } else {
      return ymd();
    }
  }

  String chatMsgShowTimeFormat() {
    var now = DateTime.now();
    var hour = this.hour > 9 ? '${this.hour}': '0${this.hour}';
    var minute = this.minute > 9 ? '${this.minute}': '0${this.minute}';
    // 同一天
    if (now.year == year && now.month == month && now.day == day) {
      return '$hour:$minute';
    } else {
      var days = now.difference(this).inDays;
      if (days == 1) {
        var yesterday = now.add(const Duration(days: -1));
        if (yesterday.day == day) {
          return "${"im_time_yesterday".tr} $hour:$minute";
        }
        return "${"im_time_before_yesterday".tr} $hour:$minute";
      }
      var month = this.month > 9 ? '${this.month}': '0${this.month}';
      var date = day > 9 ? '$day': '0$day';
      return  "$month-$date $hour:$minute";
    }
  }
}