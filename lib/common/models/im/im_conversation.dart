


import 'im_message.dart';

class IMConversationInfo{

  IMConversationInfo({
    this.id,
    this.type,
    this.personList,
    this.title,
    this.adminPerson,
    this.note,
    this.unreadNumber,
    this.isTop,
    this.createTime,
    this.updateTime,
    this.lastMessage,
  });

  String? id;
  String? type;
  List<String>? personList;
  String? title;
  String? adminPerson;
  String? note;
  int? unreadNumber;
  bool? isTop;
  String? createTime ;
  String? updateTime ;
  IMMessage? lastMessage;

factory IMConversationInfo.fromJson(Map<String, dynamic> json) => IMConversationInfo(
        id: json['id'],
        type: json['type'],
        personList: json["personList"] == null ? null : List<String>.from(json["personList"].map((x) => x)),
        title: json['title'],
        adminPerson: json['adminPerson'],
        note: json['note'],
        unreadNumber: json['unreadNumber'],
        isTop: json['isTop'],
        createTime: json['createTime'],
        updateTime: json['updateTime'],
        lastMessage: json["lastMessage"] == null ? null : IMMessage.fromJson(json["lastMessage"]),
        
      );
  Map<String, dynamic> toJson() => {
        "id": id,
        "type": type,
        "personList": personList == null ? null : List<dynamic>.from(personList!.map((x) => x)),
        "title": title,
        "adminPerson": adminPerson,
        "note": note,
        "unreadNumber": unreadNumber,
        "isTop": isTop,
        "createTime": createTime,
        "updateTime": updateTime,
        "lastMessage": lastMessage == null ? null : lastMessage!.toJson(),
      };

}
        