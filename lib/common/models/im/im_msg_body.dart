import 'package:get/get.dart';

enum IMMessageType { text, emoji, image, audio, location, file, process, cms }

class IMMessageBody {
  IMMessageBody(
      {this.type,
      this.body,
      this.fileId,
      this.fileExtension,
      this.fileTempPath,
      this.fileName,
      this.audioDuration,
      this.address,
      this.addressDetail,
      this.latitude,
      this.longitude,
      this.title,
      this.work,
      this.process,
      this.processName,
      this.application,
      this.applicationName,
      this.job,
      this.audioPlaying});

  String? type;
  String? body;
  String? fileId; //文件id
  String? fileExtension; //文件扩展
  String? fileTempPath; //本地临时文件地址
  String? fileName; // 文件名称
  String? audioDuration; // 音频文件时长
  String? address; //type=location的时候位置信息
  String? addressDetail;
  double? latitude; //type=location的时候位置信息
  double? longitude; //type=location的时候位置信息
  String? title; // 工作流程 信息等 标题字段
  String? work; // 工作流程 工作id
  String? process; // 工作流程 流程id
  String? processName; // 工作流程 流程名称
  String? application; // 工作流程 应用id
  String? applicationName; // 工作流程 应用名称
  String? job; // 工作流程 jobId
  bool? audioPlaying = false; // 是否正在播放 UI使用实际数据中不存在这个字段

  factory IMMessageBody.fromJson(Map<String, dynamic> json) => IMMessageBody(
      type: json['type'],
      body: json['body'],
      fileId: json['fileId'],
      fileExtension: json['fileExtension'],
      fileTempPath: json['fileTempPath'],
      fileName: json['fileName'],
      audioDuration: json['audioDuration'],
      address: json['address'],
      addressDetail: json['addressDetail'],
      latitude: json['latitude'],
      longitude: json['longitude'],
      title: json['title'],
      work: json['work'],
      process: json['process'],
      processName: json['processName'],
      application: json['application'],
      applicationName: json['applicationName'],
      job: json['job'],
      audioPlaying: false);
  Map<String, dynamic> toJson() => {
        "type": type,
        "body": body,
        "fileId": fileId,
        "fileExtension": fileExtension,
        "fileTempPath": fileTempPath,
        "fileName": fileName,
        "audioDuration": audioDuration,
        "address": address,
        "addressDetail": addressDetail,
        "latitude": latitude,
        "longitude": longitude,
        "title": title,
        "work": work,
        "process": process,
        "processName": processName,
        "application": application,
        "applicationName": applicationName,
        "job": job,
        "audioPlaying": audioPlaying,
      };

  ///
  /// 会话列表 最后一条消息显示用
  ///
  String conversationBodyString() {
    switch (type) {
      case 'text':
        return body ?? '';
      case 'emoji':
        return 'im_chat_msg_type_emoji'.tr;
      case 'image':
        return 'im_chat_msg_type_image'.tr;
      case 'audio':
        return 'im_chat_msg_type_audio'.tr;
      case 'location':
        return 'im_chat_msg_type_location'.tr;
      case 'file':
        return 'im_chat_msg_type_file'.tr;
      case 'process':
        return 'im_chat_msg_type_process'.tr;
      case 'cms':
        return 'im_chat_msg_type_cms'.tr;
    }
    return '';
  }
}
