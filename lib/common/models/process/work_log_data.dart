 
import 'task_completed_data.dart';
import 'task_data.dart';

class WorkLog {
    List<TaskCompletedData>? taskCompletedList;
    List<TaskData>? taskList;
    String? id;
    String? job;
    String? work;
    String? workCompleted;
    bool? completed;
    String? fromActivity;
    String? fromActivityType;
    String? fromActivityName;
    String? fromActivityAlias;
    String? fromActivityToken;
    String? fromTime;
    String? arrivedActivity;
    String? arrivedActivityType;
    String? arrivedActivityName;
    String? arrivedActivityToken;
    String? arrivedTime;
    String? application;
    String? applicationName;
    String? process;
    String? processName;
    String? route;
    String? routeName;
    bool? connected;
    int? duration;
    bool? splitting;
    String? splitToken;
    String? splitValue;

    bool isEnd = false; // 是否结束 log

    WorkLog({
        this.taskCompletedList,
        this.taskList,
        this.id,
        this.job,
        this.work,
        this.workCompleted,
        this.completed,
        this.fromActivity,
        this.fromActivityType,
        this.fromActivityName,
        this.fromActivityAlias,
        this.fromActivityToken,
        this.fromTime,
        this.arrivedActivity,
        this.arrivedActivityType,
        this.arrivedActivityName,
        this.arrivedActivityToken,
        this.arrivedTime,
        this.application,
        this.applicationName,
        this.process,
        this.processName,
        this.route,
        this.routeName,
        this.connected,
        this.duration,
        this.splitting,
        this.splitToken,
        this.splitValue,
    });

    WorkLog copywith() {
      return WorkLog(
        taskCompletedList: taskCompletedList,
        taskList: taskList,
        id: id,
        job: job,
        work: work,
        workCompleted: workCompleted,
        completed: completed,
        fromActivity: fromActivity,
        fromActivityType: fromActivityType,
        fromActivityName: fromActivityName,
        fromActivityAlias: fromActivityAlias,
        fromActivityToken: fromActivityToken,
        fromTime: fromTime,
        arrivedActivity: arrivedActivity,
        arrivedActivityType: arrivedActivityType,
        arrivedActivityName: arrivedActivityName,
        arrivedActivityToken: arrivedActivityToken,
        arrivedTime: arrivedTime ,
        application: application,
        applicationName: applicationName,
        process: process,
        processName: processName,
        route: route,
        routeName: routeName,
        connected: connected,
        duration: duration,
        splitting: splitting,
        splitToken: splitToken,
        splitValue: splitValue
      );
    }

    factory WorkLog.fromJson(Map<String, dynamic> json) => WorkLog(
        taskCompletedList: json["taskCompletedList"] == null ? [] : List<TaskCompletedData>.from(json["taskCompletedList"]!.map((x) => TaskCompletedData.fromJson(x))),
        taskList: json["taskList"] == null ? [] : List<TaskData>.from(json["taskList"]!.map((x) => TaskData.fromJson(x))),
        id: json["id"],
        job: json["job"],
        work: json["work"],
        workCompleted: json["workCompleted"],
        completed: json["completed"],
        fromActivity: json["fromActivity"],
        fromActivityType: json["fromActivityType"],
        fromActivityName: json["fromActivityName"],
        fromActivityAlias: json["fromActivityAlias"],
        fromActivityToken: json["fromActivityToken"],
        fromTime: json["fromTime"],
        arrivedActivity: json["arrivedActivity"],
        arrivedActivityType: json["arrivedActivityType"],
        arrivedActivityName: json["arrivedActivityName"],
        arrivedActivityToken: json["arrivedActivityToken"],
        arrivedTime: json["arrivedTime"] ,
        application: json["application"],
        applicationName: json["applicationName"],
        process: json["process"],
        processName: json["processName"],
        route: json["route"],
        routeName: json["routeName"],
        connected: json["connected"],
        duration: json["duration"],
        splitting: json["splitting"],
        splitToken: json["splitToken"],
        splitValue: json["splitValue"],
    );

    Map<String, dynamic> toJson() => {
        "taskCompletedList": taskCompletedList == null ? [] : List<Map<String, dynamic>>.from(taskCompletedList!.map((x) => x.toJson())),
        "taskList": taskList == null ? [] : List<Map<String, dynamic>>.from(taskList!.map((x) => x.toJson())),
        "id": id,
        "job": job,
        "work": work,
        "workCompleted": workCompleted,
        "completed": completed,
        "fromActivity": fromActivity,
        "fromActivityType": fromActivityType,
        "fromActivityName": fromActivityName,
        "fromActivityAlias": fromActivityAlias,
        "fromActivityToken": fromActivityToken,
        "fromTime": fromTime,
        "arrivedActivity": arrivedActivity,
        "arrivedActivityType": arrivedActivityType,
        "arrivedActivityName": arrivedActivityName,
        "arrivedActivityToken": arrivedActivityToken,
        "arrivedTime": arrivedTime,
        "application": application,
        "applicationName": applicationName,
        "process": process,
        "processName": processName,
        "route": route,
        "routeName": routeName,
        "connected": connected,
        "duration": duration,
        "splitting": splitting,
        "splitToken": splitToken,
        "splitValue": splitValue,
    };
}
