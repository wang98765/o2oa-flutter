// To parse this JSON data, do
//
//     final taskData = taskDataFromJson(jsonString);


class TaskData {
    TaskData({
        this.rank,
        this.id,
        this.job,
        this.title,
        this.startTime,
        this.startTimeMonth,
        this.work,
        this.application,
        this.applicationName,
        this.applicationAlias,
        this.process,
        this.processName,
        this.processAlias,
        this.serial,
        this.person,
        this.identity,
        this.unit,
        this.activity,
        this.activityName,
        this.activityAlias,
        this.activityDescription,
        this.activityType,
        this.activityToken,
        this.creatorPerson,
        this.creatorIdentity,
        this.creatorUnit,
        this.expired,
        this.urged,
        this.routeList,
        this.routeNameList,
        this.routeOpinionList,
        this.routeDecisionOpinionList,
        this.routeName,
        this.opinion,
        this.mediaOpinion,
        this.modified,
        this.viewed,
        this.allowRapid,
        this.first,
        this.properties,
        this.series,
        this.workCreateType,
        this.createTime,
        this.updateTime,
    });

    int? rank;
    String? id;
    String? job;
    String? title;
    String? startTime;
    String? startTimeMonth;
    String? work;
    String? application;
    String? applicationName;
    String? applicationAlias;
    String? process;
    String? processName;
    String? processAlias;
    String? serial;
    String? person;
    String? identity;
    String? unit;
    String? activity;
    String? activityName;
    String? activityAlias;
    String? activityDescription;
    String? activityType;
    String? activityToken;
    String? creatorPerson;
    String? creatorIdentity;
    String? creatorUnit;
    bool? expired;
    bool? urged;
    List<String>? routeList;
    List<String>? routeNameList;
    List<String>? routeOpinionList;
    List<String>? routeDecisionOpinionList;
    String? routeName;
    String? opinion;
    String? mediaOpinion; // 手写意见的图片 id
    bool? modified;
    bool? viewed;
    bool? allowRapid; // 是否允许 批量快速处理
    bool? first;
    Properties? properties;
    String? series;
    String? workCreateType;
    String? createTime;
    String? updateTime;

    factory TaskData.fromJson(Map<String, dynamic> json) => TaskData(
        rank: json["rank"],
        id: json["id"],
        job: json["job"],
        title: json["title"],
        startTime: json["startTime"],
        startTimeMonth: json["startTimeMonth"],
        work: json["work"],
        application: json["application"],
        applicationName: json["applicationName"],
        applicationAlias: json["applicationAlias"],
        process: json["process"],
        processName: json["processName"],
        processAlias: json["processAlias"],
        serial: json["serial"],
        person: json["person"],
        identity: json["identity"],
        unit: json["unit"],
        activity: json["activity"],
        activityName: json["activityName"],
        activityAlias: json["activityAlias"],
        activityDescription: json["activityDescription"],
        activityType: json["activityType"],
        activityToken: json["activityToken"],
        creatorPerson: json["creatorPerson"],
        creatorIdentity: json["creatorIdentity"],
        creatorUnit: json["creatorUnit"],
        expired: json["expired"],
        urged: json["urged"],
        routeList: json["routeList"] == null ? null : List<String>.from(json["routeList"].map((x) => x)),
        routeNameList: json["routeNameList"] == null ? null : List<String>.from(json["routeNameList"].map((x) => x)),
        routeOpinionList: json["routeOpinionList"] == null ? null : List<String>.from(json["routeOpinionList"].map((x) => x)),
        routeDecisionOpinionList: json["routeDecisionOpinionList"] == null ? null : List<String>.from(json["routeDecisionOpinionList"].map((x) => x)),
        routeName: json["routeName"],
        opinion: json["opinion"],
        mediaOpinion: json["mediaOpinion"],
        modified: json["modified"],
        viewed: json["viewed"],
        allowRapid: json["allowRapid"],
        first: json["first"],
        properties: json["properties"] == null ? null : Properties.fromJson(json["properties"]),
        series: json["series"],
        workCreateType: json["workCreateType"],
        createTime: json["createTime"],
        updateTime: json["updateTime"],
    );

    Map<String, dynamic> toJson() => {
        "rank": rank,
        "id": id,
        "job": job,
        "title": title,
        "startTime": startTime,
        "startTimeMonth": startTimeMonth,
        "work": work,
        "application": application,
        "applicationName": applicationName,
        "applicationAlias": applicationAlias,
        "process": process,
        "processName": processName,
        "processAlias": processAlias,
        "serial": serial,
        "person": person,
        "identity": identity,
        "unit": unit,
        "activity": activity,
        "activityName": activityName,
        "activityAlias": activityAlias,
        "activityDescription": activityDescription,
        "activityType": activityType,
        "activityToken": activityToken,
        "creatorPerson": creatorPerson,
        "creatorIdentity": creatorIdentity,
        "creatorUnit": creatorUnit,
        "expired": expired,
        "urged": urged,
        "routeList": routeList == null ? null : List<dynamic>.from(routeList!.map((x) => x)),
        "routeNameList": routeNameList == null ? null : List<dynamic>.from(routeNameList!.map((x) => x)),
        "routeOpinionList": routeOpinionList == null ? null : List<dynamic>.from(routeOpinionList!.map((x) => x)),
        "routeDecisionOpinionList": routeDecisionOpinionList == null ? null : List<dynamic>.from(routeDecisionOpinionList!.map((x) => x)),
        "routeName": routeName,
        "opinion": opinion,
        "mediaOpinion": mediaOpinion,
        "modified": modified,
        "viewed": viewed,
        "allowRapid": allowRapid,
        "first": first,
        "properties": properties == null ? null : properties!.toJson(),
        "series": series,
        "workCreateType": workCreateType,
        "createTime": createTime,
        "updateTime": updateTime,
    };
}

class Properties {
    Properties({
        this.prevTaskIdentity,
        this.prevTaskIdentityList,
        this.title,
        this.prevTaskList,
        this.prevTask,
    });

    String? prevTaskIdentity;
    List<String>? prevTaskIdentityList;
    String? title;
    List<PrevTask>? prevTaskList;
    PrevTask? prevTask;

    factory Properties.fromJson(Map<String, dynamic> json) => Properties(
        prevTaskIdentity: json["prevTaskIdentity"],
        prevTaskIdentityList: json["prevTaskIdentityList"] == null ? null : List<String>.from(json["prevTaskIdentityList"].map((x) => x)),
        title: json["title"],
        prevTaskList: json["prevTaskList"] == null ? null : List<PrevTask>.from(json["prevTaskList"].map((x) => PrevTask.fromJson(x))),
        prevTask: json["prevTask"] == null ? null : PrevTask.fromJson(json["prevTask"]),
    );

    Map<String, dynamic> toJson() => {
        "prevTaskIdentity": prevTaskIdentity,
        "prevTaskIdentityList": prevTaskIdentityList == null ? null : List<dynamic>.from(prevTaskIdentityList!.map((x) => x)),
        "title": title,
        "prevTaskList": prevTaskList == null ? null : List<dynamic>.from(prevTaskList!.map((x) => x.toJson())),
        "prevTask": prevTask == null ? null : prevTask!.toJson(),
    };
}

class PrevTask {
    PrevTask({
        this.routeName,
        this.unit,
        this.identity,
        this.person,
        this.opinion,
        this.startTime,
        this.completedTime,
    });

    String? routeName;
    String? unit;
    String? identity;
    String? person;
    String? opinion;
    String? startTime;
    String? completedTime;

    factory PrevTask.fromJson(Map<String, dynamic> json) => PrevTask(
        routeName: json["routeName"],
        unit: json["unit"],
        identity: json["identity"],
        person: json["person"],
        opinion: json["opinion"],
        startTime: json["startTime"],
        completedTime: json["completedTime"],
    );

    Map<String, dynamic> toJson() => {
        "routeName": routeName,
        "unit": unit,
        "identity": identity,
        "person": person,
        "opinion": opinion,
        "startTime": startTime,
        "completedTime": completedTime,
    };
}
