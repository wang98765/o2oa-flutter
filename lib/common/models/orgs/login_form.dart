///
///密码登录的表单
///
class LoginForm {
  String? credential;
  String? password;
  String? captcha; //图片认证编号id
  String? captchaAnswer; //图片认证码
  String? isEncrypted; //是否启用加密 y是启用加密

  LoginForm(
      {this.credential,
      this.password,
      this.captcha,
      this.captchaAnswer,
      this.isEncrypted});

  LoginForm.fromJson(Map<String, dynamic>? jsonMap) {
    if (jsonMap != null) {
      credential = jsonMap['credential'];
      password = jsonMap['password'];
      captcha = jsonMap['captcha'];
      captchaAnswer = jsonMap['captchaAnswer'];
      isEncrypted = jsonMap['isEncrypted'];
    }
  }

  Map<String, dynamic> toJson() => {
        'credential': credential,
        'password': password,
        'captcha': captcha,
        'captchaAnswer': captchaAnswer,
        'isEncrypted': isEncrypted,
      };
}

class UpdateLoginPasswordForm {
  String? oldPassword;
  String? newPassword;
  String? confirmPassword;
  String? isEncrypted;

  UpdateLoginPasswordForm(
      {this.oldPassword,
      this.newPassword,
      this.confirmPassword,
      this.isEncrypted});
  UpdateLoginPasswordForm.fromJson(Map<String, dynamic>? jsonMap) {
    if (jsonMap != null) {
      oldPassword = jsonMap['oldPassword'];
      newPassword = jsonMap['newPassword'];
      confirmPassword = jsonMap['confirmPassword'];
      isEncrypted = jsonMap['isEncrypted'];
    }
  }
  Map<String, dynamic> toJson() => {
        'oldPassword': oldPassword,
        'newPassword': newPassword,
        'confirmPassword': confirmPassword,
        'isEncrypted': isEncrypted,
      };
}
