///{
//      "subDirectUnitCount": 0,
//      "subDirectIdentityCount": 0,
//      "control": {
//        "allowEdit": false,
//        "allowDelete": false
//      },
//      "id": "3af179fb-8b7a-4963-a032-3b6bff158f50",
//      "name": "贵州移动",
//      "unique": "00450000000000000000",
//      "distinguishedName": "贵州移动@00450000000000000000@U",
//      "typeList": [
//        "1"
//      ],
//      "pinyin": "guizhouyidong",
//      "pinyinInitial": "gzyd",
//      "description": "顶级",
//      "shortName": "",
//      "level": 1,
//      "levelName": "贵州移动",
//      "superior": "",
//      "orderNumber": 1,
//      "controllerList": [],
//      "inheritedControllerList": [],
//      "dingdingId": "",
//      "dingdingHash": "",
//      "qiyeweixinId": "",
//      "zhengwuDingdingId": "",
//      "zhengwuDingdingHash": "",
//      "qiyeweixinHash": "",
//      "createTime": "2019-06-03 16:33:02",
//      "updateTime": "2019-06-03 16:33:02"
//    },
///

class O2Unit{
  int? subDirectUnitCount;//下级组织
  int?  subDirectIdentityCount;//下级身份
  String?  id;
  String?  name;
  String?  unique;
  String?  distinguishedName;
  String?  pinyin;
  String?  pinyinInitial;
  String?  description;
  String?  shortName;
  int?  level;
  String?  levelName;
  String?  superior;
  int?  orderNumber;
  String?  dingdingId;
  String?  dingdingHash;
  String?  qiyeweixinId;
  String?  zhengwuDingdingId;
  String?  zhengwuDingdingHash;
  String?  qiyeweixinHash;
  String?  createTime;
  String?  updateTime;

  O2Unit({this.subDirectUnitCount = 0,
  this.subDirectIdentityCount = 0,
  this.id,
  this.name,
  this.unique,
  this.distinguishedName,
  this.pinyin,
  this.pinyinInitial,
  this.description,
  this.shortName,
  this.level = -1,
  this.levelName,
  this.superior,
  this.orderNumber = -1,
  this.dingdingId,
  this.dingdingHash,
  this.qiyeweixinId,
  this.zhengwuDingdingId,
  this.zhengwuDingdingHash,
  this.qiyeweixinHash,
  this.createTime,
  this.updateTime,});



  @override
  bool operator ==(other) {
    return other is O2Unit && other.id == id;
  }

   @override
  int get hashCode => id.hashCode;

  factory O2Unit.fromJson(Map<String, dynamic> json) {
    return O2Unit(subDirectUnitCount: json["subDirectUnitCount"] ?? 0,
      subDirectIdentityCount: json["subDirectIdentityCount"]?? 0,
      id: json["id"],
      name: json["name"],
      unique: json["unique"],
      distinguishedName: json["distinguishedName"],
      pinyin: json["pinyin"],
      pinyinInitial: json["pinyinInitial"],
      description: json["description"],
      shortName: json["shortName"],
      level: json["level"]?? -1,
      levelName: json["levelName"],
      superior: json["superior"],
      orderNumber: json["orderNumber"] ?? -1,
      dingdingId: json["dingdingId"],
      dingdingHash: json["dingdingHash"],
      qiyeweixinId: json["qiyeweixinId"],
      zhengwuDingdingId: json["zhengwuDingdingId"],
      zhengwuDingdingHash: json["zhengwuDingdingHash"],
      qiyeweixinHash: json["qiyeweixinHash"],
      createTime: json["createTime"],
      updateTime: json["updateTime"],);
  }

  Map<String, dynamic> toJson() {
    return {
      "subDirectUnitCount": subDirectUnitCount,
      "subDirectIdentityCount": subDirectIdentityCount,
      "id": id,
      "name": name,
      "unique": unique,
      "distinguishedName": distinguishedName,
      "pinyin": pinyin,
      "pinyinInitial": pinyinInitial,
      "description": description,
      "shortName": shortName,
      "level": level,
      "levelName": levelName,
      "superior": superior,
      "orderNumber": orderNumber,
      "dingdingId": dingdingId,
      "dingdingHash": dingdingHash,
      "qiyeweixinId": qiyeweixinId,
      "zhengwuDingdingId": zhengwuDingdingId,
      "zhengwuDingdingHash": zhengwuDingdingHash,
      "qiyeweixinHash": qiyeweixinHash,
      "createTime": createTime,
      "updateTime": updateTime,
    };
  }

}