



class SearchPersonList {
  List<String>? personList; 

  SearchPersonList({
    this.personList
  });


  factory SearchPersonList.fromJson(Map<String, dynamic> json) {
    return SearchPersonList(
        personList: json["personList"] == null? null : List<String>.from(json["personList"].map((x) => x)),
    );
  }

  Map<String, dynamic> toJson() {
    return {
      "personList": personList,
    };
  }
}