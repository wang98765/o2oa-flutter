
class CaptchaImgData {
  String? id; 
  String? image; //图片验证码 base64字符串
   
  CaptchaImgData({
    this.id,
    this.image,
  });

  CaptchaImgData.fromJson(Map<String, dynamic>? jsonMap) {
    if (jsonMap != null) {
      id = jsonMap['id'];
      image = jsonMap['image'];
    }
  }

   Map<String, dynamic> toJson() => {
     'id': id,
     'image': image,
   };
}