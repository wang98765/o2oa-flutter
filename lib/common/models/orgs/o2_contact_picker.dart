
import '../enums/index.dart';
import 'index.dart';

/// 通讯录选择器返回结果
class ContactPickerResult {

  List<O2Unit>? departments;
  List<O2Identity>? identities;
  List<O2Group>? groups;
  List<O2Person>? users;

  ContactPickerResult({this.departments, this.identities, this.groups, this.users});


  Map<String, dynamic> toJson() {
    return {
      "departments": departments,
      "identities": identities,
      "groups": groups,
      "users": users,
    };
  }
}

/// 选择器 传入的参数 根据参数展现不同的选择器功能
class ContactPickerArguments {
  List<ContactPickMode>? pickerModes;
  List<String>? topUnitList;
  String? unitType;
  int? maxNumber;
  bool? multiple;
  List<String>? dutyList;
  List<String>? initDeptList; // 已选部门
  List<String>? initIdList; // 已选身份
  List<String>? initGroupList; // 已选群组
  List<String>? initUserList; // 已选人员
  ContactPickerArguments({
    required this.pickerModes,
    this.topUnitList,
    this.unitType,
    this.maxNumber,
    this.multiple,
    this.dutyList,
    this.initDeptList,
    this.initIdList,
    this.initGroupList, 
    this.initUserList,
  });

  Map<String, dynamic> toJson() {
    return {
      "topUnitList": topUnitList,
      "unitType": unitType,
      "maxNumber": maxNumber,
      "multiple": multiple,
      "dutyList": dutyList,
      "initIdList": initIdList,
      "initDeptList": initDeptList,
      "initGroupList": initGroupList,
      "initUserList": initUserList,
    };
  }


}