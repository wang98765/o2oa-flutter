// {
//         "id": "7a72221c-a4f1-4fdb-b82a-3e6b4b10794f",
//         "pinyin": "qqhao",
//         "pinyinInitial": "qqh",
//         "name": "QQ号",
//         "unique": "7a72221c-a4f1-4fdb-b82a-3e6b4b10794f",
//         "distinguishedName": "QQ号@7a72221c-a4f1-4fdb-b82a-3e6b4b10794f@PA",
//         "person": "eb459959-fd2c-4476-af65-5625d4db0d8b",
//         "orderNumber": 70382685,
//         "attributeList": [
//           "50954018"
//         ],
//         "createTime": "2020-03-25 14:44:45",
//         "updateTime": "2021-01-13 11:25:17"
//       }


      // To parse this JSON data, do
//
//     final o2PersonAttribute = o2PersonAttributeFromJson(jsonString);

 
class O2PersonAttribute {
    String? id;
    String? pinyin;
    String? pinyinInitial;
    String? name;
    String? unique;
    String? distinguishedName;
    String? person;
    int? orderNumber;
    List<String>? attributeList;
     

    O2PersonAttribute({
        this.id,
        this.pinyin,
        this.pinyinInitial,
        this.name,
        this.unique,
        this.distinguishedName,
        this.person,
        this.orderNumber,
        this.attributeList,
         
    });

    factory O2PersonAttribute.fromJson(Map<String, dynamic> json) => O2PersonAttribute(
        id: json["id"],
        pinyin: json["pinyin"],
        pinyinInitial: json["pinyinInitial"],
        name: json["name"],
        unique: json["unique"],
        distinguishedName: json["distinguishedName"],
        person: json["person"],
        orderNumber: json["orderNumber"],
        attributeList: json["attributeList"] == null ? [] : List<String>.from(json["attributeList"]!.map((x) => x)),
        
    );

    Map<String, dynamic> toJson() => {
        "id": id,
        "pinyin": pinyin,
        "pinyinInitial": pinyinInitial,
        "name": name,
        "unique": unique,
        "distinguishedName": distinguishedName,
        "person": person,
        "orderNumber": orderNumber,
        "attributeList": attributeList == null ? [] : List<dynamic>.from(attributeList!.map((x) => x)),
        
    };
}
