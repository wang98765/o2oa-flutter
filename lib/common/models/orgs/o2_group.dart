
class O2Group {
   
  String? id;
  String? distinguishedName;
  String? unique;
  String? name;
  int? orderNumber;
   

  O2Group.fromJson(Map<String, dynamic>? jsonMap) {
    if (jsonMap != null) {
      id = jsonMap['id'];
      distinguishedName = jsonMap['distinguishedName'];
      unique = jsonMap['unique'];
      name = jsonMap['name'];
      orderNumber = jsonMap['orderNumber'];
    }
  }

  Map<String, dynamic> toJson() => {
        "id": id,
        "name": name,
        "distinguishedName": distinguishedName,
        "unique": unique,
        "orderNumber": orderNumber,
      };
}
