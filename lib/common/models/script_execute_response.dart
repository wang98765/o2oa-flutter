
class ScriptExecuteResponse {
  dynamic value;

  ScriptExecuteResponse({this.value});

   ScriptExecuteResponse.fromJson(Map<String, dynamic>? map) {
     if (map != null) {
      value = map['value'];
     }
   }

}