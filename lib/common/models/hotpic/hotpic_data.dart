

// To parse this JSON data, do
//
//     final hotpicData = hotpicDataFromJson(jsonString);

class HotpicData {
    HotpicData({
        this.id,
        this.application,
        this.infoId,
        this.title,
        this.summary,
        this.url,
        this.picId,
        this.creator,
        this.createTime,
        this.updateTime,
    });

    String? id;
    String? application; // 应用名称  CMS|BBS等等.
    String? infoId; // 信息对象ID
    String? title; // 信息标题
    String? summary;
    String? url;
    String? picId;
    String? creator;
    String? createTime;
    String? updateTime;

    factory HotpicData.fromJson(Map<String, dynamic> json) => HotpicData(
        id: json["id"],
        application: json["application"],
        infoId: json["infoId"],
        title: json["title"],
        summary: json["summary"],
        url: json["url"],
        picId: json["picId"],
        creator: json["creator"],
        createTime: json["createTime"],
        updateTime: json["updateTime"],
    );

    Map<String, dynamic> toJson() => {
        "id": id,
        "application": application,
        "infoId": infoId,
        "title": title,
        "summary": summary,
        "url": url,
        "picId": picId,
        "creator": creator,
        "createTime": createTime,
        "updateTime": updateTime,
    };

    bool isCmsInfo() {
      return (application == 'CMS' || application == 'cms');
    }
    bool isBBSInfo() {
      return (application == 'BBS' || application == 'bbs');
    }
}
