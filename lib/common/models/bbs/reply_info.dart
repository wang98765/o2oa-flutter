class SubjectReplyInfoJson {
  String? id;
  String? createTime;
  String? updateTime;
  String? forumId;
  String? forumName;
  String? sectionId;
  String? sectionName;
  String? mainSectionId;
  String? mainSectionName;
  String? subjectId;
  String? title;
  String? parentId;
  String? content;
  String? creatorName;
  String? creatorNameShort;
  int? orderNumber;
  SubjectReplyInfoJson({
    this.id,
    this.createTime,
    this.updateTime,
    this.forumId,
    this.forumName,
    this.sectionId,
    this.sectionName,
    this.mainSectionId,
    this.mainSectionName,
    this.subjectId,
    this.title,
    this.parentId,
    this.content,
    this.creatorName,
    this.creatorNameShort,
    this.orderNumber,
  });



    factory SubjectReplyInfoJson.fromJson(Map<String, dynamic> json) => SubjectReplyInfoJson(
        id: json["id"] ,
        createTime: json["createTime"] ,
        updateTime: json["updateTime"] ,
        forumId: json["forumId"] ,
        forumName: json["forumName"] ,
        sectionId: json["sectionId"] ,
        sectionName: json["sectionName"] ,
        mainSectionId: json["mainSectionId"] ,
        mainSectionName: json["mainSectionName"], 
        subjectId: json["subjectId"] ,
        title: json["title"] ,
        parentId: json["parentId"] ,
        content: json["content"] ,
        creatorName: json["creatorName"] ,
        creatorNameShort: json["creatorNameShort"] ,
        orderNumber: json["orderNumber"] ,
    );

    Map<String, dynamic> toJson() => {
        "id": id  ,
        "createTime": createTime  ,
        "updateTime": updateTime ,
        "forumId": forumId ,
        "forumName": forumName ,
        "sectionId": sectionId ,
        "sectionName": sectionName ,
        "mainSectionId": mainSectionId ,
        "mainSectionName": mainSectionName ,
        "subjectId": subjectId ,
        "title": title ,
        "parentId": parentId ,
        "content": content ,
        "creatorName": creatorName ,
        "creatorNameShort": creatorNameShort ,
        "orderNumber": orderNumber ,
    };
}
