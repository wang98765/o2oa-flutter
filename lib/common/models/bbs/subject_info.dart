
///
/// 帖子信息对象
///
class SubjectInfo {

SubjectInfo({
  this.id,
  this.createTime,
  this.updateTime,
  this.sequence,
  this.forumId,
  this.forumName,
  this.sectionId,
  this.sectionName,
  this.mainSectionId,
  this.mainSectionName,
  this.title,
  this.creatorName,
  this.creatorNameShort,
  this.type,
  this.summary,
  this.content,
  this.latestReplyTime,
  this.latestReplyUser,
  this.latestReplyId,
  this.replyTotal,
  this.viewTotal,
  this.hot,
  this.stopReply,
  this.recommendToBBSIndex,
  this.bBSIndexSetterName,
  this.recommendToForumIndex,
  this.forumIndexSetterName,
  this.topToSection,
  this.topToMainSection,
  this.topToForum,
  this.topToBBS,
  this.isTopSubject,
  this.isCreamSubject,
  this.anonymousSubject,
  this.typeCategory,
  this.machineName,
  this.systemType,
  this.nickName

  // this.attachmentList,
});
                           String? id;
                           String?  createTime;
                           String?  updateTime;
                           String?  sequence;
                           String?  forumId;
                           String?  forumName;
                           String?  sectionId;
                           String?  sectionName;
                           String?  mainSectionId;
                           String?  mainSectionName;
                           String?  title; // 标题
                           String?  creatorName;
                           String?  creatorNameShort;
                           String?  type; // 类型
                           String?  summary; // 摘要
                           String?  content; // 内容
                           String?  latestReplyTime;
                           String?  latestReplyUser;
                           String?  latestReplyId;
                           int?  replyTotal;
                           int?  viewTotal;
                           int?  hot;
                           bool?  stopReply;
                           bool?  recommendToBBSIndex;
                           String?  bBSIndexSetterName;
                           bool?  recommendToForumIndex;
                           String?  forumIndexSetterName;
                           bool?  topToSection;
                           bool?  topToMainSection;
                           bool?  topToForum;
                           bool?  topToBBS;
                           bool?  isTopSubject;
                           bool?  isCreamSubject;
                           bool?  anonymousSubject; // 匿名发帖
                           String? typeCategory; // 分类
                           String? machineName; // 设备名称
                           String? systemType; // 系统类型
                           String? nickName; // 昵称 


    factory SubjectInfo.fromJson(Map<String, dynamic> json) => SubjectInfo(
        id: json["id"] ,
        createTime: json["createTime"] ,
        updateTime: json["updateTime"] ,
        sequence: json["sequence"] ,
        forumId: json["forumId"] ,
        forumName: json["forumName"] ,
        sectionId: json["sectionId"] ,
        sectionName: json["sectionName"] ,
        mainSectionId: json["mainSectionId"] ,
        mainSectionName: json["mainSectionName"] ,
        title: json["title"] ,
        creatorName: json["creatorName"] ,
        creatorNameShort: json["creatorNameShort"] ,
        type: json["type"] ,
        summary: json["summary"] ,
        content: json["content"] ,
        latestReplyTime: json["latestReplyTime"] ,
        latestReplyUser: json["latestReplyUser"] ,
        latestReplyId: json["latestReplyId"] ,
        replyTotal: json["replyTotal"] ,
        viewTotal: json["viewTotal"] ,
        hot: json["hot"] ,
        stopReply: json["stopReply"] ,
        recommendToBBSIndex: json["recommendToBBSIndex"] ,
        bBSIndexSetterName: json["bBSIndexSetterName"] ,
        recommendToForumIndex: json["recommendToForumIndex"] ,
        forumIndexSetterName: json["forumIndexSetterName"] ,
        topToSection: json["topToSection"] ,
        topToMainSection: json["topToMainSection"] ,
        topToForum: json["topToForum"] ,
        topToBBS: json["topToBBS"] ,
        isTopSubject: json["isTopSubject"] ,
        isCreamSubject: json["isCreamSubject"] ,
        anonymousSubject: json["anonymousSubject"] ,
        typeCategory: json["typeCategory"] ,
        machineName: json["machineName"] ,
        systemType: json["systemType"] ,
        nickName: json["nickName"] ,
    );

    Map<String, dynamic> toJson() => {
        "id": id  ,
        "createTime": createTime  ,
        "updateTime": updateTime ,
        "sequence": sequence ,
        "forumId": forumId ,
        "forumName": forumName ,
        "sectionId": sectionId ,
        "sectionName": sectionName ,
        "mainSectionId": mainSectionId ,
        "mainSectionName": mainSectionName ,
        "title": title ,
        "creatorName": creatorName ,
        "creatorNameShort": creatorNameShort ,
        "type": type ,
        "summary": summary ,
        "content": content ,
        "latestReplyTime": latestReplyTime ,
        "latestReplyUser": latestReplyUser ,
        "latestReplyId": latestReplyId ,
        "replyTotal": replyTotal ,
        "viewTotal": viewTotal ,
        "hot": hot ,
        "stopReply": stopReply ,
        "recommendToBBSIndex": recommendToBBSIndex ,
        "bBSIndexSetterName": bBSIndexSetterName ,
        "recommendToForumIndex": recommendToForumIndex ,
        "forumIndexSetterName": forumIndexSetterName ,
        "topToSection": topToSection ,
        "topToMainSection": topToMainSection ,
        "topToForum": topToForum ,
        "topToBBS": topToBBS ,
        "isTopSubject": isTopSubject ,
        "isCreamSubject": isCreamSubject ,
        "anonymousSubject": anonymousSubject ,
        "typeCategory": typeCategory ,
        "machineName": machineName ,
        "systemType": systemType ,
        "nickName": nickName ,
    };
}