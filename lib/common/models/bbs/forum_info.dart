// To parse this JSON data, do
//
//     final forumInfo = forumInfoFromJson(jsonString);

///
/// 論壇分區
/// 
class ForumInfo {
    ForumInfo({
        this.sectionInfoList,
        this.id,
        this.forumName,
        this.forumManagerList,
        this.forumNotice,
        this.forumVisible,
        this.visiblePermissionList,
        this.subjectPublishAble,
        this.publishPermissionList,
        this.replyPublishAble,
        this.replyPermissionList,
        this.indexListStyle,
        this.forumIndexStyle,
        this.subjectType,
        this.typeCategory,
        this.indexRecommendable,
        this.subjectNeedAudit,
        this.replyNeedAudit,
        this.sectionCreateAble,
        this.sectionTotal,
        this.subjectTotal,
        this.replyTotal,
        this.subjectTotalToday,
        this.replyTotalToday,
        this.creatorName,
        this.forumColor,
        this.forumStatus,
        this.orderNumber,
        this.replyMessageNotify,
        this.replyMessageNotifyType,
        this.subjectMessageNotify,
        this.subjectMessageNotifyType,
        this.createTime,
        this.updateTime,
    });

    List<SectionInfoList>? sectionInfoList;
    String? id;
    String? forumName;
    List<String>? forumManagerList;
    String? forumNotice;
    String? forumVisible;
    List<String>? visiblePermissionList;
    String? subjectPublishAble;
    List<String>? publishPermissionList;
    String? replyPublishAble;
    List<String>? replyPermissionList;
    String? indexListStyle;
    String? forumIndexStyle;
    String? subjectType;
    String? typeCategory;
    bool? indexRecommendable;
    bool? subjectNeedAudit;
    bool? replyNeedAudit;
    bool? sectionCreateAble;
    int? sectionTotal;
    int? subjectTotal;
    int? replyTotal;
    int? subjectTotalToday;
    int? replyTotalToday;
    String? creatorName;
    String? forumColor;
    String? forumStatus;
    int? orderNumber;
    bool? replyMessageNotify;
    String? replyMessageNotifyType;
    bool? subjectMessageNotify;
    String? subjectMessageNotifyType;
    String?  createTime;
    String?  updateTime;

    factory ForumInfo.fromJson(Map<String, dynamic> json) => ForumInfo(
        sectionInfoList: json["sectionInfoList"] == null ? null : List<SectionInfoList>.from(json["sectionInfoList"].map((x) => SectionInfoList.fromJson(x))),
        id: json["id"] ,
        forumName: json["forumName"] ,
        forumManagerList: json["forumManagerList"] == null ? null : List<String>.from(json["forumManagerList"].map((x) => x)),
        forumNotice: json["forumNotice"] ,
        forumVisible: json["forumVisible"] ,
        visiblePermissionList: json["visiblePermissionList"] == null ? null : List<String>.from(json["visiblePermissionList"].map((x) => x)),
        subjectPublishAble: json["subjectPublishAble"],
        publishPermissionList: json["publishPermissionList"] == null ? null : List<String>.from(json["publishPermissionList"].map((x) => x)),
        replyPublishAble: json["replyPublishAble"] ,
        replyPermissionList: json["replyPermissionList"] == null ? null : List<String>.from(json["replyPermissionList"].map((x) => x)),
        indexListStyle: json["indexListStyle"] ,
        forumIndexStyle: json["forumIndexStyle"],
        subjectType: json["subjectType"] ,
        typeCategory: json["typeCategory"] ,
        indexRecommendable: json["indexRecommendable"],
        subjectNeedAudit: json["subjectNeedAudit"] ,
        replyNeedAudit: json["replyNeedAudit"] ,
        sectionCreateAble: json["sectionCreateAble"] ,
        sectionTotal: json["sectionTotal"] ,
        subjectTotal: json["subjectTotal"] ,
        replyTotal: json["replyTotal"] ,
        subjectTotalToday: json["subjectTotalToday"] ,
        replyTotalToday: json["replyTotalToday"] ,
        creatorName: json["creatorName"] ,
        forumColor: json["forumColor"],
        forumStatus: json["forumStatus"] ,
        orderNumber: json["orderNumber"] ,
        replyMessageNotify: json["replyMessageNotify"],
        replyMessageNotifyType: json["replyMessageNotifyType"],
        subjectMessageNotify: json["subjectMessageNotify"] ,
        subjectMessageNotifyType: json["subjectMessageNotifyType"] ,
        createTime: json["createTime"] ,
        updateTime: json["updateTime"] ,
    );

    Map<String, dynamic> toJson() => {
        "sectionInfoList": sectionInfoList == null ? null : List<SectionInfoList>.from(sectionInfoList!.map((x) => x.toJson())),
        "id": id  ,
        "forumName": forumName  ,
        "forumManagerList": forumManagerList == null ? null : List<String>.from(forumManagerList!.map((x) => x)),
        "forumNotice": forumNotice  ,
        "forumVisible": forumVisible  ,
        "visiblePermissionList": visiblePermissionList == null ? null : List<String>.from(visiblePermissionList!.map((x) => x)),
        "subjectPublishAble": subjectPublishAble  ,
        "publishPermissionList": publishPermissionList == null ? null : List<String>.from(publishPermissionList!.map((x) => x)),
        "replyPublishAble": replyPublishAble  ,
        "replyPermissionList": replyPermissionList == null ? null : List<String>.from(replyPermissionList!.map((x) => x)),
        "indexListStyle": indexListStyle ,
        "forumIndexStyle": forumIndexStyle  ,
        "subjectType": subjectType  ,
        "typeCategory": typeCategory  ,
        "indexRecommendable": indexRecommendable  ,
        "subjectNeedAudit": subjectNeedAudit  ,
        "replyNeedAudit": replyNeedAudit  ,
        "sectionCreateAble": sectionCreateAble  ,
        "sectionTotal": sectionTotal  ,
        "subjectTotal": subjectTotal  ,
        "replyTotal": replyTotal ,
        "subjectTotalToday": subjectTotalToday  ,
        "replyTotalToday": replyTotalToday  ,
        "creatorName": creatorName  ,
        "forumColor": forumColor  ,
        "forumStatus": forumStatus  ,
        "orderNumber": orderNumber ,
        "replyMessageNotify": replyMessageNotify  ,
        "replyMessageNotifyType": replyMessageNotifyType  ,
        "subjectMessageNotify": subjectMessageNotify  ,
        "subjectMessageNotifyType": subjectMessageNotifyType,
        "createTime": createTime  ,
        "updateTime": updateTime ,
    };
}

///
/// 論壇板塊
/// 
class SectionInfoList {
    SectionInfoList({
        this.id,
        this.sectionName,
        this.forumId,
        this.forumName,
        this.mainSectionId,
        this.mainSectionName,
        this.sectionLevel,
        this.sectionDescription,
        this.icon,
        this.sectionNotice,
        this.sectionVisible,
        this.visiblePermissionList,
        this.subjectPublishAble,
        this.publishPermissionList,
        this.replyPublishAble,
        this.replyPermissionList,
        this.moderatorNames,
        this.subjectTypeList,
        this.sectionType,
        this.subjectType,
        this.typeCategory,
        this.indexRecommendable,
        this.subjectNeedAudit,
        this.replyNeedAudit,
        this.subSectionCreateAble,
        this.subjectTotal,
        this.replyTotal,
        this.subjectTotalToday,
        this.replyTotalToday,
        this.creatorName,
        this.sectionStatus,
        this.orderNumber,
        this.replyMessageNotify,
        this.replyMessageNotifyType,
        this.subjectMessageNotify,
        this.subjectMessageNotifyType,
        this.sectionGrade,
        this.createTime,
        this.updateTime,
    });

    String? id;
    String? sectionName;
    String? forumId;
    String? forumName;
    String? mainSectionId;
    String? mainSectionName;
    String? sectionLevel;
    String? sectionDescription;
    String? icon;
    String? sectionNotice;
    String? sectionVisible;
    List<String>? visiblePermissionList;
    String? subjectPublishAble;
    List<String>? publishPermissionList;
    String? replyPublishAble;
    List<String>? replyPermissionList;
    List<String>? moderatorNames;
    List<String>? subjectTypeList;
    String? sectionType;
    String? subjectType;
    String? typeCategory;
    bool? indexRecommendable;
    bool? subjectNeedAudit;
    bool? replyNeedAudit;
    bool? subSectionCreateAble;
    int? subjectTotal;
    int? replyTotal;
    int? subjectTotalToday;
    int? replyTotalToday;
    String? creatorName;
    String? sectionStatus;
    int? orderNumber;
    bool? replyMessageNotify;
    String? replyMessageNotifyType;
    bool? subjectMessageNotify;
    String? subjectMessageNotifyType;
    bool? sectionGrade;
    String? createTime;
    String? updateTime;

    factory SectionInfoList.fromJson(Map<String, dynamic> json) => SectionInfoList(
        id: json["id"] ,
        sectionName: json["sectionName"]  ,
        forumId: json["forumId"]  ,
        forumName: json["forumName"]  ,
        mainSectionId: json["mainSectionId"] ,
        mainSectionName: json["mainSectionName"]  ,
        sectionLevel: json["sectionLevel"]  ,
        sectionDescription: json["sectionDescription"]  ,
        icon: json["icon"] ,
        sectionNotice: json["sectionNotice"]  ,
        sectionVisible: json["sectionVisible"] ,
        visiblePermissionList: json["visiblePermissionList"] == null ? null : List<String>.from(json["visiblePermissionList"].map((x) => x)),
        subjectPublishAble: json["subjectPublishAble"] ,
        publishPermissionList: json["publishPermissionList"] == null ? null : List<String>.from(json["publishPermissionList"].map((x) => x)),
        replyPublishAble: json["replyPublishAble"] ,
        replyPermissionList: json["replyPermissionList"] == null ? null : List<String>.from(json["replyPermissionList"].map((x) => x)),
        moderatorNames: json["moderatorNames"] == null ? null : List<String>.from(json["moderatorNames"].map((x) => x)),
        subjectTypeList: json["subjectTypeList"] == null ? null : List<String>.from(json["subjectTypeList"].map((x) => x)),
        sectionType: json["sectionType"]  ,
        subjectType: json["subjectType"] ,
        typeCategory: json["typeCategory"]  ,
        indexRecommendable: json["indexRecommendable"]  ,
        subjectNeedAudit: json["subjectNeedAudit"]  ,
        replyNeedAudit: json["replyNeedAudit"]  ,
        subSectionCreateAble: json["subSectionCreateAble"]  ,
        subjectTotal: json["subjectTotal"]  ,
        replyTotal: json["replyTotal"]  ,
        subjectTotalToday: json["subjectTotalToday"] ,
        replyTotalToday: json["replyTotalToday"] ,
        creatorName: json["creatorName"]  ,
        sectionStatus: json["sectionStatus"]  ,
        orderNumber: json["orderNumber"] ,
        replyMessageNotify: json["replyMessageNotify"]  ,
        replyMessageNotifyType: json["replyMessageNotifyType"]  ,
        subjectMessageNotify: json["subjectMessageNotify"] ,
        subjectMessageNotifyType: json["subjectMessageNotifyType"]  ,
        sectionGrade: json["sectionGrade"]  ,
        createTime: json["createTime"]  ,
        updateTime: json["updateTime"]  ,
    );

    Map<String, dynamic> toJson() => {
        "id": id  ,
        "sectionName": sectionName ,
        "forumId": forumId  ,
        "forumName": forumName  ,
        "mainSectionId": mainSectionId  ,
        "mainSectionName": mainSectionName ,
        "sectionLevel": sectionLevel  ,
        "sectionDescription": sectionDescription ,
        "icon": icon,
        "sectionNotice": sectionNotice  ,
        "sectionVisible": sectionVisible  ,
        "visiblePermissionList": visiblePermissionList == null ? null : List<String>.from(visiblePermissionList!.map((x) => x)),
        "subjectPublishAble": subjectPublishAble ,
        "publishPermissionList": publishPermissionList == null ? null : List<String>.from(publishPermissionList!.map((x) => x)),
        "replyPublishAble": replyPublishAble  ,
        "replyPermissionList": replyPermissionList == null ? null : List<String>.from(replyPermissionList!.map((x) => x)),
        "moderatorNames": moderatorNames == null ? null : List<String>.from(moderatorNames!.map((x) => x)),
        "subjectTypeList": subjectTypeList == null ? null : List<String>.from(subjectTypeList!.map((x) => x)),
        "sectionType": sectionType  ,
        "subjectType": subjectType  ,
        "typeCategory": typeCategory  ,
        "indexRecommendable": indexRecommendable  ,
        "subjectNeedAudit": subjectNeedAudit  ,
        "replyNeedAudit": replyNeedAudit ,
        "subSectionCreateAble": subSectionCreateAble  ,
        "subjectTotal": subjectTotal ,
        "replyTotal": replyTotal  ,
        "subjectTotalToday": subjectTotalToday  ,
        "replyTotalToday": replyTotalToday ,
        "creatorName": creatorName ,
        "sectionStatus": sectionStatus ,
        "orderNumber": orderNumber  ,
        "replyMessageNotify": replyMessageNotify  ,
        "replyMessageNotifyType": replyMessageNotifyType  ,
        "subjectMessageNotify": subjectMessageNotify ,
        "subjectMessageNotifyType": subjectMessageNotifyType  ,
        "sectionGrade": sectionGrade ,
        "createTime": createTime  ,
        "updateTime": updateTime  ,
    };
}
