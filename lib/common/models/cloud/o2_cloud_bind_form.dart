
class O2CloudBindData {
  String? unit;  //上一步 选择的公司名称
  String? mobile; //手机号码
  String? code; //验证码
  String? name; //设备号 友盟的token 中心服务器推送消息需要
  String? deviceType; //设备类型  android ios
  String? account; //账号 就是手机号码对应的id


  O2CloudBindData(
      {this.unit,
      this.mobile,
      this.code,
      this.name,
      this.deviceType,
      this.account,
      });

  O2CloudBindData.fromJson(Map<String, dynamic> jsonMap) {
    unit = jsonMap['unit'];
    mobile = jsonMap['mobile'];
    code = jsonMap['code'];
    name = jsonMap['name'];
    deviceType = jsonMap['deviceType'];
    account = jsonMap['account'];
     
  }

  Map<String, dynamic> toJson() => {
        "unit": unit,
        "mobile": mobile,
        "code": code,
        "name": name,
        "deviceType": deviceType,
        "account": account,
      };
}