///
/// flutter和h5交互消息对象
///
class JsMessage {
  String? type;
  String? callback;
  Map<String, dynamic>? data;

  JsMessage.fromJson(Map<String, dynamic> jsonMap) {
    type = jsonMap['type'];
    callback = jsonMap['callback'];
    data = jsonMap['data'];
  }
}

///
/// alert
///
class JsNotificationAlertMessage {
  String? message;
  String? title;
  String? buttonName;
  JsNotificationAlertMessage.fromJson(Map<String, dynamic> jsonMap) {
    message = jsonMap['message'];
    title = jsonMap['title'];
    buttonName = jsonMap['buttonName'];
  }
}

class JsNotificationConfirmMessage {
  String? message;
  String? title;
  List<String>? buttonLabels;
  JsNotificationConfirmMessage.fromJson(Map<String, dynamic> jsonMap) {
    message = jsonMap['message'];
    title = jsonMap['title'];
    buttonLabels = jsonMap['buttonLabels'] == null
        ? null
        : List<String>.from(jsonMap["buttonLabels"].map((x) => x));
  }
}

///
/// actionSheet
///
class JsNotificationActionSheetMessage {
  String? title;
  String? cancelButton;
  List<String>? otherButtons;
  JsNotificationActionSheetMessage.fromJson(Map<String, dynamic> jsonMap) {
    cancelButton = jsonMap['cancelButton'];
    title = jsonMap['title'];
    otherButtons = jsonMap['otherButtons'] == null
        ? null
        : List<String>.from(jsonMap["otherButtons"].map((x) => x));
  }
}

///
/// toast
///
class JsNotificationToastMessage {
  String? message;

  JsNotificationToastMessage.fromJson(Map<String, dynamic> jsonMap) {
    message = jsonMap['message'];
  }
}

///
/// Loading
///
class JsNotificationLoadingMessage {
  String? text;

  JsNotificationLoadingMessage.fromJson(Map<String, dynamic> jsonMap) {
    text = jsonMap['text'];
  }
}

/// datePicker 等  传参用
class JsUtilDatePickerMessage {
  String? value;
  JsUtilDatePickerMessage.fromJson(Map<String, dynamic> jsonMap) {
    value = jsonMap['value'];
  }
}

///  timePicker 等  传参用
class JsUtilTimePickerMessage {
  String? value;
  bool? withSecond;
  JsUtilTimePickerMessage.fromJson(Map<String, dynamic> jsonMap) {
    value = jsonMap['value'];
    withSecond = jsonMap['withSecond'];
  }
}

///  _chooseInterval 等  传参用
class JsUtilDateIntervalPickerMessage {
  String? startDate;
  String? endDate;
  JsUtilDateIntervalPickerMessage.fromJson(Map<String, dynamic> jsonMap) {
    startDate = jsonMap['startDate'];
    endDate = jsonMap['endDate'];
  }
}

/// 定位结果反馈对象
class JsDevicePhoneInfoResponse {
  String? screenWidth;
  String? screenHeight;
  String? brand;
  String? model;
  String? version;
  String? netInfo;
  String? operatorType;
  JsDevicePhoneInfoResponse({
    this.screenWidth,
    this.screenHeight,
    this.brand,
    this.model,
    this.version,
    this.netInfo,
    this.operatorType
  });
  Map<String, dynamic> toJson() => {
        "screenWidth": screenWidth,
        "screenHeight": screenHeight,
        "brand": brand,
        "model": model,
        "version": version,
        "netInfo": netInfo,
        "operatorType": operatorType,
      };
}

/// 定位结果反馈对象
class JsDeviceLocationResponse {
  String? latitude;
  String? longitude;
  String? address;
  JsDeviceLocationResponse({
    this.latitude,
    this.longitude,
    this.address
  });
  Map<String, dynamic> toJson() => {
        "latitude": latitude,
        "longitude": longitude,
        "address": address,
      };
}

/// 设置标题
class JsUtilNavigationMessage {
  String? title;
  JsUtilNavigationMessage.fromJson(Map<String, dynamic> jsonMap) {
    title = jsonMap['title'];
  }
}

/// 打开其他 app 传入对象 
class JsUtilOtherAppMessage {
  String? schema; // 调起其他 app 的 schema
  JsUtilOtherAppMessage.fromJson(Map<String, dynamic> jsonMap) {
    schema = jsonMap['schema'];
  }
}
/// 打开新窗口 传入对象 
class JsUtilOpenWindowMessage {
  String? url; // 新窗口访问地址
  JsUtilOpenWindowMessage.fromJson(Map<String, dynamic> jsonMap) {
    url = jsonMap['url'];
  }
}
/// 内部原生应用打开对象
class JsUtilOpenInnerAppMessage {
  String? appKey; // 对应用的 key:  task(待办)、taskcompleted(已办)、read(待阅)、readcompleted(已阅)、meeting(会议管理)、clouddisk(网盘)、bbs(论坛)、cms(信息中心)、attendance(考勤)、calendar(日程)、mindMap(脑图)、portal(门户，门户需要传入portalFlag和portalTitle) 
  String? appDisplayName; // 应用显示名称
  String? portalFlag; // 门户标识
  String? portalTitle; // 门户标题
  String? portalPage; // 门户页面 id
  JsUtilOpenInnerAppMessage.fromJson(Map<String, dynamic> jsonMap) {
    appKey = jsonMap['appKey'];
    appDisplayName = jsonMap['appDisplayName'];
    portalFlag = jsonMap['portalFlag'];
    portalTitle = jsonMap['portalTitle'];
    portalPage = jsonMap['portalPage'];
  }
}


///  选择器对象
class JsBizPickerMessage {
  // pickMode: ["departmentPicker", "identityPicker"], //Array[String] 选择器类型，可传入值：departmentPicker、identityPicker、groupPicker、personPicker
  List<String>? pickMode; 
  // 顶层组织
  List<String>? topList; 
  // 可选择的组织类别。为空就是全部组织类型都可以
  String? orgType; 
  // 是否多选
  bool? multiple; 
  // 当multiple为true的时候，最多可选择的数量 0不限制
  int? maxNumber; 
  // 已经选择的部门的distinguishedName列表
  List<String>? pickedDepartments; 
  // 已经选择的身份的distinguishedName列表
  List<String>? pickedIdentities; 
  // 已经选择的人员的distinguishedName列表
  List<String>? pickedUsers; 
  // 已经选择的群组的distinguishedName列表
  List<String>? pickedGroups; 
  // 可选择的人员职责
  List<String>? duty; 

  JsBizPickerMessage.fromJson(Map<String, dynamic> jsonMap) {
    pickMode = jsonMap["pickMode"] == null ? null : List<String>.from(jsonMap["pickMode"].map((x) => x ));
    topList = jsonMap["topList"] == null ? null : List<String>.from(jsonMap["topList"].map((x) => x ));
    orgType = jsonMap['orgType'] ?? '';
    multiple = jsonMap['multiple'] ?? true;
    maxNumber = jsonMap['maxNumber'] ?? 0;
    pickedDepartments = jsonMap["pickedDepartments"] == null ? null : List<String>.from(jsonMap["pickedDepartments"].map((x) => x ));
    pickedIdentities = jsonMap["pickedIdentities"] == null ? null : List<String>.from(jsonMap["pickedIdentities"].map((x) => x ));
    pickedUsers = jsonMap["pickedUsers"] == null ? null : List<String>.from(jsonMap["pickedUsers"].map((x) => x ));
    pickedGroups = jsonMap["pickedGroups"] == null ? null : List<String>.from(jsonMap["pickedGroups"].map((x) => x ));
    duty = jsonMap["duty"] == null ? null : List<String>.from(jsonMap["duty"].map((x) => x ));
  }
}