 
/// 分享查询的对象
class ShareFileInfo {
    String? contentType;
    String? id;
    String? person;
    String? name;
    String? fileId; // 文件 id
    String? fileType; // folder attachment
    String? extension;
    int? length;
    String? shareType;
    String? validTime;
    String? lastUpdateTime;
    List<String>? shareUserList;
    List<String>? shareOrgList;
    List<String>? shareGroupList;
    List<String>? shieldUserList;
    String? createTime;
    String? updateTime;

    ShareFileInfo({
        this.contentType,
        this.id,
        this.person,
        this.name,
        this.fileId,
        this.fileType,
        this.extension,
        this.length,
        this.shareType,
        this.validTime,
        this.lastUpdateTime,
        this.shareUserList,
        this.shareOrgList,
        this.shareGroupList,
        this.shieldUserList,
        this.createTime,
        this.updateTime,
    });

    factory ShareFileInfo.fromJson(Map<String, dynamic> json) => ShareFileInfo(
        contentType: json["contentType"],
        id: json["id"],
        person: json["person"],
        name: json["name"],
        fileId: json["fileId"],
        fileType: json["fileType"],
        extension: json["extension"],
        length: json["length"],
        shareType: json["shareType"],
        validTime: json["validTime"] ,
        lastUpdateTime: json["lastUpdateTime"],
        shareUserList: json["shareUserList"] == null ? [] : List<String>.from(json["shareUserList"]!.map((x) => x)),
        shareOrgList: json["shareOrgList"] == null ? [] : List<String>.from(json["shareOrgList"]!.map((x) => x)),
        shareGroupList: json["shareGroupList"] == null ? [] : List<String>.from(json["shareGroupList"]!.map((x) => x)),
        shieldUserList: json["shieldUserList"] == null ? [] : List<String>.from(json["shieldUserList"]!.map((x) => x)),
        createTime: json["createTime"] ,
        updateTime: json["updateTime"],
    );

    Map<String, dynamic> toJson() => {
        "contentType": contentType,
        "id": id,
        "person": person,
        "name": name,
        "fileId": fileId,
        "fileType": fileType,
        "extension": extension,
        "length": length,
        "shareType": shareType,
        "validTime": validTime,
        "lastUpdateTime": lastUpdateTime,
        "shareUserList": shareUserList,
        "shareOrgList": shareOrgList ,
        "shareGroupList": shareGroupList ,
        "shieldUserList": shieldUserList ,
        "createTime": createTime,
        "updateTime": updateTime,
    };
}
