
class FileSearchPost{
  String? fileType;

  FileSearchPost({this.fileType});

  Map<String, dynamic> toJson() => {
    "fileType": fileType
  };
}