
class FolderInfo {
    FolderInfo({
        this.attachmentCount,
        this.size,
        this.folderCount,
        this.id,
        this.person,
        this.name,
        this.superior,
        this.status,
        this.lastUpdateTime,
        this.createTime,
        this.updateTime,
        this.fileId,

        this.isAdmin, 
        this.isEditor,
        this.isZone,
        this.zoneId
    });

    int? attachmentCount;
    int? size;
    int? folderCount;
    String? id;
    String? person;
    String? name;
    String? superior;
    String? status;
    String? lastUpdateTime;
    String? createTime;
    String? updateTime;
    String? fileId; //分享对象的时候这个代表文件原始id

     // v3 版本字段
    bool? isAdmin;
    bool? isEditor;
    bool? isZone;
    String? zoneId;

    factory FolderInfo.fromJson(Map<String, dynamic> json) => FolderInfo(
        attachmentCount: json["attachmentCount"],
        size: json["size"],
        folderCount: json["folderCount"],
        id: json["id"],
        person: json["person"],
        name: json["name"],
        superior: json["superior"],
        status: json["status"],
        lastUpdateTime: json["lastUpdateTime"],
        createTime: json["createTime"],
        updateTime: json["updateTime"],
        fileId: json["fileId"],
        isAdmin: json["isAdmin"],
        isEditor: json["isEditor"],
        isZone: json["isZone"],
        zoneId: json["zoneId"],
    );

    Map<String, dynamic> toJson() => {
        "attachmentCount": attachmentCount,
        "size": size,
        "folderCount": folderCount,
        "id": id,
        "person": person,
        "name": name,
        "superior": superior,
        "status": status,
        "lastUpdateTime": lastUpdateTime,
        "createTime": createTime,
        "updateTime": updateTime,
        "fileId": fileId,
        "isAdmin": isAdmin,
        "isEditor": isEditor,
        "isZone": isZone,
        "zoneId": zoneId,
    };
}