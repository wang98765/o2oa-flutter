import 'cms_category_data.dart';

class CmsAppData {
  CmsAppData({
    this.config,
    this.id,
    this.appName,
    this.appAlias,
    this.appType,
    this.documentType,
    this.appInfoSeq,
    this.description,
    this.appIcon,
    this.iconColor,
    this.creatorPerson,
    this.creatorIdentity,
    this.creatorUnitName,
    this.creatorTopUnitName,
    this.anonymousAble,
    this.allPeopleView,
    this.allPeoplePublish,
    this.sendNotify,
    this.showAllDocuments,
    this.allowWaitPublish,
    this.categoryList,
    this.viewablePersonList,
    this.viewableUnitList,
    this.viewableGroupList,
    this.publishablePersonList,
    this.publishableUnitList,
    this.publishableGroupList,
    this.manageablePersonList,
    this.manageableUnitList,
    this.manageableGroupList,
    this.createTime,
    this.updateTime,
    this.wrapOutCategoryList,
  });
  String? config;
  String? id;
  String? appName;
  String? appAlias;
  String? appType;
  String? documentType;
  String? appInfoSeq;
  String? description;
  String? appIcon;
  String? iconColor;
  String? creatorPerson;
  String? creatorIdentity;
  String? creatorUnitName;
  String? creatorTopUnitName;
  bool? anonymousAble;
  bool? allPeopleView;
  bool? allPeoplePublish;
  bool? sendNotify;
  bool? showAllDocuments;
  bool? allowWaitPublish;
  List<String>? categoryList;
  List<String>? viewablePersonList;
  List<String>? viewableUnitList;
  List<String>? viewableGroupList;
  List<String>? publishablePersonList;
  List<String>? publishableUnitList;
  List<String>? publishableGroupList;
  List<String>? manageablePersonList;
  List<String>? manageableUnitList;
  List<String>? manageableGroupList;
  String? createTime;
  String? updateTime;
  List<CmsCategoryData>? wrapOutCategoryList;

  factory CmsAppData.fromJson(Map<String, dynamic> json) => CmsAppData(
        wrapOutCategoryList: json["wrapOutCategoryList"] == null ? null : List<CmsCategoryData>.from(json["wrapOutCategoryList"].map((x) => CmsCategoryData.fromJson(x))),
        config: json["config"],
        id: json["id"],
        appName: json["appName"],
        appAlias: json["appAlias"],
        appType: json["appType"],
        documentType: json["documentType"],
        appInfoSeq: json["appInfoSeq"],
        description: json["description"],
        appIcon: json["appIcon"],
        iconColor: json["iconColor"],
        creatorPerson: json["creatorPerson"],
        creatorIdentity: json["creatorIdentity"],
        creatorUnitName: json["creatorUnitName"],
        creatorTopUnitName: json["creatorTopUnitName"],
        anonymousAble: json["anonymousAble"],
        allPeopleView: json["allPeopleView"],
        allPeoplePublish: json["allPeoplePublish"],
        sendNotify: json["sendNotify"],
        showAllDocuments: json["showAllDocuments"],
        allowWaitPublish: json["allowWaitPublish"],
        categoryList: json["categoryList"] == null? null : List<String>.from(json["categoryList"].map((x) => x)),
        viewablePersonList: json["viewablePersonList"] == null? null :List<String>.from(json["viewablePersonList"].map((x) => x)),
        viewableUnitList: json["viewableUnitList"] == null? null :List<String>.from(json["viewableUnitList"].map((x) => x)),
        viewableGroupList: json["viewableGroupList"] == null? null :List<String>.from(json["viewableGroupList"].map((x) => x)),
        publishablePersonList: json["publishablePersonList"] == null? null :List<String>.from(json["publishablePersonList"].map((x) => x)),
        publishableUnitList: json["publishableUnitList"] == null? null :List<String>.from(json["publishableUnitList"].map((x) => x)),
        publishableGroupList: json["publishableGroupList"] == null? null :List<String>.from(json["publishableGroupList"].map((x) => x)),
        manageablePersonList: json["manageablePersonList"] == null? null :List<String>.from(json["manageablePersonList"].map((x) => x)),
        manageableUnitList: json["manageableUnitList"] == null? null :List<String>.from(json["manageableUnitList"].map((x) => x)),
        manageableGroupList: json["manageableGroupList"] == null? null :List<String>.from(json["manageableGroupList"].map((x) => x)),
        createTime: json["createTime"],
        updateTime: json["updateTime"],
    );

    Map<String, dynamic> toJson() => {
        "wrapOutCategoryList": wrapOutCategoryList == null? null : List<dynamic>.from(wrapOutCategoryList!.map((x) => x.toJson())),
        "config": config,
        "id": id,
        "appName": appName,
        "appAlias": appAlias,
        "appType": appType,
        "documentType": documentType,
        "appInfoSeq": appInfoSeq,
        "description": description,
        "appIcon": appIcon,
        "iconColor": iconColor,
        "creatorPerson": creatorPerson,
        "creatorIdentity": creatorIdentity,
        "creatorUnitName": creatorUnitName,
        "creatorTopUnitName": creatorTopUnitName,
        "anonymousAble": anonymousAble,
        "allPeopleView": allPeopleView,
        "allPeoplePublish": allPeoplePublish,
        "sendNotify": sendNotify,
        "showAllDocuments": showAllDocuments,
        "allowWaitPublish": allowWaitPublish,
        "categoryList": categoryList == null? null : List<String>.from(categoryList!.map((x) => x)),
        "viewablePersonList": viewablePersonList == null? null : List<String>.from(viewablePersonList!.map((x) => x)),
        "viewableUnitList": viewableUnitList == null? null : List<String>.from(viewableUnitList!.map((x) => x)),
        "viewableGroupList": viewableGroupList == null? null : List<String>.from(viewableGroupList!.map((x) => x)),
        "publishablePersonList": publishablePersonList == null? null : List<String>.from(publishablePersonList!.map((x) => x)),
        "publishableUnitList": publishableUnitList == null? null : List<String>.from(publishableUnitList!.map((x) => x)),
        "publishableGroupList": publishableGroupList == null? null : List<String>.from(publishableGroupList!.map((x) => x)),
        "manageablePersonList": manageablePersonList == null? null : List<String>.from(manageablePersonList!.map((x) => x)),
        "manageableUnitList": manageableUnitList == null? null : List<String>.from(manageableUnitList!.map((x) => x)),
        "manageableGroupList": manageableGroupList == null? null : List<String>.from(manageableGroupList!.map((x) => x)),
        "createTime": createTime,
        "updateTime": updateTime,
    };

}
