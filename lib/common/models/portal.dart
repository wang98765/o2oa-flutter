

class PortalCornerMarkData {

  int? count;

  PortalCornerMarkData({this.count});

  factory PortalCornerMarkData.fromJson(Map<String, dynamic> json) => PortalCornerMarkData(count: json["count"]);
  

}