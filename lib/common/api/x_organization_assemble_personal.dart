



import 'dart:io';

import 'package:get/get.dart';

import '../models/index.dart';
import '../services/index.dart';
import '../utils/index.dart';

///
///组织人员服务
///
class OrganizationPersonalService extends GetxService {
  
  static OrganizationPersonalService get to => Get.find();
  //
  
  String baseUrl() {
    return O2ApiManager.instance.getModuleBaseUrl(
        O2DistributeModuleEnum.x_organization_assemble_personal) ?? '';
  }




  ///
  /// 会议管理的配置文件
  /// 
  Future<MeetingConfigModel?> meetingConfig() async {
    try {
      ApiResponse response = await O2HttpClient.instance
          .get('${baseUrl()}jaxrs/definition/meetingConfig');
      if (response.data != null && response.data is String) {
        SharedPreferenceService.to.putString(SharedPreferenceService.meetingConfigKey, response.data);
        return MeetingConfigModel.fromJson(O2Utils.parseStringToJson(response.data));
      }
    } catch (err, stackTrace) {
      OLogger.e(' 会议管理的配置文件获取失败', err, stackTrace);
    }
    return null;
  }

  ///
  /// 获取当前用户信息
  /// 
  Future<O2Person?> person() async {
    try {
      ApiResponse response = await O2HttpClient.instance
          .get('${baseUrl()}jaxrs/person');
      O2Person idData = O2Person.fromJson(response.data);
      return idData;
    } catch (err, stackTrace) {
      OLogger.e('个人信息获取失败', err, stackTrace);
    }
    return null;
  }

  ///
  /// 修改个人信息
  ///
  Future<IdData?> personEdit(O2Person form) async {
    try {
      ApiResponse response = await O2HttpClient.instance
          .put('${baseUrl()}jaxrs/person', form.toJson());
      IdData idData = IdData.fromJson(response.data);
      return idData;
    } catch (err, stackTrace) {
      OLogger.e('修改个人信息失败', err, stackTrace);
    }
    return null;
  }

  ///
  /// 修改头像
  ///
  Future<ValueBoolData?> updateAvatar(File file) async {
    try {
      ApiResponse response = await O2HttpClient.instance.putUploadFile('${baseUrl()}jaxrs/person/icon', file);
      ValueBoolData valueBoolData = ValueBoolData.fromJson(response.data);
      return valueBoolData;
    } catch (err, stackTrace) {
      OLogger.e('修改个人头像失败', err, stackTrace);
    }
    return null;
  }

  ///
  /// 登录
  ///
  Future<ValueBoolData?> updateLoginPassword(UpdateLoginPasswordForm form) async {
    try {
      ApiResponse response = await O2HttpClient.instance
          .put('${baseUrl()}jaxrs/person/password', form.toJson());
      ValueBoolData idData = ValueBoolData.fromJson(response.data);
      return idData;
    } catch (err, stackTrace) {
      OLogger.e('修改登录失败', err, stackTrace);
    }
    return null;
  }

  ///
  /// 获取个性化数据
  /// 
  Future<String?> getMyCustomData(String name) async {
     try {
      ApiResponse response = await O2HttpClient.instance
          .get('${baseUrl()}jaxrs/custom/$name');
      if (response.data != null && response.data is String) {
        return response.data;
      }
    } catch (err, stackTrace) {
      OLogger.e('获取个性化数据失败， name： $name ', err, stackTrace);
    }
    return null;
  }

  ///
  /// 保存个性化数据
  /// 
  Future<IdData?> saveMyCustomData(String name, Map<String, dynamic> data) async {
     try {
      ApiResponse response = await O2HttpClient.instance
          .put('${baseUrl()}jaxrs/custom/$name', data);
      IdData idData = IdData.fromJson(response.data);
      return idData;
    } catch (err, stackTrace) {
      OLogger.e('保存个性化数据失败， name： $name ', err, stackTrace);
    }
    return null;
  }

  /// 我的委托
  Future<List<EmpowerData>?> myEmpowerList() async {
     try {
      ApiResponse response = await O2HttpClient.instance.get('${baseUrl()}jaxrs/empower/list/currentperson');
      final list = response.data == null ? [] : response.data as List;
      return list.map((group) => EmpowerData.fromJson(group)).toList();
    } catch (err, stackTrace) {
      OLogger.e('获取我的委托列表失败', err, stackTrace);
    }
    return null;
  }
  /// 收到的委托
  Future<List<EmpowerData>?> empowerToMeList() async {
     try {
      ApiResponse response = await O2HttpClient.instance.get('${baseUrl()}jaxrs/empower/list/to');
      final list = response.data == null ? [] : response.data as List;
      return list.map((group) => EmpowerData.fromJson(group)).toList();
    } catch (err, stackTrace) {
      OLogger.e('获取收到的委托列表失败', err, stackTrace);
    }
    return null;
  }
  /// 删除委托
  Future<ValueBoolData?> empowerDelete(String id) async {
     try {
      ApiResponse response = await O2HttpClient.instance.delete('${baseUrl()}jaxrs/empower/$id');
      return ValueBoolData.fromJson(response.data);
    } catch (err, stackTrace) {
      OLogger.e('删除委托失败', err, stackTrace);
    }
    return null;
  }
  /// 创建委托
  Future<IdData?> empowerCreate(EmpowerData body) async {
     try {
      ApiResponse response = await O2HttpClient.instance.post('${baseUrl()}jaxrs/empower', body.toJson());
      return IdData.fromJson(response.data);
    } catch (err, stackTrace) {
      OLogger.e('创建委托失败', err, stackTrace);
    }
    return null;
  }
}