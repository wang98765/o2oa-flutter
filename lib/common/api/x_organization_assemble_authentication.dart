
import 'package:get/get.dart';

import '../models/index.dart';
import '../utils/index.dart';

///
///认证服务
///
class OrgAuthenticationService extends GetxService {
  
  static OrgAuthenticationService get to => Get.find();

  String baseUrl() {
    return O2ApiManager.instance.getModuleBaseUrl(
        O2DistributeModuleEnum.x_organization_assemble_authentication) ?? '';
  }


  
  ///
  /// 扫码登录pc
  /// 
  Future<O2Person?> login2Pc(String meta) async {
    try {
      ApiResponse response = await O2HttpClient.instance.post('${baseUrl()}jaxrs/authentication/bind/meta/$meta', {});
      return O2Person.fromJson(response.data);
    } catch (err, stackTrace) {
      OLogger.e('扫码登录pc失败', err, stackTrace);
    }
    return null;
  }

  ///
  /// 获取服务器开启的登录模式
  /// 
  Future<LoginModeData?> getLoginMode() async {
    try {
      ApiResponse response = await O2HttpClient.instance.get('${baseUrl()}jaxrs/authentication/mode', needToken: false);
      LoginModeData mode = LoginModeData.fromJson(response.data);
      return mode;
    } catch (err, stackTrace) {
      OLogger.e('获取登录模式失败', err, stackTrace);
    }
    return null;
  }

  ///
  ///获取图片验证码
  ///
  Future<CaptchaImgData?> getLoginCaptchaImgData(int width, int height) async {
    try {
      ApiResponse response = await O2HttpClient.instance.get('${baseUrl()}jaxrs/authentication/captcha/width/$width/height/$height', needToken: false);
      CaptchaImgData mode = CaptchaImgData.fromJson(response.data);
      return mode;
    } catch (err, stackTrace) {
      OLogger.e('获取图片验证码失败', err, stackTrace);
    }
    return null;
  }

  ///
  /// 获取加密的公钥
  /// 
  Future<RSAPublicKeyData?> getRSAPublicKey() async {
    try {
      ApiResponse response = await O2HttpClient.instance.get('${baseUrl()}jaxrs/authentication/captchaRSAPublicKey', needToken: false);
      RSAPublicKeyData mode = RSAPublicKeyData.fromJson(response.data);
      return mode;
    } catch (err, stackTrace) {
      OLogger.e('获取加密的公钥失败', err, stackTrace);
    }
    return null;
  }

  ///
  /// 登录
  ///
  Future<O2Person?> login(LoginForm form) async {
    try {
      ApiResponse response = await O2HttpClient.instance
          .post('${baseUrl()}jaxrs/authentication/captcha', form.toJson(), needToken: false);
      //更新用户信息和token
      O2Person person = O2Person.fromJson(response.data);
      await O2ApiManager.instance.setupUser(person);
      return person;
    } catch (err, stackTrace) {
      OLogger.e('登录失败', err, stackTrace);
    }
   
    return null;
  }

  ///
  ///获取短信验证码
  ///
  Future<CollectCodeData?> getPhoneCode(String credential) async {
    try {
      ApiResponse response = await O2HttpClient.instance.get('${baseUrl()}jaxrs/authentication/code/credential/$credential', needToken: false);
      CollectCodeData data = CollectCodeData.fromJson(response.data);
      return data;
    } catch (err, stackTrace) {
      OLogger.e('获取验证码失败', err, stackTrace);
    }
    return null;
  }
 
  ///
  ///手机验证码登录
  ///
  Future<O2Person?> loginWithCode(String credential, String codeAnswer) async {
    try {
      Map<String, String> data = {};
      data['credential'] = credential;
      data['codeAnswer'] = codeAnswer;
      ApiResponse response = await O2HttpClient.instance
          .post('${baseUrl()}jaxrs/authentication/code', data, needToken: false);
      //更新用户信息和token
      O2Person person = O2Person.fromJson(response.data);
      O2ApiManager.instance.setupUser(person);
      return person;
    } catch (err, stackTrace) {
      OLogger.e('登录失败', err, stackTrace);
    }
    return null;
    
  }

  ///
  ///检测当前登录状态
  Future<O2Person?> who() async {
    try {
      ApiResponse response =
        await O2HttpClient.instance.get('${baseUrl()}jaxrs/authentication');
      String token = response.data['token'] as String;
      if (token.isNotEmpty) {
        //更新用户信息和token
        O2Person person = O2Person.fromJson(response.data);
        O2ApiManager.instance.setupUser(person);
        return person;
      } else {
        OLogger.i('错误，token过期！');
        return null;
      }
    } catch (err, stackTrace) {
      OLogger.e('当前登录状态已过期', err, stackTrace);
      return null;
    }
  }

  Future<IdData?> logout()  async {
    try {
      ApiResponse response = await O2HttpClient.instance.delete('${baseUrl()}jaxrs/authentication');
      IdData data = IdData.fromJson(response.data);
      return data;
    } catch (err, stackTrace) {
      OLogger.e('登出失败', err, stackTrace);
      return null;
    }
  }
}
