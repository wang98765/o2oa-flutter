

import 'package:get/get.dart';

import '../models/index.dart';
import '../utils/index.dart';

class AppPackingClientAssembleControlService {
  static AppPackingClientAssembleControlService get to => Get.find();

  String baseUrl() {
    return O2ApiManager.instance
            .getModuleBaseUrl(O2DistributeModuleEnum.x_app_packaging_client_assemble_control) ??
        '';
  }

  String apkDownloadUrl(String fileId, {String? outBaseUrl}) {
    String url = baseUrl();
    if (outBaseUrl != null) {
      url = outBaseUrl;
    }
    return '${url}jaxrs/apppackanony/file/download/$fileId';
  }

  /// 最后一次打包对象
  Future<O2AppInnerUpdateInfo?> androidPackLastAPk({String? outBaseUrl}) async {
    try {
      String url = baseUrl();
      if (outBaseUrl != null) {
        url = outBaseUrl;
      }
      ApiResponse response =
          await O2HttpClient.instance.get('${url}jaxrs/apppackanony/file/type/android/last');
      return O2AppInnerUpdateInfo.fromJson(response.data);
    } catch (err, stackTrace) {
      OLogger.e('配置文件读取失败', err, stackTrace);
    }
    return null;
  }

}