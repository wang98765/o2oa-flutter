 
import 'dart:math';

import 'package:get/get.dart';

import '../models/index.dart';
import '../utils/index.dart';
import '../values/index.dart';
 


///
///组织人员服务
///
class OrganizationControlService extends GetxService {
  
  static OrganizationControlService get to => Get.find();
  //
  
  String baseUrl() {
    return O2ApiManager.instance.getModuleBaseUrl(
        O2DistributeModuleEnum.x_organization_assemble_control) ?? '';
  }

   ///人员头像地址
  String iconUrl(String? person) {
    if (person == null || person.isEmpty) {
      return '';
    }
    final r = Random();
    final s = r.nextInt(1000);
    return '${baseUrl()}jaxrs/person/$person/icon?$s';
  }


  /// 用户对象查询
  /// jaxrs/person/list/like
  ///
  Future<List<O2Person>?> personSearch(String key) async {
    try {
      var body = {
        "key": key
      };
      ApiResponse response =
      await O2HttpClient.instance.put('${baseUrl()}jaxrs/person/list/like', body);
      var list = response.data == null ? [] : response.data as List;
      return list.map((i) => O2Person.fromJson(i)).toList();
    } catch (err, stackTrace) {
      OLogger.e('搜索用户失败', err, stackTrace);
    }
    return null;
  }
  /// 用户对象查询
  /// jaxrs/person/{person}
  ///
  Future<O2Person?> person(String person) async {
    try {
      ApiResponse response =
      await O2HttpClient.instance.get('${baseUrl()}jaxrs/person/$person');
      return O2Person.fromJson(response.data);
    } catch (err, stackTrace) {
      OLogger.e('查询用户对象失败', err, stackTrace);
    }
    return null;
  }

  ///顶级组织
  ///jaxrs/unit/list/top
  ///
  Future<List<O2Unit>?> unitTopList() async {
    try {
      ApiResponse response =
      await O2HttpClient.instance.get('${baseUrl()}jaxrs/unit/list/top');
      var list = response.data == null ? [] : response.data as List;
      return list.map((group) => O2Unit.fromJson(group)).toList();
    } catch (err, stackTrace) {
      OLogger.e('查询顶级组织失败', err, stackTrace);
    }
    return null;
  }

  ///
  /// 下级组织列表
  /// jaxrs/unit/list/{UnitDistinguishedName}/sub/direct
  ///
  Future<List<O2Unit>?> unitListWithParent(String parentUnit) async {
    try {
      ApiResponse response =
      await O2HttpClient.instance.get('${baseUrl()}jaxrs/unit/list/$parentUnit/sub/direct');
      var list = response.data == null ? [] : response.data as List;
      return list.map((group) => O2Unit.fromJson(group)).toList();
    } catch (err, stackTrace) {
      OLogger.e('查询下级组织列表失败', err, stackTrace);
    }
    return null;
  }
  
  ///
  /// 组织下身份列表
  ////jaxrs/identity/list/unit/{UnitDistinguishedName}
  Future<List<O2Identity>?> identityListWithUnit(String unit) async {
    try {
      ApiResponse response =
      await O2HttpClient.instance.get('${baseUrl()}jaxrs/identity/list/unit/$unit');
      var list = response.data == null ? [] : response.data as List;
      return list.map((group) => O2Identity.fromJson(group)).toList();
    } catch (err, stackTrace) {
      OLogger.e('查询组织下身份列表失败', err, stackTrace);
    }
    return null;
  }

  ///
  /// 列示人员的所有身份信息
  /// 	jaxrs/identity/list/person/
  /// 
  Future<List<O2Identity>?> identityListByPerson(String person) async {
    try {
      ApiResponse response =
      await O2HttpClient.instance.get('${baseUrl()}jaxrs/identity/list/person/$person');
      var list = response.data == null ? [] : response.data as List;
      return list.map((i) => O2Identity.fromJson(i)).toList();
    } catch (err, stackTrace) {
      OLogger.e('列示人员的所有身份信息失败', err, stackTrace);
    }
    return null;
  }


    ///
    /// 根据请求id返回组织列表
    /// @param body {"unitList":[]}
    ///
    Future<List<O2Unit>?> unitList(List<String> unitList) async {
    try {
      final body = {
        'unitList': unitList
      };
      ApiResponse response =
      await O2HttpClient.instance.post('${baseUrl()}jaxrs/unit/list', body);
      var list = response.data == null ? [] : response.data as List;
      return list.map((group) => O2Unit.fromJson(group)).toList();
    } catch (err, stackTrace) {
      OLogger.e('根据请求id返回组织失败', err, stackTrace);
    }
    return null;
  }


    ///
    /// 根据组织类型查询组织， 如果没有组织类型 用上面两个接口 ，有组织类型就用这个接口
    /// body：{"type":"一级部门","unitList":[]}
    ///
    Future<List<O2Unit>?> unitListByType(String type) async {
    try {
      final body = {
        'type': type
      };
      ApiResponse response =
      await O2HttpClient.instance.post('${baseUrl()}jaxrs/unit/list', body);
      var list = response.data == null ? [] : response.data as List;
      return list.map((group) => O2Unit.fromJson(group)).toList();
    } catch (err, stackTrace) {
      OLogger.e('组织类型查询组织失败', err, stackTrace);
    }
    return null;
  }



  /// 分页查询 群组列表
  /// 
  /// [lastId] 传入列表最后一个对象的id， 第一页传默认 O2.o2DefaultPageFirstKey 
  /// [limit] 每页数量
  Future<List<O2Group>?> getGroupListByPage(String lastId, { int limit = O2.o2DefaultPageSize}) async {
    try {
      ApiResponse response =
      await O2HttpClient.instance.get('${baseUrl()}jaxrs/group/list/$lastId/next/$limit');
      var list = response.data == null ? [] : response.data as List;
      return list.map((group) => O2Group.fromJson(group)).toList();
    } catch (err, stackTrace) {
      OLogger.e('查询群组列表失败', err, stackTrace);
    }
    return null;
  }

  
}
