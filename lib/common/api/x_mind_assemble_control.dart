import 'dart:convert';

import 'package:get/get.dart';

import '../models/index.dart';
import '../utils/index.dart';
import '../values/index.dart';

class MindMapService extends GetxService {

  static MindMapService get to => Get.find();

  String baseUrl() {
    return O2ApiManager.instance
            .getModuleBaseUrl(O2DistributeModuleEnum.x_mind_assemble_control) ??
        '';
  }

  ///
  /// 我的文件
  ///
  Future<List<MindFolder>?> myFolderTree() async {
    try {
      ApiResponse response =
          await O2HttpClient.instance.get('${baseUrl()}jaxrs/folder/tree/my');
      var list = response.data == null ? [] : response.data as List;
      return list.map((folder) => MindFolder.fromJson(folder)).toList();
    } catch (err, stackTrace) {
      OLogger.e('获取文件夹失败', err, stackTrace);
    }
    return null;
  }

  ///
  /// 获取脑图详细信息
  ///
  Future<MindMap?> mindMap(String id) async {
    try {
      ApiResponse response =
          await O2HttpClient.instance.get('${baseUrl()}jaxrs/mind/view/$id');
      return MindMap.fromJson(response.data);
    } catch (err, stackTrace) {
      OLogger.e('获取脑图失败', err, stackTrace);
    }
    return null;
  }

  ///
  /// 保存脑图数据
  ///
  Future<String?> saveMindMap(MindMap? map, MindMapData? data) async {
    if (map == null || data == null) {
      OLogger.e('无法保存，传入参数异常！');
      return null;
    }
    String content = json.encode(data.toJson());
    map.content = content;
    try {
      ApiResponse response = await O2HttpClient.instance
          .post('${baseUrl()}jaxrs/mind/save', map.toJson());
      IdData idData = IdData.fromJson(response.data);
      return idData.id!;
    } catch (err, stackTrace) {
      OLogger.e('保存脑图失败', err, stackTrace);
    }
    return null;
  }

  ///
  /// 保存脑图数据
  ///
  Future<String?> renameMindMap(MindMap? map) async {
    if (map == null) {
      // throw O2ValidateError('无法保存，传入参数异常！');
      OLogger.e('无法保存，传入参数异常！');
      return null;
    }
    try {
      ApiResponse response = await O2HttpClient.instance
          .post('${baseUrl()}jaxrs/mind/save', map.toJson());
      IdData idData = IdData.fromJson(response.data);
      return idData.id!;
    } catch (err, stackTrace) {
      OLogger.e('保存脑图失败', err, stackTrace);
    }
    return null;
  }

  ///
  /// 删除脑图文件
  ///
  Future<bool> deleteMindMap(String mapId) async {
    try {
      await O2HttpClient.instance
          .delete('${baseUrl()}jaxrs/mind/recycle/$mapId');
      return true;
    } catch (err, stackTrace) {
      OLogger.e('删除脑图失败', err, stackTrace);
    }
    return false;
  }

  ///
  /// 分页查询脑图列表
  /// @param lastId 上一页最后一个id
  /// @param folderId 所属文件夹
  ///
  Future<List<MindMap>?> mindFilterByPage(
      String lastId, String folderId) async {
    try {
      Map<String, String> data = {};
      data['folderId'] = folderId;
      ApiResponse response = await O2HttpClient.instance.put(
          '${baseUrl()}jaxrs/mind/filter/list/$lastId/next/${O2.o2DefaultPageSize}',
          data);
      var list = response.data == null ? [] : response.data as List;
      return list.map((folder) => MindMap.fromJson(folder)).toList();
    } catch (err, stackTrace) {
      OLogger.e('获取脑图列表失败', err, stackTrace);
    }
    return null;
  }

  ///
  /// 新增修改目录
  ///
  Future<String?> saveMindFolder(String name, String parentId,
      {String? id}) async {
    try {
      Map<String, String> data = {};
      data['name'] = name;
      data['parentId'] = parentId;
      if (id != null) {
        data['id'] = id;
      }
      ApiResponse response = await O2HttpClient.instance
          .post('${baseUrl()}jaxrs/folder/save', data);
      IdData idData = IdData.fromJson(response.data);
      return idData.id!;
    } catch (err, stackTrace) {
      OLogger.e('保存目录失败', err, stackTrace);
    }
    return null;
  }

  ///
  /// 删除目录
  ///
  Future<bool> deleteMindFolder(String id) async {
    try {
      await O2HttpClient.instance.delete('${baseUrl()}jaxrs/folder/$id');
      return true;
    } catch (err, stackTrace) {
      OLogger.e('删除目录失败', err, stackTrace);
    }
    return false;
  }
}
