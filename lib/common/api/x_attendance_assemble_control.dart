



import 'package:get/get.dart';

import '../models/index.dart';
import '../services/index.dart';
import '../utils/index.dart';
import '../values/index.dart';

///
/// 考勤服务
///
class AttendanceAssembleControlService extends GetxService {
  static AttendanceAssembleControlService get to => Get.find();

  String baseUrl() {
    return O2ApiManager.instance
            .getModuleBaseUrl(O2DistributeModuleEnum.x_attendance_assemble_control) ??
        '';
  }
  /// 是否关闭旧考勤
  bool closeOldAttendance() {
    return SharedPreferenceService.to.getBool(SharedPreferenceService.v2AttendanceKey);
  }

  //////////// v2 ///////
  

  /// 考勤配置文件读取 
  Future<AttendanceV2Config?> config() async {
    try {
      ApiResponse response =
          await O2HttpClient.instance.get('${baseUrl()}jaxrs/v2/config');
      return AttendanceV2Config.fromJson(response.data);
    } catch (err, stackTrace) {
      OLogger.e('配置文件读取失败', err, stackTrace);
    }
    return null;
  }
  ///
  ///  打卡
  /// 
  Future<CheckInResponse?> checkInPostV2(CheckPost body) async {
    try {
      ApiResponse response =
          await O2HttpClient.instance.post('${baseUrl()}jaxrs/v2/mobile/check', body.toJson());
      return CheckInResponse.fromJson(response.data);
    } catch (err, stackTrace) {
      OLogger.e('打卡失败', err, stackTrace);
    }
    return null;
  }



  ///
  /// 打卡前获取打卡需要信息的请求.
  /// 包含了需要打卡的数据和工作场所列表等
  /// 
  Future<PreCheckInData?> loadPreCheckInData() async {
    try {
      ApiResponse response =
          await O2HttpClient.instance.get('${baseUrl()}jaxrs/v2/mobile/check/pre');
      return PreCheckInData.fromJson(response.data);
    } catch (err, stackTrace) {
      OLogger.e('查询打卡记录失败', err, stackTrace);
    }
    return null;
  }
  ///  查询我的统计数据
  /// 
  /// 根据[startDate]和[endDate]统计计算 
  Future<AttendanceV2Statistic?> myStatistic(String startDate, String endDate) async {
    try {
      final body = AttendanceV2RequestStatistic(endDate: endDate, startDate: startDate);
      ApiResponse response =
          await O2HttpClient.instance.post('${baseUrl()}jaxrs/v2/my/statistic', body.toJson());
      return AttendanceV2Statistic.fromJson(response.data);
    } catch (err, stackTrace) {
      OLogger.e('查询打卡记录失败', err, stackTrace);
    }
    return null;
  }

  /// 分页查我的异常打卡数据列表
  Future<List<AttendanceV2Appeal>?> myAppealListByPage(int page, {int limit = O2.o2DefaultPageSize, Map<String, dynamic>? body}) async {
    try {
      Map<String, dynamic> data = {};
      if (body != null) {
        data = body;
      }
      ApiResponse response = await O2HttpClient.instance.post('${baseUrl()}jaxrs/v2/appeal/list/$page/size/$limit', data);
      var list = response.data == null ? [] : response.data as List;
      return list.map((d) => AttendanceV2Appeal.fromJson(d)).toList();
    } catch (err, stackTrace) {
      OLogger.e('查询异常打卡数据列表失败', err, stackTrace);
    }
    return null;
  }
  ///  检查是否能够申诉
  Future<ValueBoolData?>  checkAppealEnable(String id) async {
    try {
       
      ApiResponse response = await O2HttpClient.instance.get('${baseUrl()}jaxrs/v2/appeal/$id/start/check');
      return ValueBoolData.fromJson(response.data);
    } catch (err, stackTrace) {
      OLogger.e('查询数据是否能够申诉失败', err, stackTrace);
    }
    return null;
  } 
  /// 启动流程更新数据
  Future<ValueBoolData?>  appealStartProcess(String id, String jobId) async {
    try {
      Map<String, dynamic> data = {
        "job": jobId
      };
      ApiResponse response = await O2HttpClient.instance.post('${baseUrl()}jaxrs/v2/appeal/$id/start/process', data);
      return ValueBoolData.fromJson(response.data);
    } catch (err, stackTrace) {
      OLogger.e('更新数据失败', err, stackTrace);
    }
    return null;
  }
  /// 还原数据状态 清空 job
  Future<ValueBoolData?>  appealRestStatus(String id) async {
    try {
      ApiResponse response = await O2HttpClient.instance.get('${baseUrl()}jaxrs/v2/appeal/$id/reset/status');
      return ValueBoolData.fromJson(response.data);
    } catch (err, stackTrace) {
      OLogger.e('还原数据状态失败', err, stackTrace);
    }
    return null;
  }

//////////////////////////////////////////////////

  ///
  /// 获取当天的打卡记录
  /// 包含了考勤配置信息
  /// 
  Future<ScheduleInfo?> listMyRecords() async {
    try {
      ApiResponse response =
          await O2HttpClient.instance.get('${baseUrl()}jaxrs/attendancedetail/mobile/my');
      return ScheduleInfo.fromJson(response.data);
    } catch (err, stackTrace) {
      OLogger.e('查询打卡记录失败', err, stackTrace);
    }
    return null;
  }


  ///
  /// 查询工作打卡地点信息
  /// 
  Future<List<WorkplaceInfo>?> 	listAllAttendanceWorkPlace() async {
    try {
      ApiResponse response =
          await O2HttpClient.instance.get('${baseUrl()}jaxrs/workplace/list/all');
      var list = response.data == null ? [] : response.data as List;
      return list.map((group) => WorkplaceInfo.fromJson(group)).toList();
    } catch (err, stackTrace) {
      OLogger.e('查询工作打卡地点信息失败', err, stackTrace);
    }
    return null;
  }

  ///  打卡
  Future<IdData?> checkInPost(MobileCheckPost body) async {
    try {
      ApiResponse response =
          await O2HttpClient.instance.post('${baseUrl()}jaxrs/attendancedetail/mobile/recive', body.toJson());
      return IdData.fromJson(response.data);
    } catch (err, stackTrace) {
      OLogger.e('打卡失败', err, stackTrace);
    }
    return null;
  }

}