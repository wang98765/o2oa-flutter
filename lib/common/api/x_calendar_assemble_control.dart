

import 'package:get/get.dart';

import '../models/index.dart';
import '../utils/index.dart';

/// 日程管理
class CalendarAssembleControlService extends GetxService {
  static CalendarAssembleControlService get to => Get.find();

  String baseUrl() {
    return O2ApiManager.instance
            .getModuleBaseUrl(O2DistributeModuleEnum.x_calendar_assemble_control) ??
        '';
  }

  /// 日程事件查询
  Future<EventFilterList?> eventListWithFilter(String startTime, String endTime, {List<String> calendarIds = const []}) async {
    try {
      Map<String, dynamic> map = {
        "startTime": startTime,
        "endTime": endTime,
      };
      if (calendarIds.isNotEmpty) {
        map["calendarIds"] = calendarIds;
      }
      ApiResponse response =
          await O2HttpClient.instance.put('${baseUrl()}jaxrs/event/list/filter', map);
      return EventFilterList.fromJson(response.data);
    } catch (err, stackTrace) {
      OLogger.e('日程事件查询失败', err, stackTrace);
    }
    return null;
  }
  /// 日历查询
  Future<MyCalendarList?>	listMyCalendar() async {
    try {
      ApiResponse response =
          await O2HttpClient.instance.get('${baseUrl()}jaxrs/calendar/list/my');
      return MyCalendarList.fromJson(response.data);
    } catch (err, stackTrace) {
      OLogger.e('日历查询失败', err, stackTrace);
    }
    return null;
  }
  /// 日历查询
  Future<CalendarInfo?>	getCalendar(String id) async {
    try {
      ApiResponse response =
          await O2HttpClient.instance.get('${baseUrl()}jaxrs/calendar/$id');
      return CalendarInfo.fromJson(response.data);
    } catch (err, stackTrace) {
      OLogger.e('日历查询失败', err, stackTrace);
    }
    return null;
  }


  /// 日历保存
  Future<IdData?>	saveCalendar(CalendarInfo info) async {
    try {
      ApiResponse response =
          await O2HttpClient.instance.post('${baseUrl()}jaxrs/calendar', info.toJson());
      return IdData.fromJson(response.data);
    } catch (err, stackTrace) {
      OLogger.e('日历保存失败', err, stackTrace);
    }
    return null;
  }

  /// 日历删除
  Future<IdData?>	deleteCalendar(String id) async {
    try {
      ApiResponse response =
          await O2HttpClient.instance.delete('${baseUrl()}jaxrs/calendar/$id');
      return IdData.fromJson(response.data);
    } catch (err, stackTrace) {
      OLogger.e('日历删除失败', err, stackTrace);
    }
    return null;
  }
 

  
  /// 日程保存
  Future<IdData?>	saveEvent(EventInfo info) async {
    try {
      ApiResponse response =
          await O2HttpClient.instance.post('${baseUrl()}jaxrs/event', info.toJson());
      return IdData.fromJson(response.data);
    } catch (err, stackTrace) {
      OLogger.e('日程保存失败', err, stackTrace);
    }
    return null;
  }
  /// 日程修改
  /// 
  /// 只修改当前日程
  Future<IdData?>	 updateEventSingle(EventInfo info) async {
    try {
      ApiResponse response =
          await O2HttpClient.instance.put('${baseUrl()}jaxrs/event/update/single/${info.id}', info.toJson());
      return IdData.fromJson(response.data);
    } catch (err, stackTrace) {
      OLogger.e('日程修改失败', err, stackTrace);
    }
    return null;
  }
  /// 日程修改
  /// 
  /// 之后的重复日程全部修改
  Future<IdData?>	 updateEventAfter(EventInfo info) async {
    try {
      ApiResponse response =
          await O2HttpClient.instance.put('${baseUrl()}jaxrs/event/update/after/${info.id}', info.toJson());
      return IdData.fromJson(response.data);
    } catch (err, stackTrace) {
      OLogger.e('日程修改失败', err, stackTrace);
    }
    return null;
  }
  /// 日程修改
  /// 
  /// 所有的重复日程都修改
  Future<IdData?>	 updateEventAll(EventInfo info) async {
    try {
      ApiResponse response =
          await O2HttpClient.instance.put('${baseUrl()}jaxrs/event/update/all/${info.id}', info.toJson());
      return IdData.fromJson(response.data);
    } catch (err, stackTrace) {
      OLogger.e('日程修改失败', err, stackTrace);
    }
    return null;
  }
  /// 日程删除
  /// 
  /// 只删除当前日程
  Future<IdData?>	 deleteEventSingle(String id) async {
    try {
      ApiResponse response =
          await O2HttpClient.instance.delete('${baseUrl()}jaxrs/event/single/$id');
      return IdData.fromJson(response.data);
    } catch (err, stackTrace) {
      OLogger.e('日程删除失败', err, stackTrace);
    }
    return null;
  }
  /// 日程删除
  /// 
  /// 之后的重复日程全部删除
  Future<IdData?>	 deleteEventAfter(String id) async {
    try {
      ApiResponse response =
          await O2HttpClient.instance.delete('${baseUrl()}jaxrs/event/after/$id');
      return IdData.fromJson(response.data);
    } catch (err, stackTrace) {
      OLogger.e('日程删除失败', err, stackTrace);
    }
    return null;
  }
  /// 日程删除
  /// 
  /// 所有的重复日程都删除
  Future<IdData?>	 deleteEventAll(String id) async {
    try {
      ApiResponse response =
          await O2HttpClient.instance.delete('${baseUrl()}jaxrs/event/all/$id');
      return IdData.fromJson(response.data);
    } catch (err, stackTrace) {
      OLogger.e('日程删除失败', err, stackTrace);
    }
    return null;
  }
  	

}