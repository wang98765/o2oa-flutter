// 帧动画Image
import 'package:flutter/material.dart';

class PlayAssetsImageListWidget extends StatefulWidget {
  final List<String> assetList;
  final double width;
  final double height;
  final int interval ;
  final bool isStop;

  const PlayAssetsImageListWidget(
      {Key? key,
      required this.assetList,
      required this.width,
      required this.height,
      required this.isStop,
      required this.interval})
      : super(key: key);

  @override
  State<StatefulWidget> createState() {
    return VoiceAnimationImageState();
  }
 
}

class VoiceAnimationImageState extends State<PlayAssetsImageListWidget>
    with SingleTickerProviderStateMixin {
  // 动画控制
  Animation<double>? _animation;
  AnimationController? _controller;

  @override
  void initState() {
    super.initState();

    final int imageCount = widget.assetList.length;
    final int maxTime = widget.interval * imageCount;

    // 启动动画controller
    _controller = AnimationController(
        duration: Duration(milliseconds: maxTime), vsync: this);
    _controller?.addStatusListener((AnimationStatus status) {
      if (status == AnimationStatus.completed) {
        _controller?.forward(from: 0.0); // 完成后重新开始
      }
    });

    _animation = Tween<double>(begin: 0, end: imageCount.toDouble())
        .animate(_controller!)
      ..addListener(() {
        setState(() {});
      });
  }

  @override
  void dispose() {
    _controller?.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    if (widget.isStop) {
      _controller?.stop();
    } else {
      _controller?.forward();
    }
    int ix = _animation?.value.floor() ?? 0 % widget.assetList.length;
    List<Widget> images = [];
    // 把所有图片都加载进内容，否则每一帧加载时会卡顿
    for (int i = 0; i < widget.assetList.length; ++i) {
      if (i != ix) {
        images.add(Image.asset(
          widget.assetList[i],
          width: 0,
          height: 0,
        ));
      }
    }
    images.add(Image.asset(
      widget.assetList[ix],
      width: widget.width,
      height: widget.height,
    ));
    return Stack(alignment: AlignmentDirectional.center, children: images);
  }
}
