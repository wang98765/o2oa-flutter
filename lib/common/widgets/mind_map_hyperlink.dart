import 'package:flutter/material.dart';
import 'package:get/get.dart';
import '../models/mindmap/mind_node.dart';
import '../style/index.dart';

class MindMapHyperlinkForm extends StatefulWidget {
  final String hyperlink;
  final String hyperlinkTitle;
  const MindMapHyperlinkForm({super.key, required this.hyperlink, required this.hyperlinkTitle});

  @override
  State<StatefulWidget> createState() {
    return MindMapHyperlinkFormState();
  }
}

class MindMapHyperlinkFormState extends State<MindMapHyperlinkForm> {
  final GlobalKey<FormState> _formKey = GlobalKey();
  // 手机号码的控制器
  final TextEditingController linkController = TextEditingController();
  // 验证码的控制器
  final TextEditingController descController = TextEditingController();

  @override
  Widget build(BuildContext context) {
    linkController.text = widget.hyperlink;
    descController.text = widget.hyperlinkTitle;
    return Scaffold(
        appBar: AppBar(
          title:  Text('mindmap_link'.tr),
          actions: [TextButton(onPressed: saveData, child: Text('positive'.tr , style: AppTheme.whitePrimaryTextStyle))],
        ),
        body: Container(
            width: double.infinity,
            height: double.infinity,
            padding: const EdgeInsets.all(32.0),
            child: SingleChildScrollView(
              child: Form(
                key: _formKey,
                child: Column(children: <Widget>[
                  TextFormField(
                      // initialValue: _link,
                      maxLines: 1,
                      autofocus: true,
                      controller: linkController,
                      keyboardType: TextInputType.text,
                      decoration:  InputDecoration(
                          labelText: 'mindmap_link_address'.tr,
                          hintText: 'mindmap_link_address_hint'.tr),
                      validator: (value) {
                        if (value?.isEmpty == true) {
                          return 'mindmap_link_address_not_empty'.tr;
                        }
                        if (!value!.startsWith(RegExp(r"https?://")) &&
                            !value.startsWith(RegExp(r"ftps?://"))) {
                          return 'mindmap_link_address_error'.tr;
                        }
                        return null;
                      },
                      // onSaved: (value) => _link = value ?? ''
                      ),
                  TextFormField(
                      // initialValue: _linkTitle,
                      maxLines: 1,
                      controller: descController,
                      keyboardType: TextInputType.text,
                      decoration: InputDecoration(
                          labelText: 'mindmap_link_note'.tr, hintText: 'mindmap_link_note_hint'.tr),
                      // onSaved: (value) => _linkTitle = value ?? ''
                      ),
                ]),
              ),
            )));
  }
  void saveData() {
    if (_formKey.currentState?.validate() == true) {
      //_formKey.currentState?.save();
      NodeData data = NodeData();
      data.hyperlink = linkController.text;
      data.hyperlinkTitle = descController.text;
      _close(data);
    }
  }
  void _close(NodeData? data) {
    Navigator.pop(context, data);
  }
}
