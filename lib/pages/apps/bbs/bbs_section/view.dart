import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:get/get.dart';
import 'package:pull_to_refresh/pull_to_refresh.dart';

import '../../../../common/models/bbs/index.dart';
import '../../../../common/routers/index.dart';
import '../../../../common/style/index.dart';
import '../../../../common/widgets/index.dart';
import 'index.dart';

class BbsSectionPage extends GetView<BbsSectionController> {
  const BbsSectionPage({Key? key}) : super(key: key);

  static Future<void> open(SectionInfoList section) async {
    await Get.toNamed(O2OARoutes.appBBSSection, arguments: section);
  }

  // 主视图
  Widget _buildView() {
    return Obx(() => SmartRefresher(
          enablePullDown: true,
          enablePullUp: controller.state.hasMoreData,
          controller: controller.refreshController,
          onRefresh: controller.refreshData,
          onLoading: controller.loadMore,
          child: ListView.separated(
              itemCount: controller.state.subjectList.length,
              separatorBuilder: ((context, index) => const Divider(height: 1)),
              itemBuilder: (context, index) {
                var item = controller.state.subjectList[index];
                var title = '[${item.type ?? ''}] ${item.title ?? ''}';
                var name = item.nickName ?? '';
                if (name.isEmpty) {
                  name = item.creatorNameShort ?? '';
                }
                return ListTile(
                  onTap: () => controller.clickSubject(item),
                  title: Text(title),
                  subtitle: Padding(
                    padding: const EdgeInsets.only(top: 10),
                    child: Text(item.summary ?? ''),
                  ),
                  trailing: Column(children: [
                    const SizedBox(height: 16),
                    Expanded(
                        flex: 1,
                        child: Text(
                          name,
                          style: TextStyle(fontSize: 14.sp),
                          textAlign: TextAlign.center,
                        )),
                    Row(
                      mainAxisSize: MainAxisSize.min,
                      children: [
                        const Icon(Icons.visibility, size: 16),
                        Text('${item.viewTotal ?? 0}',
                            style: TextStyle(fontSize: 12.sp)),
                        const Icon(Icons.mode_comment, size: 16),
                        Text('${item.replyTotal ?? 0}',
                            style: TextStyle(fontSize: 12.sp)),
                      ],
                    )
                  ]),
                );
              }),
        ));
  }

  @override
  Widget build(BuildContext context) {
    return GetBuilder<BbsSectionController>(
      builder: (_) {
        return Scaffold(
          appBar: AppBar(
            title: Obx(() => Text(controller.state.title)),
            actions: [
              Obx(() => Visibility(
                  visible: controller.state.subjectPublishAble,
                  child: TextButton(
                      onPressed: () => controller.clickGotoPublish(),
                      child: Text('bbs_subject_publish'.tr,
                          style: AppTheme.whitePrimaryTextStyle))))
            ],
          ),
          body: SafeArea(
            child: Obx(() => controller.state.subjectList.length > 0
                ? _buildView()
                : O2UI.noResultView(context)),
          ),
        );
      },
    );
  }
}
