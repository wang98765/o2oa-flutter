import 'package:get/get.dart';

import 'controller.dart';

class BbsSectionBinding implements Bindings {
  @override
  void dependencies() {
    Get.lazyPut<BbsSectionController>(() => BbsSectionController());
  }
}
