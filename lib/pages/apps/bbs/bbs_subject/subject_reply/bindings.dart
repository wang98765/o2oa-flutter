import 'package:get/get.dart';

import 'controller.dart';

class SubjectReplyBinding implements Bindings {
  @override
  void dependencies() {
    Get.lazyPut<SubjectReplyController>(() => SubjectReplyController());
  }
}
