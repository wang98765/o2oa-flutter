import 'package:flutter/material.dart';
import 'package:get/get.dart';

import 'index.dart';
import 'widgets/widgets.dart';

class MainPage extends GetView<MainController> {
  const MainPage({Key? key}) : super(key: key);
 

// 内容页
  Widget _buildPageView() {
    return PageView(
      physics: const NeverScrollableScrollPhysics(),
      controller: controller.pageViewController,
      onPageChanged: controller.handlePageChanged,
      children: const [
        CheckInWidget(),
        StatisticWidget()
      ],
    );
  }

  // 底部导航
  Widget _buildBottomNavigationBar() {
    return Obx(() => BottomNavigationBar(
          items:  controller.state.bottomTabs,
          currentIndex: controller.state.currentIndex,
          type: BottomNavigationBarType.fixed,
          onTap: controller.handleNavBarTap
        ));
  }

  @override
  Widget build(BuildContext context) {
    return GetBuilder<MainController>(
      builder: (_) {
        return Scaffold(
          appBar: AppBar(title: Obx(()=>Text(controller.state.title))),
          body: _buildPageView(),
          bottomNavigationBar: _buildBottomNavigationBar()
        );
      },
    );
  }
}
