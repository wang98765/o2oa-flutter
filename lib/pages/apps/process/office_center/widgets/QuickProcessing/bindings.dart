import 'package:get/get.dart';

import 'controller.dart';

class QuickprocessingBinding implements Bindings {
  @override
  void dependencies() {
    Get.lazyPut<QuickprocessingController>(() => QuickprocessingController());
  }
}
