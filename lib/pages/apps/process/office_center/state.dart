import 'package:get/get.dart';

import '../../../../common/models/process/index.dart';

class OfficeCenterState {
  // title
  final _title = "".obs;
  set title(value) => _title.value = value;
  get title => _title.value;

  final RxList<dynamic> dataList = <dynamic>[].obs;
  
  // 批量快速处理的待办数据
  final RxList<TaskData> taskSelectedList = <TaskData>[].obs;

   // 是否有更多翻页数据
  final _hasMoreData = true.obs;
  set hasMoreData(bool value) => _hasMoreData.value = value;
  bool get hasMoreData => _hasMoreData.value;

}
