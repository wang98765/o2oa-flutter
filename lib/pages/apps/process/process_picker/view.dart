import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:grouped_list/grouped_list.dart';

import '../../../../common/models/index.dart';
import '../../../../common/routers/index.dart';
import 'index.dart';

class ProcessPickerPage extends GetView<ProcessPickerController> {
  const ProcessPickerPage({Key? key}) : super(key: key);

  static Future<dynamic> startPicker(ProcessPickerMode mode) async {
    return await Get.toNamed(O2OARoutes.appProcessPicker,
        arguments: {"mode": mode});
  }

  @override
  Widget build(BuildContext context) {
    return GetBuilder<ProcessPickerController>(
      builder: (_) {
        return Scaffold(
            appBar: AppBar(title: Text("process_picker_title".tr)),
            body: SafeArea(
              child: Obx(
                  () => controller.state.mode.value == ProcessPickerMode.process
                      ? Row(
                          children: [
                            Expanded(
                              flex: 1,
                              child: appListView(context),
                            ),
                            const VerticalDivider(width: 1),
                            Expanded(
                              flex: 1,
                              child: processListView(context),
                            )
                          ],
                        )
                      : appListView(context)),
            ));
      },
    );
  }

  Widget appListView(BuildContext context) {
    return Obx(() {
      var appList = controller.state.appList.toList();
      return GroupedListView<ProcessApplicationData, String>(
        elements: appList,
        groupBy: (element) {
          var category = element.applicationCategory ?? '';
          if (category.isEmpty) {
            category = 'process_picker_app_no_categroy'.tr;
          }
          return category;
        },
        groupSeparatorBuilder: ((value) => Padding(
            padding: const EdgeInsets.only(left: 8, top: 10, bottom: 10),
            child: Text(
              value,
              style: TextStyle(color: Theme.of(context).hintColor),
            ))),
        itemBuilder: (context, element) {
          return Obx(() {
            final currentApp = controller.state.currentApp.value;
            return Padding(
                padding: const EdgeInsets.only(left: 20),
                child: ListTile(
                  onTap: () => controller.clickApp(element),
                  selected: currentApp?.id == element.id,
                  title: Text(
                    element.name ?? '',
                    style: const TextStyle(fontSize: 16),
                  ),
                ));
          });
        },
      );
    });
  }

  Widget processListView(BuildContext context) {
    return Obx(() {
      return ListView.separated(
          itemBuilder: (context, index) {
            final process = controller.state.processList[index];
            return ListTile(
              onTap: () => controller.clickProcess(process),
              title: Text(
                process.name ?? '',
                style: const TextStyle(fontSize: 16),
              ),
            );
          },
          separatorBuilder: (context, index) {
            return Divider(
              height: 1,
              color: Theme.of(context).dividerColor,
            );
          },
          itemCount: controller.state.processList.length);
    });
  }
}
