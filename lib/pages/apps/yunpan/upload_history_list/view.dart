import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:o2oa_all_platform/common/extension/int_extension.dart';

import '../../../../common/style/index.dart';
import '../../../../common/widgets/index.dart';
import 'index.dart';

class UploadHistoryListPage extends GetView<UploadHistoryListController> {
  const UploadHistoryListPage({Key? key}) : super(key: key);

  // 主视图
  Widget _buildView() {
    return Obx(() => ListView.builder(
        itemCount: controller.state.uploadList.length,
        itemBuilder: (context, index) {
          final item = controller.state.uploadList[index];
          final progress = item.progress ?? 0;
          return ListTile(
              title: Text(item.name ?? ''),
              subtitle: Text(item.length?.friendlyFileLength() ?? ''),
              trailing: progress >= 1
                  ? Text('cloud_disk_upload_completed'.tr,
                      style: Theme.of(context).textTheme.bodySmall)
                  : SizedBox(
                      height: 40.0,
                      width: 40,
                      child: CircularProgressIndicator(
                          value: progress,
                          backgroundColor: Colors.white,
                          valueColor: const AlwaysStoppedAnimation<Color>(
                              AppColor.primaryColor)),
                    ));
        }));
  }

  @override
  Widget build(BuildContext context) {
    return GetBuilder<UploadHistoryListController>(
      builder: (_) {
        return Scaffold(
            appBar: AppBar(title: Text("cloud_disk_upload_title".tr)),
            body: SafeArea(
              // ignore: prefer_is_empty
              child: Container(
                color: Theme.of(context).scaffoldBackgroundColor,
                child: Obx(() => controller.state.uploadList.length > 0
                    ? _buildView()
                    : O2UI.noResultView(context)),
              ),
            ));
      },
    );
  }
}
