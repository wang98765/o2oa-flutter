import 'package:get/get.dart';

import '../../../../common/models/index.dart';

class ShareListState {
  // title
  final _title = "".obs;
  set title(value) => _title.value = value;
  get title => _title.value;


  Rx<CloudDiskShareListType> shareType = Rx<CloudDiskShareListType>(CloudDiskShareListType.shareTome);


  RxList<ShareFileInfo> shareList = <ShareFileInfo>[].obs;

}
