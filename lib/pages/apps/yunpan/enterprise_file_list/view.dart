import 'package:flutter/material.dart';
import 'package:get/get.dart';

import '../../../../common/routers/index.dart';
import '../../../../common/widgets/index.dart';
import 'index.dart';
import 'widgets/widgets.dart';

/// 企业网盘 文件列表
class EnterpriseFileListPage extends GetView<EnterpriseFileListController> {
  const EnterpriseFileListPage({Key? key}) : super(key: key);

  ///
  static Future<dynamic> open(
      String topId, String topName, bool canCreate) async {
    if (topId.isEmpty || topName.isEmpty) {
      return null;
    }
    return await Get.toNamed(O2OARoutes.appCloudDiskV3EnterpriseFileList,
        arguments: {
          "topId": topId,
          "topName": topName,
          "canCreate": canCreate
        });
  }

  // 主视图
  Widget _buildView() {
    return const EnterpriseFileListWidget();
  }

  @override
  Widget build(BuildContext context) {
    return GetBuilder<EnterpriseFileListController>(
      builder: (_) {
        return Scaffold(
          appBar:
              AppBar(title: Obx(() => Text(controller.state.title)), actions: [
            PopupMenuButton<int>(
              icon: const Icon(Icons.more_vert, color: Colors.white),
              itemBuilder: (BuildContext context) => [
                PopupMenuItem<int>(
                  value: 0,
                  child: Text('cloud_disk_action_create_folder'.tr),
                ),
                PopupMenuItem<int>(
                  value: 1,
                  child: Text('cloud_disk_action_upload_file'.tr),
                ),
                PopupMenuItem<int>(
                  value: 2,
                  child: Text('cloud_disk_upload_title'.tr),
                )
              ],
              onSelected: (value) {
                switch (value) {
                  case 0:
                    controller.createFolder();
                    break;
                  case 1:
                    controller.uploadFile();
                    break;
                  case 2:
                    controller.openUploadPage();
                    break;
                  default:
                    break;
                }
              },
            ),
          ]),
          body: SafeArea(
              child: Container(
                  color: Theme.of(context).scaffoldBackgroundColor,
                  child: Stack(
                    children: [
                      _buildView(),
                      // 动画 丢文件到右上角 表示后台上传开始
                      Obx(() => AnimatedPositioned(
                            right: controller.state.animRight,
                            top: controller.state.animTop,
                            width: controller.state.animWidth,
                            height: controller.state.animWidth,
                            duration: const Duration(milliseconds: 500),
                            curve: Curves.easeInCubic,
                            child: Visibility(
                                visible: controller.state.visibleAnimIcon,
                                child: const AssetsImageView(
                                    'icon_file_unkown.png',
                                    fit: BoxFit.fill)),
                            onEnd: () => controller.initAnimElement(),
                          ))
                      ]))),
        );
      },
    );
  }
}
