import 'package:get/get.dart';

import 'controller.dart';

class EnterpriseFileListBinding implements Bindings {
  @override
  void dependencies() {
    Get.lazyPut<EnterpriseFileListController>(() => EnterpriseFileListController());
  }
}
