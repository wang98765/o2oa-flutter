import 'package:get/get.dart';

import 'controller.dart';

class CloudDiskV3Binding implements Bindings {
  @override
  void dependencies() {
    Get.lazyPut<CloudDiskV3Controller>(() => CloudDiskV3Controller());
  }
}
