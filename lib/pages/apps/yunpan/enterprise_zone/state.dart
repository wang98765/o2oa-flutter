import 'package:get/get.dart';

import '../../../../common/models/index.dart';

class EnterpriseZoneState {
  // title
  final _title = "".obs;
  set title(value) => _title.value = value;
  get title => _title.value;

// 是否能新建工作区
  final _canCreate = false.obs;
  set canCreate(bool value) => _canCreate.value = value;
  bool get canCreate => _canCreate.value;

  RxList<CloudDiskZoneInfo> zoneList = <CloudDiskZoneInfo>[].obs;

}
