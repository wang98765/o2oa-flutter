import 'package:get/get.dart';

import 'controller.dart';

class FileTypeListBinding implements Bindings {
  @override
  void dependencies() {
    Get.lazyPut<FileTypeListController>(() => FileTypeListController());
  }
}
