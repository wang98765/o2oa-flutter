import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:get/get.dart';
import 'package:o2oa_all_platform/common/extension/int_extension.dart';
import 'package:pull_to_refresh/pull_to_refresh.dart';

import '../../../../common/models/index.dart';
import '../../../../common/routers/index.dart';
import '../../../../common/values/index.dart';
import '../../../../common/widgets/index.dart';
import 'index.dart';

class FileTypeListPage extends GetView<FileTypeListController> {
  const FileTypeListPage({Key? key}) : super(key: key);

  static Future<dynamic> open(CloudDiskFileType type) async {
    await Get.toNamed(O2OARoutes.appCloudDiskListFileByType,
        arguments: {"type": type});
  }

  // 图片视图
  Widget _imageListView(BuildContext context) {
    return Obx(() => SmartRefresher(
        enablePullDown: true,
        enablePullUp: controller.state.hasMoreData,
        controller: controller.refreshController,
        onRefresh: () => controller.refreshFileList(),
        onLoading: () => controller.loadMoreFileList(),
        child: GridView.count(
            crossAxisCount: 3,
            mainAxisSpacing: 10.0,
            crossAxisSpacing: 10.0,
            childAspectRatio: 1, //设置宽高比
            children: controller.state.fileList.map((element) {
              return LayoutBuilder(builder: (context, constraints) {
                // 获取视图的宽度和高度
                double width = constraints.maxWidth;
                double height = constraints.maxHeight;
                return InkWell(
                    onTap: () => controller.clickFile(element),
                    child: Image(
                        image: NetworkImage(
                            controller.imageOnlineUrl(
                                element.id, width, height),
                            headers: controller.getHeaders()),
                        alignment: Alignment.center,
                        width: width,
                        height: height,
                        fit: BoxFit.fill));
              });

              // return Column(
              //     mainAxisAlignment: MainAxisAlignment.center,
              //     crossAxisAlignment: CrossAxisAlignment.center,
              //     children: [
              //       Image(
              //           image: NetworkImage(
              //               controller.imageOnlineUrl(element.id),
              //               headers: controller.getHeaders()),
              //           alignment: Alignment.center,
              //           width: 40.w,
              //           height: 40.w,
              //           fit: BoxFit.fill),
              //       Expanded(
              //           flex: 1,
              //           child: Center(
              //               child: Text(element.name ?? '',
              //                   softWrap: true,
              //                   textAlign: TextAlign.center,
              //                   overflow: TextOverflow.ellipsis,
              //                   maxLines: 1,
              //                   style: Theme.of(context).textTheme.bodyMedium)))
              //     ]);
            }).toList())));
  }

  // 文件视图
  Widget _fileListView() {
    return Obx(() => SmartRefresher(
        enablePullDown: true,
        enablePullUp: controller.state.hasMoreData,
        controller: controller.refreshController,
        onRefresh: () => controller.refreshFileList(),
        onLoading: () => controller.loadMoreFileList(),
        child: ListView.builder(
            itemCount: controller.state.fileList.length,
            itemBuilder: (context, index) {
              var item = controller.state.fileList[index];
              final icon = FileIcon.getFileIconAssetsNameByExtension(
                  item.extension ?? '');
              final len = (item.length ?? 0).friendlyFileLength();
              final updateTime = '${item.updateTime ?? ''} $len';
              return ListTile(
                onTap: () => controller.clickFile(item),
                leading: AssetsImageView(icon, width: 40, height: 40),
                title: Text(item.name ?? ''),
                subtitle: Text(updateTime),
              );
            })));
  }

  @override
  Widget build(BuildContext context) {
    return GetBuilder<FileTypeListController>(
      builder: (_) {
        return Scaffold(
            appBar: AppBar(
                title: Obx(() => Text(controller.state.type.value.getName()))),
            body: SafeArea(
              child: Container(
                  color: Theme.of(context).scaffoldBackgroundColor,
                  child: Padding(
                    padding:
                        EdgeInsets.only(top: 10.w, left: 14.w, right: 14.w),
                    child: Obx(() =>
                        controller.state.type.value == CloudDiskFileType.image
                            ? _imageListView(context)
                            : _fileListView()),
                  )),
            ));
      },
    );
  }
}
