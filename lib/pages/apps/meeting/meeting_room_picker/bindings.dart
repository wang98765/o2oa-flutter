import 'package:get/get.dart';

import 'controller.dart';

class MeetingRoomPickerBinding implements Bindings {
  @override
  void dependencies() {
    Get.lazyPut<MeetingRoomPickerController>(() => MeetingRoomPickerController());
  }
}
