import 'package:get/get.dart';

import '../../../../common/models/index.dart';

class MeetingFormState {
  // title
  final _title = "".obs;
  set title(value) => _title.value = value;
  get title => _title.value;

  Rx<MeetingInfoModel?> meetingInfo = Rx<MeetingInfoModel?>(null);

  RxList<String> typeList = <String>[].obs;

  // 是否显示 会议 mode 选择器
  final _showMode = false.obs;
  set showMode(bool value) => _showMode.value = value;
  bool get showMode => _showMode.value;

  // 是否显示 在线会议的字段  link roomid
  final _showOnline = false.obs;
  set showOnline(bool value) => _showOnline.value = value;
  bool get showOnline => _showOnline.value;
}
