import 'package:get/get.dart';

import 'controller.dart';

class MeetingDetailBinding implements Bindings {
  @override
  void dependencies() {
    Get.lazyPut<MeetingDetailController>(() => MeetingDetailController());
  }
}
