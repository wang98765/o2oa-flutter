import 'package:get/get.dart';

class CmsDocumentState {
  // title
  final _title = "".obs;
  set title(value) => _title.value = value;
  get title => _title.value;

   // 访问地址
  final _url = "".obs;
  set url(value) => _url.value = value;
  String get url => _url.value;
}
