import 'package:get/get.dart';

import '../../../common/models/index.dart';

class CmsState {

  // title
  final _title = "".obs;
  set title(value) => _title.value = value;
  get title => _title.value;
  
  final RxList<CmsAppData> appList = <CmsAppData>[].obs;
}
