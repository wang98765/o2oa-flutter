import 'package:get/get.dart';

import '../../../../common/models/index.dart';

class CmsCategoryPickerState {
  // title
  final _title = "".obs;
  set title(value) => _title.value = value;
  get title => _title.value;


  final RxList<CmsCategoryData> categoryList = <CmsCategoryData>[].obs;
}
