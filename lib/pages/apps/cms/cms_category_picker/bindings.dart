import 'package:get/get.dart';

import 'controller.dart';

class CmsCategoryPickerBinding implements Bindings {
  @override
  void dependencies() {
    Get.lazyPut<CmsCategoryPickerController>(() => CmsCategoryPickerController());
  }
}
