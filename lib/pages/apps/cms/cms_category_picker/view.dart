import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:grouped_list/grouped_list.dart';

import '../../../../common/models/index.dart';
import '../../../../common/widgets/index.dart';
import 'index.dart';

class CmsCategoryPickerPage extends GetView<CmsCategoryPickerController> {
  const CmsCategoryPickerPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return GetBuilder<CmsCategoryPickerController>(
      builder: (_) {
        return Scaffold(
          appBar: AppBar(title: Text("cms_category_picker_title".tr)),
          body: SafeArea(child: Obx(() {
            var list = controller.state.categoryList.toList();
            return GroupedListView<CmsCategoryData, String>(
              elements: list,
              groupBy: (element) => element.appName ?? '无',
              groupSeparatorBuilder: ((value) => Padding(
                  padding: const EdgeInsets.only(left: 8, top: 10, bottom: 10),
                  child: Text(value))),
              itemBuilder: (context, element) {
                return Card(
                  elevation: 8.0,
                  margin: const EdgeInsets.symmetric(
                      horizontal: 10.0, vertical: 6.0),
                  child: ListTile(
                    onTap: () => controller.clickBackResult(element),
                    // contentPadding: const EdgeInsets.symmetric(
                    //     horizontal: 20.0, vertical: 10.0),
                    title: Text(
                      element.categoryName ?? '',
                      style: const TextStyle(fontSize: 16),
                    ),
                    trailing: O2UI.rightArrow(),
                  ),
                );
              },
            );
          })),
        );
      },
    );
  }
}
