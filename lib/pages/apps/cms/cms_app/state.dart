import 'package:get/get.dart';

import '../../../../common/models/cms/index.dart';

class CmsAppState {
  // title
  final _title = "".obs;
  set title(value) => _title.value = value;
  get title => _title.value;


  final RxList<CmsCategoryData> categoryList = <CmsCategoryData>[].obs;

  Rx<CmsAppData?> app = Rx<CmsAppData?>(null);

  final _canPublish = false.obs;
  set canPublish(bool value) => _canPublish.value = value;
  bool get canPublish => _canPublish.value;

  // 初始化 index
  final _initCategoryIndex = 0.obs;
  set initCategoryIndex(int value) => _initCategoryIndex.value = value;
  int get initCategoryIndex => _initCategoryIndex.value;
}
