import 'package:flutter/material.dart';
import 'package:get/get.dart';

import '../../../../common/models/index.dart';
import '../../../../common/routers/index.dart';
import '../../../../common/style/index.dart';
import '../../../../common/utils/index.dart';
import '../../../../common/widgets/index.dart';
import '../calendar_const.dart';
import '../widgets/widgets.dart';
import 'index.dart';

class CreateCalendarEventPage extends GetView<CreateCalendarEventController> {
  const CreateCalendarEventPage({Key? key}) : super(key: key);

  static Future<dynamic> openUpdate(EventInfo info) async {
    await Get.toNamed(O2OARoutes.appCalendarEventCreate,
        arguments: {"eventInfo": info});
  }

  @override
  Widget build(BuildContext context) {
    return GetBuilder<CreateCalendarEventController>(
      builder: (_) {
        return Scaffold(
          appBar: AppBar(
              title: Obx(() => Text(controller.state.isUpdate
                  ? 'calendar_action_update_event'.tr
                  : 'calendar_action_create_event'.tr)),
              actions: [
                Obx(() => controller.state.isUpdate
                    ? TextButton(
                        onPressed: () => controller.updateEvent(),
                        child: Text('save'.tr,
                            style: AppTheme.whitePrimaryTextStyle))
                    : TextButton(
                        onPressed: () => controller.saveEvent(),
                        child: Text('save'.tr,
                            style: AppTheme.whitePrimaryTextStyle))),
              ]),
          body: SafeArea(
            child: ListView(
                children: [const SizedBox(height: 8), formView(context)]),
          ),
        );
      },
    );
  }

  Widget formView(BuildContext context) {
    return O2UI.sectionOutBox( Column(children: [
        // 日程标题
        TextField(
          controller: controller.titleController,
          decoration: InputDecoration(
              hintText: "calendar_event_form_title_hint".tr,
              label: Text('calendar_event_form_title'.tr)),
        ),
        // 日历选择
        Obx(()=>O2UI.lineWidget(
            'calendar_event_form_calendar'.tr,
            Text(controller.state.calendar.value == null
                ? ''
                : (controller.state.calendar.value?.name ?? '')),
            ontap: () => controller.pickCalendar())),
        const Divider(height: 1),
        // 全天
        Obx(()=>Padding(
            padding: const EdgeInsets.only(top: 10, bottom: 10),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              mainAxisSize: MainAxisSize.max,
              children: [
                Text(
                  'calendar_event_all_day'.tr,
                  style: Theme.of(context).textTheme.bodyLarge,
                ),
                const SizedBox(width: 10),
                Switch(
                  value: controller.state.allDay,
                  onChanged: (bool value) {
                    controller.changeAllDay(value);
                  },
                )
              ],
            ))),
        const Divider(height: 1),
        // 开始日期
        Obx(()=>O2UI.lineWidget('calendar_event_form_start_date'.tr,
            Text(controller.state.startDate),
            ontap: () => controller.selectStartDate())),
        const Divider(height: 1),
        Obx(()=>Visibility(
            visible: !controller.state.allDay,
            child: O2UI.lineWidget('calendar_event_form_start_time'.tr,
                Text(controller.state.startTime),
                ontap: () => controller.selectStartTime()))),
        Obx(()=>Visibility(
            visible: !controller.state.allDay, child: const Divider(height: 1))),
        // 结束日期
        Obx(()=>O2UI.lineWidget(
            'calendar_event_form_end_date'.tr, Text(controller.state.endDate),
            ontap: () => controller.selectEndDate())),
        const Divider(height: 1),
        Obx(()=>Visibility(
            visible: !controller.state.allDay,
            child: O2UI.lineWidget('calendar_event_form_end_time'.tr,
                Text(controller.state.endTime),
                ontap: () => controller.selectEndTime()))),
        Obx(()=>Visibility(
            visible: !controller.state.allDay, child: const Divider(height: 1))),
        // 颜色
        Obx(() {
          final eventColor = controller.state.eventColor;
          OLogger.d('obx  颜色 $eventColor');
          return ColorChooseView(
            color: eventColor,
            onChange: ((color) => controller.chooseEventColor(color)));
            }),
        const Divider(height: 1),
        // 提醒
        Obx(()=>O2UI.lineWidget('calendar_event_form_alert'.tr,
            Text(O2Calendar.remindOptions[controller.state.remind] ?? ''),
            ontap: () => controller.selectRemind())),
        const Divider(height: 1),
        // 重复
        Obx(()=>O2UI.lineWidget('calendar_event_form_repeat'.tr,
            Text(O2Calendar.repeatOptions[controller.state.repeat] ?? ''),
            ontap: () => controller.selectRepeat())),
        const Divider(height: 1),
        Obx(()=>Visibility(
            visible: controller.state.repeat != 'NONE' &&
                controller.state.repeat != 'WEEKLY',
            child: O2UI.lineWidget('calendar_event_form_repeat_end'.tr,
                Text(controller.state.repeatEndDate),
                ontap: () => controller.selectRepeatEndDate()))),
        Obx(()=>Visibility(
            visible: controller.state.repeat != 'NONE' &&
                controller.state.repeat != 'WEEKLY',
            child: const Divider(height: 1))),
        Obx(()=>Visibility(
            visible: controller.state.repeat == 'WEEKLY',
            child: weekDayChooseView(context))),
        const Divider(height: 1),
        //
        TextField(
          controller: controller.commentController,
          maxLines: 3,
          decoration: InputDecoration(
              hintText: "calendar_event_form_content_hint".tr,
              label: Text('calendar_event_form_content'.tr)),
        ),
        const SizedBox(height: 5),
        Obx(()=>Visibility(
            visible: controller.state.isUpdate,
            child:Padding(
            padding: const EdgeInsets.only(
                top: 40, bottom: 10, left: 15, right: 15),
            child: SizedBox(
                width: double.infinity,
                height: 44,
                child: O2ElevatedButton(() => controller.deleteEvent(),
                    Text(
                      'calendar_action_delete_event'.tr,
                      style: const TextStyle(fontSize: 18),
                    ))),
          )))
      ]), context);
  }

  ///
  Widget weekDayChooseView(BuildContext context) {
    return Obx(() {
      final weekdays = controller.state.weekDayKeys.toList();
      return Padding(
          padding: const EdgeInsets.all(10),
          child: Wrap(
            spacing: 8.0, // 水平间距
            runSpacing: 4.0, // 垂直间距
            children: O2Calendar.weekDays.entries
                .map((entry) => InkWell(
                    onTap: () => controller.chooseWeekDay(entry.key),
                    child: EllipticalText(entry.value,
                        backgroundColor: weekdays.contains(entry.key)
                            ? Theme.of(context).primaryColor
                            : Theme.of(context).highlightColor)))
                .toList(),
          ));
    });
  }
}
