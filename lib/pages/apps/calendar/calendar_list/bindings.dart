import 'package:get/get.dart';

import 'controller.dart';

class CalendarListBinding implements Bindings {
  @override
  void dependencies() {
    Get.lazyPut<CalendarListController>(() => CalendarListController());
  }
}
