import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:get/get.dart';
import 'package:grouped_list/grouped_list.dart';
import 'package:o2oa_all_platform/common/extension/string_extension.dart';

import '../../../../common/models/index.dart';
import '../../../../common/routers/index.dart';
import '../../../../common/style/index.dart';
import 'index.dart';

class CalendarListPage extends GetView<CalendarListController> {
  const CalendarListPage({Key? key}) : super(key: key);

  static Future<dynamic> open({bool isSelectMode = false}) async {
    return await Get.toNamed(O2OARoutes.appCalendarList, arguments: {"isSelectMode": isSelectMode});
  }

  // 主视图
  Widget _buildView() {
    return Obx(() {
      final list = controller.state.calendarList.toList();
      return GroupedListView<CalendarInfo, String>(
          elements: list,
          groupBy: (element) => element.groupedTag ?? '',
          groupSeparatorBuilder: ((value) => Padding(
              padding: const EdgeInsets.only(left: 8, top: 10, bottom: 10),
              child: Text(value))),
          itemBuilder: (context, element) {
            return InkWell(
              onTap: ()=> controller.clickCalendar(element),
              child: 
            Container(
                color: Theme.of(context).colorScheme.background,
                child: ListTile(
                  leading: Container(
                      width: 32.w,
                      height: 32.w,
                      decoration: BoxDecoration(
                        shape: BoxShape.rectangle,
                        borderRadius:
                            BorderRadius.circular(16.w), // 调整圆角半径以改变椭圆形状
                        color: element.color?.hexToColor(), // 设置背景颜色
                      )),
                  title: Text(element.name ?? '',
                      style: Theme.of(context).textTheme.bodyLarge),
                )));
          });
    });
  }

  @override
  Widget build(BuildContext context) {
    return GetBuilder<CalendarListController>(
      builder: (_) {
        return Scaffold(
          appBar: AppBar(title: Obx(()=> Text(controller.state.title)),
          actions: [
            controller.state.isSelectMode ? Container() : TextButton(
                        onPressed: () => controller.clickCreateCalendar(),
                        child: Text('calendar_action_calendar_create'.tr,
                            style: AppTheme.whitePrimaryTextStyle))
          ],),
          body: SafeArea(
            child: _buildView(),
          ),
        );
      },
    );
  }
}
