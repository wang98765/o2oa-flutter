import 'package:get/get.dart';

import 'controller.dart';

class DemoLoginBinding implements Bindings {
  @override
  void dependencies() {
    Get.lazyPut<DemoLoginController>(() => DemoLoginController());
  }
}
