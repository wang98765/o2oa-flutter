import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:o2oa_all_platform/common/extension/string_extension.dart';

import '../../../../../../common/style/index.dart';
import '../../../../../../common/widgets/index.dart';
import 'index.dart';

class EmpowerCreatePage extends GetView<EmpowerCreateController> {
  const EmpowerCreatePage({Key? key}) : super(key: key);

  // 主视图
  Widget _buildView(BuildContext context) {
    return Column(children: [
      Obx(() => MyIdentityChooseView(
          identityList: controller.state.identityList.toList(),
          onChange: (identityDn) =>
              controller.clickIdentity(identityDn),
        )
      ),
      const Divider(height: 1),
      // 委托人
      O2UI.lineWidget('account_safe_empower_form_to_person'.tr,
          Obx(() => Text(controller.state.toPerson.o2NameCut())),
          ontap: () => controller.clickChooseToPerson()),
      const Divider(height: 1),
      //  开始时间
      O2UI.lineWidget('account_safe_empower_form_start_time'.tr,
          Obx(() => Text(controller.state.startTime)),
          ontap: () => controller.clickChooseStartTime()),
      const Divider(height: 1),
      //  结束时间
      O2UI.lineWidget('account_safe_empower_form_end_time'.tr,
          Obx(() => Text(controller.state.endTime)),
          ontap: () => controller.clickChooseEndTime()),
      const Divider(height: 1),
      typeListView(context),
      Obx(()=>Visibility(
          visible: controller.state.type == 'application',
          child: O2UI.lineWidget('account_safe_empower_type_app'.tr,
              Obx(() => Text(controller.state.appName)),
              ontap: () => controller.clickChooseApplication()))),
      Obx(()=>Visibility(
          visible: controller.state.type == 'process',
          child: O2UI.lineWidget('account_safe_empower_type_process'.tr,
              Obx(() => Text(controller.state.processName)),
              ontap: () => controller.clickChooseToPerson())))
    ]);
  }

  @override
  Widget build(BuildContext context) {
    return GetBuilder<EmpowerCreateController>(
      builder: (_) {
        return Scaffold(
          appBar:
              AppBar(title: Text('account_safe_empower_create'.tr), actions: [
            TextButton(
                onPressed: () => controller.clickSave(),
                child: Text('save'.tr, style: AppTheme.whitePrimaryTextStyle))
          ]),
          body: SafeArea(
              child: Container(
                  color: Theme.of(context).colorScheme.background,
                  child: Padding(
                      padding: const EdgeInsets.all(16),
                      child: _buildView(context)))),
        );
      },
    );
  }
 

  Widget typeListView(BuildContext context) {
    return Obx(() {
      final type = controller.state.type;
      return Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        mainAxisSize: MainAxisSize.max,
        children: controller.state.typeList
            .map((element) => Expanded(
                flex: 1,
                child: InkWell(
                    onTap: () => controller.clickType(element),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: [
                        const SizedBox(height: 5),
                        Text(element.name,
                            style: Theme.of(context).textTheme.bodyMedium),
                        const SizedBox(height: 5),
                        O2RadioView(isSelected: element.key == type)
                      ],
                    ))))
            .toList(),
      );
    });
  }
}
