import 'package:get/get.dart';

import '../../../../../../common/models/index.dart';

class EmpowerCreateState {
  // title
  final _title = "".obs;
  set title(value) => _title.value = value;
  get title => _title.value;


  // 身份列表
  RxList<O2Identity> identityList = <O2Identity>[].obs;

  
  RxList<EmpowerTypeData> typeList = <EmpowerTypeData>[].obs;

  final _identity = "".obs;
  set identity(String value) => _identity.value = value;
  String get identity => _identity.value;

  final _toPerson = "".obs;
  set toPerson(String value) => _toPerson.value = value;
  String get toPerson => _toPerson.value;

  final _startTime = "".obs;
  set startTime(String value) => _startTime.value = value;
  String get startTime => _startTime.value;

  final _endTime = "".obs;
  set endTime(String value) => _endTime.value = value;
  String get endTime => _endTime.value;

  final _type = "".obs;
  set type(String value) => _type.value = value;
  String get type => _type.value;

  final _appName = "".obs;
  set appName(String value) => _appName.value = value;
  String get appName => _appName.value;

  final _processName = "".obs;
  set processName(String value) => _processName.value = value;
  String get processName => _processName.value;
}
