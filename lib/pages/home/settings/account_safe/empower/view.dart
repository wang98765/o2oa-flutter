import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:get/get.dart';
import 'package:o2oa_all_platform/common/extension/string_extension.dart';

import '../../../../../common/models/index.dart';
import '../../../../../common/style/index.dart';
import 'index.dart';

class EmpowerPage extends GetView<EmpowerController> {
  const EmpowerPage({Key? key}) : super(key: key);

  // 主视图
  Widget _buildView(BuildContext context) {
    return Column(
      children: [
        const SizedBox(height: 10),
        tabBar(context),
        const SizedBox(height: 10),
        Expanded(
            flex: 1,
            child: Container(
                color: Theme.of(context).colorScheme.background,
                child: Obx(
                  () => ListView.separated(
                      itemBuilder: (context, index) {
                        final item = controller.state.empowerList[index];
                        String person;
                        if (controller.state.listType.value ==
                            EmpowerType.myEmpower) {
                          person = item.toPerson ?? '';
                        } else {
                          person = item.fromPerson ?? '';
                        }
                        person = person.o2NameCut();
                        var type = item.type ?? '';
                        switch (type) {
                          case 'all':
                            type = 'account_safe_empower_type_all'.tr;
                            break;
                          case 'application':
                            type =
                                '${'account_safe_empower_type_app'.tr}【${item.applicationName ?? ''}】';
                            break;
                          case 'process':
                            type =
                                '${'account_safe_empower_type_process'.tr}【${item.processName ?? ''}】';
                            break;
                        }
                        return ListTile(
                          onTap: ()=> controller.deleteItem(item),
                            title: Text(person),
                            subtitle: Text(
                                '${item.startTime} - ${item.completedTime}'),
                            trailing: Text(type));
                      },
                      separatorBuilder: (context, index) {
                        return const Divider(
                          height: 1,
                        );
                      },
                      itemCount: controller.state.empowerList.length),
                )))
      ],
    );
  }

  @override
  Widget build(BuildContext context) {
    return GetBuilder<EmpowerController>(
      builder: (_) {
        return Scaffold(
          appBar: AppBar(title: Text('account_safe_empower'.tr), actions: [
            TextButton(
                onPressed: controller.goCreateEmpower,
                child: Text('create'.tr, style: AppTheme.whitePrimaryTextStyle))
          ]),
          body: SafeArea(
            child: Container(
                color: Theme.of(context).scaffoldBackgroundColor,
                child: _buildView(context)),
          ),
        );
      },
    );
  }

  Widget tabBar(BuildContext context) {
    return Obx(() => Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            GestureDetector(
              onTap: () => controller.changeType(EmpowerType.myEmpower),
              child: Padding(
                  padding: const EdgeInsets.only(
                      left: 10, right: 10, top: 5, bottom: 5),
                  child: Text(
                    'account_safe_empower_my'.tr,
                    style: TextStyle(
                        color: controller.state.listType.value ==
                                EmpowerType.myEmpower
                            ? Theme.of(context).colorScheme.primary
                            : Theme.of(context).colorScheme.secondary,
                        fontSize: 18.sp,
                        fontWeight: FontWeight.bold),
                  )),
            ),
            const SizedBox(width: 15),
            GestureDetector(
              onTap: () => controller.changeType(EmpowerType.empowerToMe),
              child: Padding(
                  padding: const EdgeInsets.only(
                      left: 10, right: 10, top: 5, bottom: 5),
                  child: Text(
                    'account_safe_empower_to_me'.tr,
                    style: TextStyle(
                        color: controller.state.listType.value ==
                                EmpowerType.empowerToMe
                            ? Theme.of(context).colorScheme.primary
                            : Theme.of(context).colorScheme.secondary,
                        fontSize: 18.sp,
                        fontWeight: FontWeight.bold),
                  )),
            )
          ],
        ));
  }
}
