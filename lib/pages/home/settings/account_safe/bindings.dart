import 'package:get/get.dart';

import 'controller.dart';

class AccountSafeBinding implements Bindings {
  @override
  void dependencies() {
    Get.lazyPut<AccountSafeController>(() => AccountSafeController());
  }
}
