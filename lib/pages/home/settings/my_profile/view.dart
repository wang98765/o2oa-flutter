import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:get/get.dart';

import '../../../../common/utils/o2_api_manager.dart';
import '../../../../common/widgets/index.dart';
import 'index.dart';

///
/// 我的页面
/// 个人详情
///
class MyProfilePage extends GetView<MyProfileController> {
  const MyProfilePage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return GetBuilder<MyProfileController>(
      builder: (_) {
        return Scaffold(
          appBar: AppBar(title: Text('my_profile_title'.tr)),
          body: SafeArea(
            child: ListView(
              children: [
                const SizedBox(height: 8),
                O2UI.sectionOutBox(
                    Column(children: [
                      InkWell(
                          onTap: () {
                            O2UI.showBottomSheetWithCancel(context, [
                              ListTile(
                                onTap: () {
                                  Navigator.pop(context);
                                  controller.editAvatar(0);
                                },
                                title: Align(
                                  alignment: Alignment.center,
                                  child: Text('album'.tr,
                                      style: Theme.of(context)
                                          .textTheme
                                          .bodyMedium),
                                ),
                              ),
                              const Divider(height: 1),
                              ListTile(
                                onTap: () {
                                  Navigator.pop(context);
                                  controller.editAvatar(1);
                                },
                                title: Align(
                                  alignment: Alignment.center,
                                  child: Text('take_photo'.tr,
                                      style: Theme.of(context)
                                          .textTheme
                                          .bodyMedium),
                                ),
                              ),
                            ]);
                          },
                          child: O2UI.lineWidget(
                            'my_profile_avatar'.tr,
                            Obx(() => SizedBox(
                                  width: 50.w,
                                  height: 50.w,
                                  child: O2UI.personAvatar(
                                      controller.state.dn, 25.w),
                                )),
                          )),
                      const Divider(height: 1),
                      O2UI.lineWidget('my_profile_name'.tr,
                          Text(O2ApiManager.instance.o2User?.name ?? '')),
                      const Divider(height: 1),
                      O2UI.lineWidget('my_profile_employee'.tr,
                          Text(O2ApiManager.instance.o2User?.employee ?? '')),
                    ]),
                    context),
                const SizedBox(height: 10),
                O2UI.sectionOutBox(
                    Obx(() => Column(children: [
                          O2UI.lineWidget(
                              'my_profile_email'.tr,
                              Text(controller.state.email,
                                  style: Theme.of(context).textTheme.bodySmall),
                              ontap: () {
                            // 修改邮件地址
                            controller.goEdit(MyProfileEditType.mail);
                          }),
                          const Divider(height: 1),
                          O2UI.lineWidget(
                              'my_profile_mobile'.tr,
                              Text(controller.state.mobile,
                                  style: Theme.of(context).textTheme.bodySmall),
                              ontap: () {
                            // 修改 手机号码
                            controller.goEdit(MyProfileEditType.mobile);
                          }),
                          const Divider(height: 1),
                          O2UI.lineWidget(
                              'my_profile_office_phone'.tr,
                              Text(controller.state.officePhone,
                                  style: Theme.of(context).textTheme.bodySmall),
                              ontap: () {
                            // 修改 办公电话
                            controller.goEdit(MyProfileEditType.officePhone);
                          }),
                          const Divider(height: 1),
                          O2UI.lineWidget(
                              'my_profile_signature'.tr,
                              Text(controller.state.signature,
                                  style: Theme.of(context).textTheme.bodySmall),
                              ontap: () {
                            // 修改 个性签名
                            controller.goEdit(MyProfileEditType.signature);
                          }),
                        ])),
                    context)
              ],
            ),
          ),
        );
      },
    );
  }
}
