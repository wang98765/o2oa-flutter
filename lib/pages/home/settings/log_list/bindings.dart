import 'package:get/get.dart';

import 'controller.dart';

class LogListBinding implements Bindings {
  @override
  void dependencies() {
    Get.lazyPut<LogListController>(() => LogListController());
  }
}
