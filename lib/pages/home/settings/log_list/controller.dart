import 'dart:io';

import 'package:get/get.dart';
import 'package:o2oa_all_platform/common/index.dart';
import 'package:url_launcher/url_launcher.dart';
// import 'package:open_file/open_file.dart';

import 'index.dart';

class LogListController extends GetxController {
  LogListController();

  final state = LogListState();
  final channel = O2FlutterMethodChannelUtils();

  // tap
  void handleTap(int index) {
    Get.snackbar(
      "标题",
      "消息",
    );
  }

  /// 在 widget 内存中分配后立即调用。
  @override
  void onInit() {
    super.onInit();
  }

  /// 在 onInit() 之后调用 1 帧。这是进入的理想场所
  @override
  void onReady() {
    loadLogList();
    super.onReady();
  }

  /// 在 [onDelete] 方法之前调用。
  @override
  void onClose() {
    super.onClose();
  }

  /// dispose 释放内存
  @override
  void dispose() {
    super.dispose();
  }

  void openLogFile(String filePath, String fileName) {
    OLogger.d('打开日志文件， path： $filePath');
    if (GetPlatform.isDesktop) {
      openInPc(filePath);
    } else {
      channel.openLocalFile(filePath);
    }
  }

  openInPc(String filePath) async {
    // 使用本地软件打开文件
    final Uri uri = Uri.file(filePath);
    if (!await launchUrl(uri)) {
      OLogger.e('无法打开？');
    }
  }

  Future<void> loadLogList() async {
    final dir = await O2FilePathUtil.getLogPath();
    if (dir != null) {
      final list = dir.listSync();
      if (list.isNotEmpty) {
        for (var element in list) {
          if (element is File) {
            state.logList.add(element);
          }
        }
      }
    }
  }
}
