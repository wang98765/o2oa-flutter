import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:get/get.dart';

import '../../../../common/api/index.dart';
import '../../../../common/style/index.dart';
import 'index.dart';

class AboutPage extends GetView<AboutController> {
  const AboutPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return GetBuilder<AboutController>(
      builder: (_) {
        return Scaffold(
          appBar: AppBar(title: Text("settings_about".tr)),
          body: SafeArea(
            child: Column(
              children: [
                Obx(() => Container(
                      width: double.infinity,
                      color: Theme.of(context).colorScheme.background,
                      child: Column(
                          crossAxisAlignment: CrossAxisAlignment.center,
                          children: [
                            SizedBox(height: 24.w),
                            ProgramCenterService.to.aboutLogoImageView(),
                            const SizedBox(height: 10),
                            Text('settings_about_version'
                                .trArgs([controller.state.versionName])),
                            SizedBox(height: 24.w),
                          ]),
                    )),
                Obx(() => Visibility(
                  visible: controller.state.isAndroid,
                  child: const SizedBox(height: 1),
                )),
                Obx(() => Visibility(
                      visible: controller.state.isAndroid,
                      child: Container(
                          width: double.infinity,
                          color: Theme.of(context).colorScheme.background,
                          child: ListTile(
                            title: Text('common_app_update_check_switch'.tr),
                            trailing: Switch(
                              value: controller.state.isOpenUpdate,
                              onChanged: (bool value) {
                                controller.switchUpdateCheck(value);
                              },
                            ),
                          )),
                    )),
                Obx(() => Visibility(
                      visible: controller.state.isAndroid,
                      child: const SizedBox(height: 1),
                    )),
                Obx(() => Visibility(
                      visible: controller.state.isAndroid,
                      child: Container(
                          width: double.infinity,
                          color: Theme.of(context).colorScheme.background,
                          child: ListTile(
                            onTap: controller.checkUpdate,
                            title: Text('common_app_update_chenck'.tr),
                            trailing: const Icon(
                              Icons.keyboard_arrow_right,
                              color: AppColor.hintText,
                              size: 22,
                            ),
                          )),
                    )),
                const SizedBox(height: 5),
                Obx(() => Visibility(
                      visible: !controller.state.isInner,
                      child: Container(
                          width: double.infinity,
                          color: Theme.of(context).colorScheme.background,
                          child: ListTile(
                            onTap: controller.openUserAgreement,
                            title: Text('user_agreement'.tr),
                            trailing: const Icon(
                              Icons.keyboard_arrow_right,
                              color: AppColor.hintText,
                              size: 22,
                            ),
                          )),
                    )),
                Obx(() => Visibility(
                      visible: !controller.state.isInner,
                      child: const SizedBox(height: 1),
                    )),
                Obx(() => Visibility(
                    visible: !controller.state.isInner,
                    child: Container(
                        width: double.infinity,
                        color: Theme.of(context).colorScheme.background,
                        child: ListTile(
                          onTap: controller.openPrivacyPolicy,
                          title: Text('privacy_policy'.tr),
                          trailing: const Icon(
                            Icons.keyboard_arrow_right,
                            color: AppColor.hintText,
                            size: 22,
                          ),
                        )))),
              ],
            ),
          ),
        );
      },
    );
  }
}
