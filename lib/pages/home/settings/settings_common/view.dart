import 'package:flutter/material.dart';
import 'package:get/get.dart';

import '../../../../common/widgets/index.dart';
import 'index.dart';

class SettingsCommonPage extends GetView<SettingsCommonController> {
  const SettingsCommonPage({Key? key}) : super(key: key);
 

  @override
  Widget build(BuildContext context) {
    return GetBuilder<SettingsCommonController>(
      builder: (_) {
        return Scaffold(
          appBar: AppBar(title: Text("settings_common".tr)),
          body: SafeArea(
            child: ListView(
              children: [
                const SizedBox(height: 10),
                O2UI.sectionOutBox(
                    O2UI.lineWidget('settings_logs'.tr,
                          const SizedBox(),
                          ontap: controller.openLogs),
                    context),
                const SizedBox(height: 10),
                O2UI.sectionOutBox(
                    O2UI.lineWidget('settings_clear_cache'.tr,
                          Obx(() => Text(
                              controller.state.cacheSize,
                              style: Theme.of(context).textTheme.bodySmall,
                            )),
                          ontap: controller.clickClearCache),
                    context),
                const SizedBox(height: 10),
                O2UI.sectionOutBox(
                    O2UI.lineWidget('settings_common_change_language'.tr,
                          Obx(() => Text(
                              controller.state.languageName,
                              style: Theme.of(context).textTheme.bodySmall,
                            )),
                          ontap: controller.changeLanguage),
                    context),
              ],
            ),
          ),
        );
      },
    );
  }
}
