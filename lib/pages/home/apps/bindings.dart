import 'package:get/get.dart';

import 'controller.dart';

class AppsBinding implements Bindings {
  @override
  void dependencies() {
    Get.lazyPut<AppsController>(() => AppsController());
  }
}
