import 'package:get/get.dart';

import '../../../common/models/index.dart';

class AppsState {
  // title
  final _title = "".obs;
  set title(value) => _title.value = value;
  get title => _title.value;

  // 是否编辑状态
  final _isEdit = false.obs;
  set isEdit(bool value) => _isEdit.value = value;
  bool get isEdit => _isEdit.value;


  // 我的首页应用
  final RxList<AppFrontData?> myAppList = <AppFrontData?>[].obs;
  // 原生应用列表
  final RxList<AppFrontData?> nativeList = <AppFrontData?>[].obs;
  // 门户应用列表
  final RxList<AppFrontData?> portalList = <AppFrontData?>[].obs;

}
