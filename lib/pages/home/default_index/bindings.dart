import 'package:get/get.dart';

import 'controller.dart';

class DefaultIndexBinding implements Bindings {
  @override
  void dependencies() {
    Get.lazyPut<DefaultIndexController>(() => DefaultIndexController());
  }
}
