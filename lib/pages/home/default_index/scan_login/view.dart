import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:o2oa_all_platform/common/widgets/index.dart';

import '../../../../common/routers/index.dart';
import 'index.dart';

class ScanLoginPage extends GetView<ScanLoginController> {
  const ScanLoginPage({Key? key}) : super(key: key);

  static void openLogin2Pc(String meta) {
    Get.toNamed(O2OARoutes.homeIndexLogin2PC, arguments: {'meta': meta} );
  }
 
  @override
  Widget build(BuildContext context) {
    return GetBuilder<ScanLoginController>(
      builder: (_) {
        return Scaffold(
          appBar: AppBar(title: Text("scan_login_to_pc".tr)),
          body: SafeArea(
            child: Column(children: [
              const SizedBox(height: 100),
              const AssetsImageView('scan_login_content.png', width: 100, height: 100),
              const SizedBox(height: 10,),
              Text('scan_login_to_pc_msg'.tr, style: const TextStyle().copyWith(fontSize: 16)),
              Padding(
                        padding: const EdgeInsets.only(
                            top: 40, bottom: 10, left: 15, right: 15),
                        child: SizedBox(
                            width: double.infinity,
                            height: 44,
                            child: O2ElevatedButton(() {
                              // 点击
                              controller.clickLoginPc();
                            },
                                Text(
                                  'scan_login_to_pc_confirm'.tr,
                                  style: const TextStyle(fontSize: 18),
                                ))),
                      )
            ]),
          ),
        );
      },
    );
  }
}
