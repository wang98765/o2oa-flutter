import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:get/get.dart';

import '../index.dart';

/// List
class CmsListWidget extends GetView<DefaultIndexController> {
  const CmsListWidget({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Obx(() => Column(
          mainAxisSize: MainAxisSize.max,
          children: controller.state.cmsList.map((document) {
            return Container(
                decoration: BoxDecoration(
                  borderRadius: const BorderRadius.all(Radius.circular(10)),
                  color: Theme.of(context).colorScheme.background,
                ),
                margin: EdgeInsets.only(bottom: 10.w, left: 10.w, right: 10.w),
                child: ListTile(
                  title: Text(
                    document.title ?? '${document.categoryName}-无标题',
                    style: Theme.of(context).textTheme.bodyLarge,
                  ),
                  subtitle: Text(
                    document.categoryName ?? '',
                    style: Theme.of(context).textTheme.bodyMedium,
                  ),
                  trailing: Text(
                    document.publishTime?.substring(0, 10) ?? '',
                    style: Theme.of(context).textTheme.bodySmall,
                  ),
                  onTap: () => controller.tapDocument(document),
                ));
          }).toList(),
        ));
  }
}
