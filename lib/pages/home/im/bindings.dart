import 'package:get/get.dart';

import 'controller.dart';

class ImBinding implements Bindings {
  @override
  void dependencies() {
    Get.lazyPut<ImController>(() => ImController());
  }
}
