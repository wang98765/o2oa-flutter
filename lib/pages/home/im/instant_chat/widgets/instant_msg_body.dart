
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:get/get.dart';

import '../../../../../common/models/index.dart';
import '../../../../../common/style/index.dart';
import '../../../../../common/utils/index.dart';
import '../index.dart';

///
/// 消息体 UI
///
class InstantMsgBodyWidget extends GetView<InstantChatController> {
  final InstantMsg msg;
  const InstantMsgBodyWidget({
    Key? key,
    required this.msg,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    // var padding = const EdgeInsets.only(left: 10);
    final type = msg.type;
    if (type != null &&
        (type.startsWith('task_') ||
            type.startsWith('taskCompleted_') ||
            type.startsWith('read_') ||
            type.startsWith('readCompleted_') ||
            type.startsWith('review_') ||
            type.startsWith('work_') ||
            type.startsWith('process_'))) {
      return processMsgView(context);
    } else if (msg.isCustomType()) {
      if (msg.body == null || msg.body!.isEmpty == true || msg.body == 'null' || msg.body == '{}') {
        return textMsgView(msg.title ?? '');
      }
      final body =
          InstantCustomO2AppMsgBody.fromJson(O2Utils.parseStringToJson(msg.body ?? '{}'));
      switch (body.o2AppMsg?.msgtype) {
        case 'text':
          var text = body.o2AppMsg?.text?.content;
          text ??= msg.title;
          final url = body.o2AppMsg?.text?.url;
          return textMsgView(text ?? '',
              underline: (url != null && url.isNotEmpty));
        case 'image':
          final imageUrl = body.o2AppMsg?.image?.url;
          return imageMsgView(imageUrl);
        case 'textcard':
          final title = body.o2AppMsg?.textcard?.title ?? '';
          final desc = body.o2AppMsg?.textcard?.desc ?? '';
          final url = body.o2AppMsg?.textcard?.url ?? '';
          if (title.isEmpty || desc.isEmpty || url.isEmpty) {
            return textMsgView(msg.title ?? '');
          } else {
            return textCardMsgView(context, title, desc);
          }
        default:
          return textMsgView(msg.title ?? '');
      }
    } else {
      return textMsgView(msg.title ?? '');
    }
    // switch (msg.type) {
    //   case "text":

    //   case "emoji":
    //     return Padding(
    //         padding: padding,
    //         child: Image.asset(
    //           O2Emoji.getEmojiPath(msgBody.body!),
    //           width: 32,
    //           height: 32,
    //         ));
    //   case "image":
    //     // 本地图片
    //     if (msgBody.fileTempPath != null && msgBody.fileTempPath!.isNotEmpty) {
    //       return Padding(
    //           padding: padding,
    //           child: Image.file(File(msgBody.fileTempPath!),
    //               width: 144, height: 192));
    //     }
    //     var imageUrl =
    //         MessageCommunicationService.to.getIMMsgImageUrl(msgBody.fileId!);
    //     return Padding(
    //         padding: padding,
    //         child: Image.network(imageUrl,
    //             headers: controller.headers, width: 144, height: 192));
    //   case "audio":
    //     return voiceBodyView(isSender);
    //   case "location":
    //     return Padding(
    //         padding: padding, child: locationMsgView(msgBody.address ?? ''));
    //   case "file":
    //     return Padding(padding: padding, child: fileMsgView(context, msgBody, isSender));
    //   case "process":
    //     return processMsgView(context);
    //   case "cms":
    //     return textBodyView('文档');
    // }
    // return Container();
  }

  ///
  /// 文本消息
  ///
  Widget textMsgView(String text, {bool underline = false}) {
    return Text(
      text,
      style: TextStyle(
          color: Colors.black,
          fontSize: 14,
          decoration: (underline ? TextDecoration.underline : null)),
    );
  }

  ///
  /// 图片消息
  ///
  Widget imageMsgView(String? imageUrl) {
    if (imageUrl == null || imageUrl.isEmpty) {
      return Padding(
        padding: const EdgeInsets.only(left: 10),
        child: Icon(Icons.image, size: 48.w),
      );
    }
    return Padding(
        padding: const EdgeInsets.only(left: 10),
        child: Image.network(imageUrl,
            headers: controller.headers, width: 144, height: 192));
  }

  ///
  /// 卡片消息
  /// 
  Widget textCardMsgView(BuildContext context, String title, String desc) {
    return Container(
        width: 200,
        decoration: BoxDecoration(
            borderRadius: const BorderRadius.all(Radius.circular(10)),
            color: Theme.of(context).colorScheme.background),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Padding(
                padding: const EdgeInsets.only(top: 10, left: 10),
                child: Text(title,
                    maxLines: 1, style: Theme.of(context).textTheme.titleMedium)),
            Padding(
                padding: const EdgeInsets.only(top: 10, bottom: 10, left: 12),
                child:
                    Text(desc, style: Theme.of(context).textTheme.bodySmall)),
            const Divider(
              height: 1,
            ),
            Padding(
                padding: const EdgeInsets.only(top: 10, bottom: 10, left: 5),
                child: Row(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: [
                    Text('im_instant_msg_detail_btn_title'.tr,
                        style: Theme.of(context).textTheme.bodySmall),
                    const Spacer(),
                    Icon(Icons.arrow_forward_ios, size: 22.w, color: AppColor.hintText,),
                    const SizedBox(width: 5),
                  ],
                ))
          ],
        ));
  }

  ///
  /// 流程卡片消息
  ///
  Widget processMsgView(BuildContext context) {
    var body = O2Utils.parseStringToJson(msg.body ?? '{}'); // 消息体 业务对象 流程相关的业务对象
    var title = body['title'] ?? ''; // 有没有 title 字段
    if (title.isEmpty) {
      title = 'process_work_no_title_no_process'.tr;
    }
    var application = body['application'] ?? ''; // 有没有 application 字段
    var applicationName =
        body['applicationName'] ?? ''; // 有没有 applicationName 字段
    var processName = body['processName'] ?? ''; // 有没有 processName 字段
    controller.getProcessAppIcon(application); // 下载application icon
    return Container(
        width: 200,
        decoration: BoxDecoration(
            borderRadius: const BorderRadius.all(Radius.circular(10)),
            color: Theme.of(context).colorScheme.background),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Padding(
                padding: const EdgeInsets.only(top: 10),
                child: Text('【$processName】',
                    style: Theme.of(context).textTheme.bodyMedium)),
            Padding(
                padding: const EdgeInsets.only(top: 10, bottom: 10, left: 10),
                child:
                    Text(title, style: Theme.of(context).textTheme.bodyLarge)),
            const Divider(
              height: 1,
            ),
            Padding(
                padding: const EdgeInsets.only(top: 10, bottom: 10, left: 5),
                child: Row(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: [
                    const SizedBox(width: 5),
                    Obx(() => controller.state.processAppIconMap[application] ==
                            null
                        ? Icon(Icons.image, size: 22.w)
                        : Image.memory(
                            controller.state.processAppIconMap[application]!,
                            width: 22.w,
                            height: 22.w,
                            fit: BoxFit.cover,
                          )),
                    const SizedBox(width: 10),
                    Text('$applicationName',
                        style: Theme.of(context).textTheme.bodySmall),
                  ],
                )),
          ],
        ));
  }
}
