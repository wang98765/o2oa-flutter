import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:get/get.dart';
import 'package:o2oa_all_platform/common/extension/index.dart';

import '../../../../common/models/index.dart';
import '../../../../common/routers/index.dart';
import '../../../../common/widgets/index.dart';
import 'index.dart';
import 'widgets/widgets.dart';

class InstantChatPage extends GetView<InstantChatController> {
  const InstantChatPage({Key? key}) : super(key: key);

  static Future<void> open(List<InstantMsg> instantMsgList) async {
    await Get.toNamed(O2OARoutes.homeImChatInstant,
        arguments: {"instantMsgList": instantMsgList});
  }

  // 主视图
  Widget _buildView() {
    return Padding(
        padding: const EdgeInsets.only(right: 10.0, left: 10.0),
        child: Obx(() => Align(
            alignment: Alignment.topCenter,
            child: ListView.separated(
                padding: const EdgeInsets.only(bottom: 10),
                reverse: true, // 反转
                shrinkWrap: true,
                itemBuilder: (BuildContext context, int index) {
                  InstantMsg info = controller.state.instantMsgList[index];
                  return GestureDetector(
                      onTap: () {
                        controller.clickInstantItem(info);
                      },
                      child: receivedLisItemView(context, info, index));
                },
                separatorBuilder: (context, index) {
                  return const SizedBox(height: 1);
                },
                itemCount: controller.state.instantMsgList.length))));
  }

  // 接收到的消息格式
  Widget receivedLisItemView(BuildContext context, InstantMsg info, int index) {
    var showTime = _showTime(info, index);
    return Column(
      children: [
        const SizedBox(height: 10),
        showTime.isEmpty
            ? Container()
            : Text(showTime, style: Theme.of(context).textTheme.bodySmall),
        Padding(
            padding: const EdgeInsets.only(right: 50.0, top: 5, bottom: 5),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.start,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                // 头像
                AssetsImageView(
                  controller.messageTypeAvatar(info.type ?? ''),
                  width: 50.w,
                  height: 50.w,
                ),
                Expanded(
                    flex: 1, child: InstantReceivedMessageWidget(msgBody: info))
              ],
            ))
      ],
    );
  }

  String _showTime(InstantMsg info, int index) {
    var showTime = '';
    DateTime? time = DateTime.tryParse(info.createTime ?? "");
    if (time != null) {
      if (index == 0) {
        //第一条显示时间
        showTime = time.chatMsgShowTimeFormat();
      } else {
        InstantMsg lastMsg = controller.state.instantMsgList[index - 1];
        DateTime? lastTime = DateTime.tryParse(lastMsg.createTime ?? "");
        if (lastTime != null && lastTime.difference(time).inMinutes > 1) {
          // 超过一分钟 显示时间
          showTime = time.chatMsgShowTimeFormat();
        }
      }
    }
    return showTime;
  }

  @override
  Widget build(BuildContext context) {
    return GetBuilder<InstantChatController>(
      builder: (_) {
        return Scaffold(
          appBar: AppBar(title: Text('im_instant_msg_title'.tr)),
          body: SafeArea(
            child: _buildView(),
          ),
        );
      },
    );
  }
}
