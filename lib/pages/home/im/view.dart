import 'package:flutter/material.dart';
import 'package:get/get.dart';

import '../../../common/style/index.dart';
import 'index.dart';
import 'widgets/widgets.dart';

class ImPage extends GetView<ImController> {
  const ImPage({Key? key}) : super(key: key);

  // 主视图
  Widget _buildView() {
    return const ConversationListWidget();
  }

  @override
  Widget build(BuildContext context) {
    return GetBuilder<ImController>(
      builder: (_) {
        return Scaffold(
          appBar: AppBar(title: Text('home_tab_im'.tr), actions: [
            TextButton(onPressed: ()=> controller.startSingleChat(), child: Text('im_action_single'.tr, style: AppTheme.whitePrimaryTextStyle)),
            TextButton(onPressed: ()=> controller.startGroupChat(), child: Text('im_action_group'.tr,  style: AppTheme.whitePrimaryTextStyle)),
          ],),
          body: SafeArea(
            child: Container(
                color: Theme.of(context).scaffoldBackgroundColor,
                child: _buildView(),
            )
          ),
        );
      },
    );
  }
}
