import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:get/get.dart';
import 'package:o2oa_all_platform/common/extension/string_extension.dart';

import '../../../../common/models/index.dart';
import '../../../../common/routers/index.dart';
import '../../../../common/utils/index.dart';
import '../../../../common/widgets/index.dart';
import 'index.dart';

class GroupMembersPage extends GetView<GroupMembersController> {
  const GroupMembersPage({Key? key}) : super(key: key);

  ///  修改群成员
  static Future<void> updateMember(IMConversationInfo info) async {
    await Get.toNamed(O2OARoutes.homeImChatGroupMembers,
        arguments: {"conversationInfo": info, "update": true});
  }

  /// 查看群信息
  static Future<void> showGroupInfo(IMConversationInfo info) async {
    await Get.toNamed(O2OARoutes.homeImChatGroupMembers,
        arguments: {"conversationInfo": info, "update": false});
  }

  @override
  Widget build(BuildContext context) {
    return GetBuilder<GroupMembersController>(
      builder: (_) {
        return Scaffold(
          appBar: AppBar(
              title: Obx(() => Text(controller.state.canUpdate
                  ? 'im_chat_group_member_title'.tr
                  : 'im_chat_group_info_title'.tr))),
          body: SafeArea(
              child: Padding(
                  padding: const EdgeInsets.all(10),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      const SizedBox(height: 10),
                      Text('im_chat_group_info_label_name'.tr),
                      const SizedBox(height: 10),
                      Padding(
                          padding: const EdgeInsets.only(left: 10, bottom: 10),
                          child: Obx(() => Text(controller.state.groupName,
                              style: Theme.of(context).textTheme.bodyMedium))),
                      const Divider(height: 1),
                      const SizedBox(height: 10),
                      Text('im_chat_group_info_label_admin'.tr),
                      const SizedBox(height: 10),
                      Padding(
                          padding: const EdgeInsets.only(left: 10, bottom: 10),
                          child: Obx(() => Text(controller.state.groupAdminName,
                              style: Theme.of(context).textTheme.bodyMedium))),
                      const Divider(height: 1),
                      const SizedBox(height: 10),
                      Text('im_chat_group_info_label_member'.tr),
                      const SizedBox(height: 10),
                      Expanded(
                        flex: 1,
                        child: gridView(),
                      )
                    ],
                  ))),
        );
      },
    );
  }

  ///  群成员
  Widget gridView() {
    return Obx(() => GridView.builder(
        itemCount: controller.state.canUpdate
            ? controller.state.memberList.length + 1
            : controller.state.memberList.length,
        gridDelegate: const SliverGridDelegateWithFixedCrossAxisCount(
          crossAxisCount: 5,
          mainAxisSpacing: 10.0, //设置上下之间的间距(Axis.horizontal设置左右之间的间距)
          crossAxisSpacing: 10.0, //设置左右之间的间距(Axis.horizontal设置上下之间的间距)
          childAspectRatio: 0.8, //设置宽高比
        ),
        itemBuilder: (context, index) {
          if (controller.state.canUpdate && index == 0) {
            return InkWell(
                onTap: () => controller.clickAddMember(),
                child: itemView(
                    AssetsImageView(
                      'icon_add_people.png',
                      width: 48.w,
                      height: 48.w,
                    ),
                    'im_chat_group_member_add'.tr,
                    context));
          }
          var dn = controller.state.canUpdate
              ? controller.state.memberList[index - 1]
              : controller.state.memberList[index];
          var name = dn.o2NameCut();
          return InkWell(
              onTap: () => controller.clickDeleteMember(dn),
              child: Stack(
                children: [
                  itemView(
                      SizedBox(
                        width: 48.w,
                        height: 48.w,
                        child: O2UI.personAvatar(dn, 24.w),
                      ),
                      name,
                      context),
                  Visibility(
                    visible: controller.state.canUpdate && dn != O2ApiManager.instance.o2User?.distinguishedName,
                    child: const Positioned(
                        right: 0,
                        top: 0,
                        child: AssetsImageView(
                          'icon_delete_people.png',
                          width: 14,
                          height: 14,
                        )),
                  )
                ],
              ));
        }));
  }

  Widget itemView(Widget child, String name, BuildContext context) {
    return Column(
      children: [
        // 头像
        child,
        Expanded(
            flex: 1,
            child: Center(
                child: Text(name,
                    softWrap: true,
                    textAlign: TextAlign.left,
                    overflow: TextOverflow.ellipsis,
                    maxLines: 1,
                    style: Theme.of(context).textTheme.bodyMedium)))
      ],
    );
  }
}
