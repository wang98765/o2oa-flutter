import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:get/get.dart';
import 'package:o2oa_all_platform/common/index.dart';

import '../index.dart';

/// 
/// 更多工具
class ImMoreToolListWidget extends GetView<ImChatController> {
  const ImMoreToolListWidget({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.all(10),
      child: GridView.count(
          shrinkWrap: true,
          crossAxisCount: 4,
          mainAxisSpacing: 20.0, //设置上下之间的间距
          childAspectRatio: 0.7, //设置宽高比
          children: tools(context)),
    );
  }

  ///
  /// 桌面端只支持文件
  /// 
  List<Widget> tools(BuildContext context) {
    if (GetPlatform.isMobile) {
      return [
            InkWell(
                onTap: () => controller.clickChoosePhotosBtn(),
                child: Column(
                  children: [
                    AssetsImageView('chat_icon_photos_big.png',
                        width: 48.w, height: 48.w),
                    const SizedBox(height: 5),
                    Text('im_chat_tools_album'.tr, style: Theme.of(context).textTheme.bodyMedium)
                  ],
                )),
            InkWell(
                onTap: () => controller.clickTakePhotoBtn(),
                child: Column(
                  children: [
                    AssetsImageView('chat_icon_camera_big.png',
                        width: 48.w, height: 48.w),
                    const SizedBox(height: 5),
                    Text('im_chat_tools_take_photo'.tr, style: Theme.of(context).textTheme.bodyMedium)
                  ],
                )),
            InkWell(
              onTap: () => controller.clickChooseFileBtn(),
              child: Column(
                children: [
                  AssetsImageView('chat_icon_file_big.png',
                      width: 48.w, height: 48.w),
                  const SizedBox(height: 5),
                  Text('im_chat_tools_file'.tr, style: Theme.of(context).textTheme.bodyMedium)
                ],
              ),
            ),
            // InkWell(
            //     onTap: () => controller.clickChooseLocationBtn(),
            //     child: Column(
            //       children: [
            //         AssetsImageView('chat_icon_location_big.png',
            //             width: 48.w, height: 48.w),
            //         const SizedBox(height: 5),
            //         Text('im_chat_tools_location'.tr, style: Theme.of(context).textTheme.bodyMedium)
            //       ],
            //     )),
          ];
    }
    return [InkWell(
              onTap: () => controller.clickChooseFileBtn(),
              child: Column(
                children: [
                  AssetsImageView('chat_icon_file_big.png',
                      width: 48.w, height: 48.w),
                  const SizedBox(height: 5),
                  Text('im_chat_tools_file'.tr, style: Theme.of(context).textTheme.bodyMedium)
                ],
              ),
            ) 
          ];
  }
}
