import 'package:get/get.dart';

import 'controller.dart';

class ImChatBinding implements Bindings {
  @override
  void dependencies() {
    Get.lazyPut<ImChatController>(() => ImChatController());
  }
}
