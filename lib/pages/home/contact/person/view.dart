import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:get/get.dart';

import '../../../../common/models/index.dart';
import '../../../../common/routers/index.dart';
import '../../../../common/utils/index.dart';
import '../../../../common/values/index.dart';
import '../../../../common/widgets/index.dart';
import 'index.dart';

class PersonPage extends GetView<PersonController> {
  const PersonPage({Key? key}) : super(key: key);

  static void openPersonInfo(O2Person person) {
    if (person.id == null || person.id!.isEmpty) {
      return;
    }
    Get.toNamed(O2OARoutes.homeContactPerson, arguments: {"id": person.id!});
  }

  static void open(String id) {
    if (id.isEmpty) {
      return;
    }
    Get.toNamed(O2OARoutes.homeContactPerson, arguments: {"id": id});
  }

  @override
  Widget build(BuildContext context) {
    return GetBuilder<PersonController>(
      builder: (_) {
        return Scaffold(
          appBar: AppBar(title: Text("contact_person_info".tr)),
          body: SafeArea(
            child: ListView(
              children: [
                const SizedBox(height: 8),
                baseAttributeView(context),
                const SizedBox(height: 10),
                otherAttributeView(context),
                Obx(() => Visibility(
                    visible: controller.state.personAttributeList
                        .toList()
                        .isNotEmpty,
                    child: const SizedBox(height: 10))),
                Obx(() => Visibility(
                    visible: controller.state.personAttributeList
                        .toList()
                        .isNotEmpty,
                    child: personAttributeView(context))),
                // 发起聊天按钮
                startChatBtnView()
              ],
            ),
          ),
        );
      },
    );
  }

  /// 基础属性
  Widget baseAttributeView(BuildContext context) {
    return O2UI.sectionOutBox(
        Obx(() => Column(children: [
              O2UI.lineWidget(
                'my_profile_avatar'.tr,
                controller.state.person.value == null
                    ? SizedBox(width: 50.w, height: 50.w)
                    : SizedBox(
                        width: 50.w,
                        height: 50.w,
                        child: O2UI.personAvatar(
                            controller.state.person.value!.distinguishedName!,
                            25.w),
                      ),
              ),
              const Divider(height: 1),
              O2UI.lineWidget('my_profile_name'.tr,
                  Text(controller.state.person.value?.name ?? '')),
              // const Divider(height: 1),
              // O2UI.lineWidget('my_profile_unique'.tr,
              //     Text(controller.state.person.value?.unique ?? '')),
              const Divider(height: 1),
              O2UI.lineWidget('my_profile_employee'.tr,
                  Text(controller.state.person.value?.employee ?? '')),
            ])),
        context);
  }

  /// 其他属性
  Widget otherAttributeView(BuildContext context) {
    return O2UI.sectionOutBox(
        Obx(() => Column(children: [
              O2UI.lineWidget(
                'contact_person_info_dept_name'.tr,
                Text(controller.state.departmentNames,
                    style: Theme.of(context).textTheme.bodySmall),
              ),
              const Divider(height: 1),
              O2UI.lineWidget(
                'my_profile_superiorName'.tr,
                Text(controller.state.superiorName,
                    style: Theme.of(context).textTheme.bodySmall),
              ),
              const Divider(height: 1),
              O2UI.lineWidget(
                  'my_profile_email'.tr,
                  Text(controller.state.person.value?.mail ?? '',
                      style: Theme.of(context).textTheme.bodySmall),
                  ontap: () => controller
                      .mailTo(controller.state.person.value?.mail ?? '')),
              const Divider(height: 1),
              O2UI.lineWidget(
                  'my_profile_mobile'.tr,
                  Text(controller.state.person.value?.showMobile ?? '',
                      style: Theme.of(context).textTheme.bodySmall),
                  ontap: controller.state.person.value?.showMobile ==
                          O2.hiddenMobileNumber
                      ? null
                      : () => controller
                          .telTo(controller.state.person.value?.mobile ?? '')),
              const Divider(height: 1),
              O2UI.lineWidget(
                  'my_profile_office_phone'.tr,
                  Text(controller.state.person.value?.officePhone ?? '',
                      style: Theme.of(context).textTheme.bodySmall),
                  ontap: () => controller
                      .telTo(controller.state.person.value?.officePhone ?? '')),
              const Divider(height: 1),
            ])),
        context);
  }

  /// 发起聊天按钮
  Widget startChatBtnView() {
    return Obx(() => Padding(
          padding:
              const EdgeInsets.only(top: 40, bottom: 10, left: 15, right: 15),
          child: O2ApiManager.instance.o2User?.distinguishedName ==
                  controller.state.person.value?.distinguishedName
              ? Container()
              : SizedBox(
                  width: double.infinity,
                  height: 44,
                  child: O2ElevatedButton(() {
                    // 点击
                    controller.startSingleChat();
                  },
                      Text(
                        'im_start_single'.tr,
                        style: const TextStyle(fontSize: 18),
                      ))),
        ));
  }

  /// 人员属性
  Widget personAttributeView(BuildContext context) {
    return O2UI.sectionOutBox(Obx(() {
      final list = controller.state.personAttributeList.toList();
      List<Widget> widgets = [];
      for (var element in list) {
        widgets.add(O2UI.lineWidget(
            element.name ?? '',
            Expanded(
              flex: 1,
              child: Text(
                element.attributeList?.join(',') ?? '',
                style: Theme.of(context).textTheme.bodySmall,
                overflow: TextOverflow.ellipsis,
              ),
            )));

        widgets.add(const Divider(height: 1));
      }
      return Column(children: widgets);
    }), context);
  }
}
