import 'package:get/get.dart';

import 'controller.dart';

class PersonBinding implements Bindings {
  @override
  void dependencies() {
    Get.lazyPut<PersonController>(() => PersonController());
  }
}
