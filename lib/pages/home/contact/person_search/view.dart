import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:get/get.dart';

import '../../../../common/widgets/index.dart';
import 'index.dart';

class PersonSearchPage extends GetView<PersonSearchController> {
  const PersonSearchPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return GetBuilder<PersonSearchController>(
      builder: (_) {
        return Scaffold(
            appBar: AppBar(
                title: Container(
              height: 36.h,
              decoration: BoxDecoration(
                  color: Theme.of(context).scaffoldBackgroundColor,
                  borderRadius: BorderRadius.all(Radius.circular(18.h))),
              alignment: Alignment.centerLeft,
              child: Padding(
                padding: EdgeInsets.only(left: 10.w),
                child: Row(
                  children: [
                    SizedBox(
                      width: 22.h,
                      height: 22.h,
                      child: Icon(Icons.search,
                          color: Theme.of(context).colorScheme.primary),
                    ),
                    Expanded(
                      flex: 1,
                      child: Padding(
                        padding: EdgeInsets.only(left: 5.w),
                        child: TextField(
                          autofocus: true,
                          focusNode: controller.searchNode,
                          controller: controller.searchController,
                          decoration: InputDecoration(
                            isDense: true,
                            border: InputBorder.none,
                            hintText: 'contact_person_search_placeholder'.tr,
                            hintStyle: Theme.of(context).textTheme.bodySmall,
                          ),
                          style: Theme.of(context).textTheme.bodyMedium,
                          textInputAction: TextInputAction.search,
                          onSubmitted: controller.onSearch,
                        ),
                      ),
                    )
                  ],
                ),
              ),
            )),
            body: SafeArea(
                child: Padding(
                    padding: const EdgeInsets.all(15),
                    child: Obx(() => Visibility(
                          visible: controller.state.personList.isNotEmpty,
                          child: Column(children: [
                            Container(
                                width: double.infinity,
                                decoration: BoxDecoration(
                                    borderRadius: const BorderRadius.all(
                                        Radius.circular(10)),
                                    color: Theme.of(context).colorScheme.background),
                                child: Column(
                                    children: controller.state.personList
                                        .map((person) {
                                  return ListTile(
                                    onTap: () =>
                                        controller.clickEnterPerson(person),
                                    leading: SizedBox(
                                      width: 50.w,
                                      height: 50.w,
                                      child: O2UI.personAvatar(
                                          person.distinguishedName!, 25.w),
                                    ),
                                    title: Text(person.name ?? ''),
                                    trailing: O2UI.rightArrow(),
                                  );
                                }).toList())),
                          ]),
                        )))));
      },
    );
  }
}
