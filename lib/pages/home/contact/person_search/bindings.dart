import 'package:get/get.dart';

import 'controller.dart';

class PersonSearchBinding implements Bindings {
  @override
  void dependencies() {
    Get.lazyPut<PersonSearchController>(() => PersonSearchController());
  }
}
