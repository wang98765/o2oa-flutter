import 'package:get/get.dart';

import '../../../common/models/index.dart';

class BaiduMapState {
  // title
  final _title = "".obs;
  set title(value) => _title.value = value;
  get title => _title.value;

  ///0: 选择位置 1: 显示地图位置
  final _mode = LocationData.locationDataModePicker.obs;
  set mode(int m) => _mode.value = m;
  int get mode => _mode.value;


  // address
  final _address = "".obs;
  set address(value) => _address.value = value;
  get address => _address.value;
}
