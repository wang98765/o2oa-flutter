// import 'dart:io';

// import 'package:flutter/material.dart';
// import 'package:flutter_baidu_mapapi_base/flutter_baidu_mapapi_base.dart';
// import 'package:flutter_baidu_mapapi_map/flutter_baidu_mapapi_map.dart';
// import 'package:get/get.dart';
// import 'package:o2oa_all_platform/common/index.dart';

// import 'index.dart';

// class BaiduMapPage extends GetView<BaiduMapController> {
//   const BaiduMapPage({Key? key}) : super(key: key);

//   static Future<dynamic> open(LocationData locaton) async {
//     return await Get.toNamed(O2OARoutes.baiduMap, arguments: locaton);
//   }

//   @override
//   Widget build(BuildContext context) {
//     var screenSize = MediaQuery.of(context).size;
//     return GetBuilder<BaiduMapController>(
//       builder: (_) {
//         return Scaffold(
//           appBar: AppBar(
//             title: Obx(() => Text(controller.state.mode == 0
//                 ? 'bmap_title_choose'.tr
//                 : 'bmap_title'.tr)),
//             actions: [
//               Obx(() => controller.state.mode == 0
//                   ? TextButton(
//                       onPressed: controller.clickSendLocation,
//                       child: Text('bmap_send_location_btn'.tr,
//                           style: AppTheme.whitePrimaryTextStyle))
//                   : TextButton(
//                       onPressed: controller.clickOpenLocation,
//                       child: Text('bmap_open_location_btn'.tr,
//                           style: AppTheme.whitePrimaryTextStyle)))
//             ],
//           ),
//           body: Stack(children: [
//             SizedBox(
//                 height: screenSize.height,
//                 width: screenSize.width,
//                 child: createBaiduMap()),
//             Container(
//               height: 32,
//               width: double.infinity,
//               color: AppColor.whiteTransparentBackground,
//               alignment: Alignment.center,
//               child: Obx(() => Text(
//                     controller.state.address,
//                     softWrap: true,
//                     textAlign: TextAlign.center,
//                     overflow: TextOverflow.ellipsis,
//                     maxLines: 1,
//                     style: const TextStyle(color: AppColor.primaryText),
//                   )),
//             ),
//           ]),
//         );
//       },
//     );
//   }

//   Widget createBaiduMap() {
//     if (Platform.isAndroid) {
//       return BMFTextureMapWidget(
//           onBMFMapCreated: (c) => controller.setBMFMapController(c),
//           mapOptions: controller.initMapOptions());
//     } else {
//       // return UiKitView(
//       //         viewType: 'pulgin.ios.o2oa.net/baidumap',
//       //       );
//       return BMFMapWidget(
//           onBMFMapCreated: (c) => controller.setBMFMapController(c),
//           mapOptions: controller.initMapOptions());
//     }
//   }
// }
