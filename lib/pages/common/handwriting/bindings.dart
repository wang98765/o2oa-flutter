import 'package:get/get.dart';

import 'controller.dart';

class HandwritingBinding implements Bindings {
  @override
  void dependencies() {
    Get.lazyPut<HandwritingController>(() => HandwritingController());
  }
}
