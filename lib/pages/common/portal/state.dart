import 'package:get/get.dart';

class PortalState {
  // title
  final _title = "".obs;
  set title(value) => _title.value = value;
  get title => _title.value;

  final _hiddenAppBar = false.obs;
  set hiddenAppBar(bool value) => _hiddenAppBar.value = value;
  bool get hiddenAppBar => _hiddenAppBar.value;

  // 访问地址
  final _url = "".obs;
  set url(value) => _url.value = value;
  String get url => _url.value;

}
