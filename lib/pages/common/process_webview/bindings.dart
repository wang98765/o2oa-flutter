import 'package:get/get.dart';

import 'controller.dart';

class ProcessWebviewBinding implements Bindings {
  @override
  void dependencies() {
    // Get.lazyPut<ProcessWebviewController>(() => ProcessWebviewController());
    Get.create<ProcessWebviewController>(() => ProcessWebviewController(), permanent: false);
  }
}
