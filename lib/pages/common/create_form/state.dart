import 'package:get/get.dart';

import '../../../common/models/index.dart';

class CreateFormState {
  // title
  final _title = "".obs;
  set title(value) => _title.value = value;
  get title => _title.value;

  // 身份列表
  RxList<O2Identity> identityList = <O2Identity>[].obs;

  final _identity = "".obs;
  set identity(String value) => _identity.value = value;
  String get identity => _identity.value;


  final _ignoreTitle = false.obs;
  set ignoreTitle(bool value) => _ignoreTitle.value = value;
  bool get ignoreTitle => _ignoreTitle.value;
}
