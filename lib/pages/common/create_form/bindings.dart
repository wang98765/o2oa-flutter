import 'package:get/get.dart';

import 'controller.dart';

class CreateFormBinding implements Bindings {
  @override
  void dependencies() {
    Get.lazyPut<CreateFormController>(() => CreateFormController());
  }
}
