import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:get/get.dart';
import 'package:o2oa_all_platform/common/index.dart';
import 'package:video_player/video_player.dart';

import 'index.dart';

class VideoPlayerPage extends GetView<VideoPlayerPageController> {
  const VideoPlayerPage({Key? key}) : super(key: key);

  /// 在线视频播放
  /// [url] 在线视频网络地址
  static Future<void> openNetworkVideo(String url, {String title = ''}) async {
    OLogger.d('在线视频， 地址$url');
    await Get.toNamed(O2OARoutes.commonVideoPlayer,
        arguments: {"url": url, "title": title});
  }

  /// 本地视频播放
  /// [filePath] 本地视频文件地址
  static Future<void> openLocalVideo(String filePath,
      {String title = ''}) async {
    await Get.toNamed(O2OARoutes.commonVideoPlayer,
        arguments: {"filePath": filePath, "title": title});
  }

  @override
  Widget build(BuildContext context) {
    return GetBuilder<VideoPlayerPageController>(
      builder: (_) {
        return Obx(()=> Scaffold(
          appBar: controller.state.isLandscape ? null : AppBar(title: Text(controller.state.title)),
          body: SafeArea(
            child: _buildView(),
          ),
        ));
      },
    );
  }

  Widget _buildView() {
    return Obx(() => controller.state.isInit ? videoView() : loadingView());
  }

  Widget videoView() {
    return GestureDetector(
        onTap: () => controller.clickVideo(),
        child: Stack(children: [
          Center(
              child: AspectRatio(
            aspectRatio: controller.videoPlayerController!.value.aspectRatio,
            child: VideoPlayer(controller.videoPlayerController!),
          )),
          _playButton(),
          _videoProgressView()
        ]));
  }

  /// 播放按钮 停止的时候显示
  Widget _playButton() {
    return Obx(() => Visibility(
          visible: !controller.state.isPlaying,
          child: Container(
              width: double.infinity,
              height: double.infinity,
              color: Colors.black.withOpacity(0.5),
              child: Center(
                child: Icon(Icons.play_circle, color: Colors.white, size: 48.w),
              )),
        ));
  }

  /// 进度条 播放的时候显示
  Widget _videoProgressView() {
    return Obx(() => Visibility(
        visible: controller.state.isPlaying,
        child: Positioned(
          bottom: 10,
          left: 14,
          right: 14,
          child: Container(
              width: double.infinity,
              decoration: BoxDecoration(
                  color: Colors.black.withOpacity(0.5),
                  borderRadius: const BorderRadius.all(Radius.circular(4))),
              child: Padding(
                  padding: const EdgeInsets.only(
                      left: 5, right: 5, top: 10, bottom: 10),
                  child: Row(children: [
                    Expanded(
                        flex: 1,
                        child: Slider(
                            /// 进度条的值
                            value: controller.state.progress,
                            min: 0.0,
                            max: 1.0,
                            activeColor: Colors.white,
                            inactiveColor: Colors.white.withOpacity(0.5),
                            onChanged: (value) => controller.slideChangeProgress(value) ,
                            onChangeStart: (value) => controller.slideChangeProgressStart(value),
                            onChangeEnd: (value) => controller.slideChangeProgressEnd(value),
                          ),
                        // LinearProgressIndicator(
                        //     valueColor: const AlwaysStoppedAnimation<Color>(
                        //         Colors.white),
                        //     backgroundColor: Colors.white.withOpacity(0.5),
                        //     value: controller.state.progress)
                            ),
                    const SizedBox(width: 5),
                    Text(
                        '${controller.state.position.hms()}/${controller.state.duration.hms()}',
                        style: TextStyle(fontSize: 12.sp, color: Colors.white)),
                    const SizedBox(width: 5),
                    IconButton(
                      padding: EdgeInsets.zero,
                      onPressed: () => controller.clickRotation(),
                      icon: Icon(controller.state.isLandscape ? Icons.fullscreen_exit : Icons.fullscreen,color: Colors.white,size: 24),
                    )
                  ]))),
        )));
  }

  Widget loadingView() {
    return const Center(
      child: CircularProgressIndicator(),
    );
  }
}
