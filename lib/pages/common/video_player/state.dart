import 'package:get/get.dart';

class VideoPlayerState {
  // title
  final _title = "".obs;
  set title(value) => _title.value = value;
  get title => _title.value;

  // 是否完成初始化
  final _isInit = false.obs;
  set isInit(bool value) => _isInit.value = value;
  bool get isInit => _isInit.value;

  // 是否正在播放
  final _isPlaying = false.obs;
  set isPlaying(bool value) => _isPlaying.value = value;
  bool get isPlaying => _isPlaying.value; 
  
  // 是否横屏
  final _isLandscape = false.obs;
  set isLandscape(bool value) => _isLandscape.value = value;
  bool get isLandscape => _isLandscape.value;


  final Rx<double> _progress = 0.00.obs;
  set progress(double value) => _progress.value = value;
  double get progress => _progress.value;

  final Rx<Duration> _position = Duration.zero.obs;
  set position(Duration value) => _position.value = value;
  Duration get position => _position.value;
  
  
  final Rx<Duration> _duration = Duration.zero.obs;
  set duration(Duration value) => _duration.value = value;
  Duration get duration => _duration.value;
  
  
}
