import 'package:get/get.dart';

import 'controller.dart';

class PreviewImageBinding implements Bindings {
  @override
  void dependencies() {
    Get.lazyPut<PreviewImageController>(() => PreviewImageController());
  }
}
