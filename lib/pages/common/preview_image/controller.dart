import 'package:get/get.dart';

import '../../../common/utils/index.dart';
import 'index.dart';

class PreviewImageController extends GetxController {
  PreviewImageController();

  final state = PreviewImageState();
 
  /// 在 widget 内存中分配后立即调用。
  @override
  void onInit() {
    super.onInit();
  }

  /// 在 onInit() 之后调用 1 帧。这是进入的理想场所
  @override
  void onReady() {
    var map = Get.arguments;
    if (map != null) {
      String fileName = map["fileName"] ?? "preview_image_title".tr;
      state.title = fileName;
      String path = map["path"] ?? "";
      if (path.isNotEmpty) {
        state.path = path;
      }
    }
    super.onReady();
  }

  /// 在 [onDelete] 方法之前调用。
  @override
  void onClose() {
    super.onClose();
  }

  /// dispose 释放内存
  @override
  void dispose() {
    super.dispose();
  }


  Future<void> saveToAlbum() async {
    if (state.path.isNotEmpty) {
      final result = await O2FlutterMethodChannelUtils().saveToAlbum(state.path);
      if (result) {
        Loading.toast('cloud_disk_save_image_album_success'.tr);
      }
    }
    
  }
}
