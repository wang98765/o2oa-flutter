import 'package:get/get.dart';

class PreviewImageState {
  // title
  final _title = "".obs;
  set title(value) => _title.value = value;
  get title => _title.value;


  // path 文件路径，本地路径
  final _path = "".obs;
  set path(value) => _path.value = value;
  String get path => _path.value;
}
