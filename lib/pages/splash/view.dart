import 'package:flutter/material.dart';
import 'package:get/get.dart';

import 'index.dart';
import 'widgets/widgets.dart';

class SplashPage extends GetView<SplashController> {
  const SplashPage({Key? key}) : super(key: key);

  // 内容页
  Widget _buildView() {
    return const SplashView();
  }

  @override
  Widget build(BuildContext context) {
    return GetBuilder<SplashController>(
      builder: (_) {
        return Scaffold(
            body: SizedBox(
                width: double.infinity,
                height: double.infinity,
                child: _buildView()));
      },
    );
  }
}
