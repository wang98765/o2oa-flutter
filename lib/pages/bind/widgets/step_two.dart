import 'package:flutter/material.dart';
import 'package:get/get.dart';
 
import '../../../common/style/index.dart';
import '../../../common/widgets/index.dart';
import '../controller.dart';

 

/// 绑定第一步
class TwoStepWidget extends GetView<BindController> {
  const TwoStepWidget({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return stepTwoWidget(context);
  }

   ///
  ///第二步 选择绑定服务器的界面
  ///
  Widget stepTwoWidget(BuildContext context) {
    return Column(children: [
      Container(
        height: 96,
        color: Theme.of(context).colorScheme.primary,
        child: Padding(
            padding: const EdgeInsets.only(top: 48),
            child: Align(
                alignment: Alignment.bottomCenter,
                child: Text('bind_choose_unit'.tr,
                    style: AppTheme.whitePrimaryTextStyle.copyWith(fontSize: 22)))),
      ),
      Container(
        height: 197,
        color: Theme.of(context).colorScheme.primary,
        child:  const AssetsImageView('bind_phone_step_two.png'),
      ),
      Expanded(
          flex: 1,
          child: Padding(
            padding: const EdgeInsets.only(left: 32, right: 32, bottom: 32),
            child: MediaQuery.removePadding(
                  context: context,
                  removeTop: true,
                  child: ListView.separated(
                      itemBuilder: (c, i) {
                        var unit = controller.unitList[i];
                        return ListTile(
                          title:  Text(unit.name ?? '', style: Theme.of(context).textTheme.bodyLarge),
                          subtitle: Text(unit.centerHost ?? '', style: Theme.of(context).textTheme.bodySmall),
                          trailing: const Icon(Icons.check_circle_outline),
                          onTap: () {
                            controller.chooseUnit(unit);
                          },
                        );
                      },
                      separatorBuilder: (c, i) {
                        return const Divider(
                          height: 1,
                        );
                      },
                      itemCount: controller.unitList.length)),
          ))
    ]);
  }

}
