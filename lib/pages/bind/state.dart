import 'package:get/get.dart';

class BindState {
  // title
  final _title = "".obs;
  set title(value) => _title.value = value;
  get title => _title.value;

  // 是否同意协议
  final _isAgree = false.obs;
  set isAgree(bool value) => _isAgree.value = value;
  bool get isAgree => _isAgree.value;

  // 当前页面状态
  final _isStepOne = true.obs;
  set isStepOne(value) => _isStepOne.value = value;
  get isStepOne => _isStepOne.value;

}
