import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:get/get.dart';

import '../../../common/routers/index.dart';
import '../../../common/style/index.dart';
import '../../../common/widgets/index.dart';
import 'index.dart';

class ServerInfoPage extends GetView<ServerInfoController> {
  const ServerInfoPage({Key? key}) : super(key: key);

  static Future<void> open({bool finishSelf = false}) async {
    if (!finishSelf) {
      await Get.toNamed(O2OARoutes.loginServerInfo);
    } else {
      await Get.offNamed(O2OARoutes.loginServerInfo);
    }
  }

  Widget _selectProtocol(BuildContext context) {
    return Obx(() => Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          mainAxisSize: MainAxisSize.max,
          children: controller.protocolList
              .map((element) => Expanded(
                  flex: 1,
                  child: InkWell(
                      onTap: () => controller.clickType(element),
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.center,
                        children: [
                          const SizedBox(height: 5),
                          Text(element,
                              style: Theme.of(context).textTheme.bodyMedium),
                          const SizedBox(height: 5),
                          O2RadioView(
                              isSelected: element == controller.state.protocol)
                        ],
                      ))))
              .toList(),
        ));
  }

  Widget _inputHost(BuildContext context) {
    return TextField(
      maxLines: 1,
      autofocus: true,
      style: Theme.of(context).textTheme.bodyMedium,
      keyboardType: TextInputType.text,
      textInputAction: TextInputAction.next,
      focusNode: controller.hostNode,
      controller: controller.hostController,
      onEditingComplete: () {
        FocusScope.of(context).requestFocus(controller.portNode);
      },
      decoration: InputDecoration(
        labelText: 'login_server_form_label_host'.tr,
      ),
    );
  }

  Widget _inputPort(BuildContext context) {
    return TextField(
      maxLines: 1,
      style: Theme.of(context).textTheme.bodyMedium,
      keyboardType: TextInputType.number,
      textInputAction: TextInputAction.next,
      focusNode: controller.portNode,
      controller: controller.portController,
      onEditingComplete: () {
        FocusScope.of(context).requestFocus(controller.urlMappingNode);
      },
      decoration: InputDecoration(
        labelText: 'login_server_form_label_port'.tr,
      ),
    );
  }

  Widget _inputUrlMapping(BuildContext context) {
    return TextField(
      maxLines: 5,
      style: Theme.of(context).textTheme.bodyMedium,
      keyboardType: TextInputType.text,
      textInputAction: TextInputAction.done,
      focusNode: controller.urlMappingNode,
      controller: controller.urlMappingController,
      decoration: InputDecoration(
        labelText: 'login_server_form_label_url_mapping'.tr,
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return GetBuilder<ServerInfoController>(
      builder: (_) {
        return Scaffold(
            appBar: AppBar(
              title: Text("login_server_title".tr),
              actions: [
                Obx(() => Visibility(
                    visible: controller.state.notSameConfig,
                    child: TextButton(
                        onPressed: () => controller.showAssetsConfig(),
                        child: Text('login_server_assets_config_btn'.tr,
                            style: AppTheme.whitePrimaryTextStyle))))
              ],
            ),
            body: SafeArea(
                child: PageScrollContentWidget(
              contentWidget: Center(
                child: SizedBox(
                    width: 295.w,
                    child: Column(children: [
                      const SizedBox(height: 16),
                      _selectProtocol(context),
                      const SizedBox(height: 16),
                      _inputHost(context),
                      const SizedBox(height: 16),
                      _inputPort(context),
                      const SizedBox(height: 16),
                      _inputUrlMapping(context),
                      const SizedBox(height: 16),
                      SizedBox(
                        width: double.infinity,
                        height: 42.h,
                        child: O2ElevatedButton(
                          () => controller.saveServerInfo(),
                          Text('save'.tr),
                        ),
                      ),
                    ])),
              ),
            )));
      },
    );
  }
}
