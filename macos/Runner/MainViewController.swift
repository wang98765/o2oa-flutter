//
//  MainViewController.swift
//  Runner
//
//  Created by FancyLou on 2022/8/30.
//

import Cocoa
import FlutterMacOS
import UserNotifications


class MainViewController: FlutterViewController {
    var status: NSStatusItem?
    
    override func viewDidLoad() {
        super.viewDidLoad()

        status = NSStatusBar.system.statusItem(withLength: NSStatusItem.variableLength)
        let image = NSImage(named: "statusLogo")
        image?.isTemplate = true
        status?.button?.image = image
        status?.button?.toolTip = "打开O2OA"
        status?.button?.action = #selector(activateWindow(_:))
        status?.button?.target = self
        
        requestNotifyPermission()
   }

    // 打开窗口
    @objc private func activateWindow(_ sender: AnyObject) {
       DispatchQueue.main.async {
           NSApp.windows.first?.orderFrontRegardless()
           NSApp.activate(ignoringOtherApps: true)
       }
   }
    // 必须请求通知权限
    private func requestNotifyPermission() {
        NSLog("请求通知权限。。。。。。。。。")
        let center = UNUserNotificationCenter.current()
        center.requestAuthorization(options: [.alert, .sound, .badge]) { granted, error in
            NSLog("请求通知权限 granted \(granted) ")
            if let error = error {
                // Handle the error here.
                NSLog("请求通知权限 error \(error.localizedDescription) ")
            }
            
        }
    }
}
