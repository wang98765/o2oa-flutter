import Cocoa
import FlutterMacOS

class MainFlutterWindow: NSWindow {
    
    var channel: FlutterMethodChannels?
  override func awakeFromNib() {
    let flutterViewController = MainViewController.init()
    let windowFrame = self.frame
    self.contentViewController = flutterViewController
    self.setFrame(windowFrame, display: true)

    RegisterGeneratedPlugins(registry: flutterViewController)
      if (channel == nil) {
          channel = FlutterMethodChannels()
      }
      channel?.register(withBinaryMessager: flutterViewController.engine.binaryMessenger)

    super.awakeFromNib()
  }
}
