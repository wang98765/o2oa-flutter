package net.zoneland.x.bpm.mobile.v1.zoneXBPM.view

import android.content.Context
import android.graphics.Color
import android.util.Log
import android.view.Gravity
import android.view.View
import android.widget.TextView
import io.flutter.plugin.platform.PlatformView
import java.util.*

/**
 * Created by fancyLou on 2022-08-25.
 * Copyright © 2022 android. All rights reserved.
 */
class TestBaiduMapView(private val context: Context?): PlatformView {

    val text = TextView(context)


    override fun getView(): View {
        text.text = "时间： ${Date()}"
        Log.d("TestBaiduMapView",  "getView........")
        text.setBackgroundColor(Color.GREEN)
        text.setTextColor(Color.WHITE)
        text.gravity = Gravity.CENTER
        return text
    }

    override fun dispose() {
        Log.d("TestBaiduMapView",  "dispose........")
    }
}