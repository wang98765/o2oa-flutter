package net.zoneland.x.bpm.mobile.v1.zoneXBPM.view

import android.content.Context
import android.util.Log
import io.flutter.plugin.common.BinaryMessenger
import io.flutter.plugin.common.StandardMessageCodec
import io.flutter.plugin.platform.PlatformView
import io.flutter.plugin.platform.PlatformViewFactory

/**
 * Created by fancyLou on 2022-08-25.
 * Copyright © 2022 android. All rights reserved.
 */
class TestBaiduMapViewFactory(val messenger: BinaryMessenger) : PlatformViewFactory(StandardMessageCodec.INSTANCE) {

    override fun create(context: Context?, viewId: Int, args: Any?): PlatformView {
        Log.d("TestBaiduMapViewFactory",  "viewId: $viewId")
        val flutterView = TestBaiduMapView(context)
        return flutterView
    }
}